﻿ИсходящиеДанные = Новый Структура;

Запрос = Новый Запрос;

Запрос.МенеджерВременныхТаблиц = ОбъектыПереносаДанных.ВыполнитьАлгоритм("ФизическиеЛица_ВременнаяТаблица", Параметры.Алгоритмы, Параметры, Запросы);

Запрос.Текст = Запросы.ГражданствоФизическихЛиц_Выборка.Текст;

ИсходящиеДанные.Вставить("ГражданствоФизическихЛиц",	Запрос.Выполнить().Выгрузить());
