﻿ПервичныеДанные = Новый ТаблицаЗначений;
ПервичныеДанные.Колонки.Добавить("Организация", Новый ОписаниеТипов("СправочникСсылка.Организации"));
ПервичныеДанные.Колонки.Добавить("ПериодРегистрации", Новый ОписаниеТипов("Дата"));
ПервичныеДанные.Колонки.Добавить("ОсновныеНачисления");
ПервичныеДанные.Колонки.Добавить("ДополнительныеНачисления");
ПервичныеДанные.Колонки.Добавить("НДФЛРасчетыСБюджетом");
ПервичныеДанные.Колонки.Добавить("ЗарплатаЗаМесяцОрганизаций");
ПервичныеДанные.Колонки.Добавить("НДФЛКЗачету");
ПервичныеДанные.Колонки.Добавить("НеОчищатьДокумент");
ПервичныеДанные.Колонки.Добавить("Авансом");

Запрос = Новый Запрос;

Запрос.УстановитьПараметр("ДатаНачалаПереносаРасчетныхДанных",	Параметры.ДатаНачалаПереносаРасчетныхДанных);

Запрос.МенеджерВременныхТаблиц = ОбъектыПереносаДанных.ВыполнитьАлгоритм("Сотрудники_ВременнаяТаблица", Параметры.Алгоритмы, Параметры, Запросы);

ДатаОкончания = ОбъектыПереносаДанных.ВыполнитьАлгоритм("ПолучитьДатуОкончанияПоРегиструОсновныеНачисления", Параметры.Алгоритмы, Параметры);

ДатаПереноса = Параметры.ДатаНачалаПереносаРасчетныхДанных;

Пока ДатаПереноса <= ДатаОкончания Цикл
	
	Запрос.УстановитьПараметр("ДатаПереносаРасчетныхДанных",	ДатаПереноса);
	
	Запрос.Текст = Запросы.ОснНачисленияРаботниковОрганизацийАвансом_Выборка.Текст;
	ВыборкаПоОрганизациям = Запрос.Выполнить().Выгрузить(ОбходРезультатаЗапроса.ПоГруппировкам);
	Для Каждого ДанныеПоОрганизации Из ВыборкаПоОрганизациям.Строки Цикл
		Организация = ДанныеПоОрганизации.ОрганизацияОтбор;
		Для Каждого ДанныеПоПериодуРегистрации Из ДанныеПоОрганизации.Строки Цикл
			НоваяСтрока = ПервичныеДанные.Добавить();
			НоваяСтрока.Авансом						= 1;
			НоваяСтрока.Организация					= Организация;
			НоваяСтрока.ПериодРегистрации			= ДанныеПоПериодуРегистрации.ПериодОтбор;
			НоваяСтрока.ОсновныеНачисления			= ДанныеПоПериодуРегистрации.Строки;
			НоваяСтрока.ДополнительныеНачисления	= Новый ТаблицаЗначений;
			НоваяСтрока.НДФЛРасчетыСБюджетом		= Новый ТаблицаЗначений;
			НоваяСтрока.ЗарплатаЗаМесяцОрганизаций	= Новый ТаблицаЗначений;
			НоваяСтрока.НДФЛКЗачету					= Новый ТаблицаЗначений;
		КонецЦикла;
	КонецЦикла;
	
	Запрос.Текст = Запросы.ДопНачисленияРаботниковОрганизацийАвансом_Выборка.Текст;
	ВыборкаПоОрганизациям = Запрос.Выполнить().Выгрузить(ОбходРезультатаЗапроса.ПоГруппировкам);
	Отбор = Новый Структура("Организация, ПериодРегистрации");
	Для Каждого ДанныеПоОрганизации Из ВыборкаПоОрганизациям.Строки Цикл
		Организация = ДанныеПоОрганизации.ОрганизацияОтбор;
		Отбор.Организация = Организация;
		Для Каждого ДанныеПоПериодуРегистрации Из ДанныеПоОрганизации.Строки Цикл
			Отбор.ПериодРегистрации = ДанныеПоПериодуРегистрации.ПериодОтбор;
			Строки = ПервичныеДанные.НайтиСтроки(Отбор);
			Если Строки.Количество() > 0 Тогда
				СтрокаРезультатов = Строки[0];
			Иначе
				СтрокаРезультатов = ПервичныеДанные.Добавить();
				СтрокаРезультатов.Авансом						= 1;
				СтрокаРезультатов.Организация					= Организация;
				СтрокаРезультатов.ПериодРегистрации				= ДанныеПоПериодуРегистрации.ПериодОтбор;
				СтрокаРезультатов.ОсновныеНачисления			= Новый ТаблицаЗначений;
				СтрокаРезультатов.ДополнительныеНачисления		= Новый ТаблицаЗначений;
				СтрокаРезультатов.НДФЛРасчетыСБюджетом			= Новый ТаблицаЗначений;
				СтрокаРезультатов.ЗарплатаЗаМесяцОрганизаций	= Новый ТаблицаЗначений;
				СтрокаРезультатов.НДФЛКЗачету					= Новый ТаблицаЗначений;
			КонецЕсли;
			СтрокаРезультатов.ДополнительныеНачисления			= ДанныеПоПериодуРегистрации.Строки;
		КонецЦикла;
	КонецЦикла;
	
	Запрос.Текст = Запросы.НДФЛРасчетыСБюджетом_Выборка.Текст;
	ВыборкаПоОрганизациям = Запрос.Выполнить().Выгрузить(ОбходРезультатаЗапроса.ПоГруппировкам);
	Отбор = Новый Структура("Организация, ПериодРегистрации");
	Для Каждого ДанныеПоОрганизации Из ВыборкаПоОрганизациям.Строки Цикл
		Организация = ДанныеПоОрганизации.ОрганизацияОтбор;
		Отбор.Организация = Организация;
		Для Каждого ДанныеПоПериодуРегистрации Из ДанныеПоОрганизации.Строки Цикл
			Отбор.ПериодРегистрации = ДанныеПоПериодуРегистрации.ПериодОтбор;
			Строки = ПервичныеДанные.НайтиСтроки(Отбор);
			Если Строки.Количество() > 0 Тогда
				СтрокаРезультатов = Строки[0];
			Иначе
				СтрокаРезультатов = ПервичныеДанные.Добавить();
				СтрокаРезультатов.Авансом						= 1;
				СтрокаРезультатов.Организация					= Организация;
				СтрокаРезультатов.ПериодРегистрации				= ДанныеПоПериодуРегистрации.ПериодОтбор;
				СтрокаРезультатов.ОсновныеНачисления			= Новый ТаблицаЗначений;
				СтрокаРезультатов.ДополнительныеНачисления		= Новый ТаблицаЗначений;
				СтрокаРезультатов.НДФЛРасчетыСБюджетом			= Новый ТаблицаЗначений;
				СтрокаРезультатов.ЗарплатаЗаМесяцОрганизаций	= Новый ТаблицаЗначений;
				СтрокаРезультатов.НДФЛКЗачету					= Новый ТаблицаЗначений;
			КонецЕсли;
			СтрокаРезультатов.НДФЛРасчетыСБюджетом				= ДанныеПоПериодуРегистрации.Строки;
		КонецЦикла;
	КонецЦикла;
	
	Запрос.Текст = Запросы.ЗарплатаЗаМесяцОрганизацийАвансом_Выборка.Текст;
	ВыборкаПоОрганизациям = Запрос.Выполнить().Выгрузить(ОбходРезультатаЗапроса.ПоГруппировкам);
	Отбор = Новый Структура("Организация, ПериодРегистрации");
	Для Каждого ДанныеПоОрганизации Из ВыборкаПоОрганизациям.Строки Цикл
		Организация = ДанныеПоОрганизации.ОрганизацияОтбор;
		Отбор.Организация = Организация;
		Для Каждого ДанныеПоПериодуРегистрации Из ДанныеПоОрганизации.Строки Цикл
			Отбор.ПериодРегистрации = ДанныеПоПериодуРегистрации.ПериодОтбор;
			Строки = ПервичныеДанные.НайтиСтроки(Отбор);
			Если Строки.Количество() > 0 Тогда
				СтрокаРезультатов = Строки[0];
			Иначе
				СтрокаРезультатов = ПервичныеДанные.Добавить();
				СтрокаРезультатов.Авансом						= 1;
				СтрокаРезультатов.Организация					= Организация;
				СтрокаРезультатов.ПериодРегистрации				= ДанныеПоПериодуРегистрации.ПериодОтбор;
				СтрокаРезультатов.ОсновныеНачисления			= Новый ТаблицаЗначений;
				СтрокаРезультатов.ДополнительныеНачисления		= Новый ТаблицаЗначений;
				СтрокаРезультатов.НДФЛРасчетыСБюджетом			= Новый ТаблицаЗначений;
				СтрокаРезультатов.ЗарплатаЗаМесяцОрганизаций	= Новый ТаблицаЗначений;
				СтрокаРезультатов.НДФЛКЗачету					= Новый ТаблицаЗначений;
			КонецЕсли;
			СтрокаРезультатов.ЗарплатаЗаМесяцОрганизаций		= ДанныеПоПериодуРегистрации.Строки;
		КонецЦикла;
	КонецЦикла;
	
	ДатаПереноса = ДобавитьМесяц(ДатаПереноса, 1);
	
КонецЦикла;

ФизическиеЛицаБезБазы = Новый ТаблицаЗначений;
ФизическиеЛицаБезБазы.Колонки.Добавить("Сотрудник");
ФизическиеЛицаБезБазы.Колонки.Добавить("Подразделение");
ФизическиеЛицаБезБазы.Колонки.Добавить("ФизическоеЛицо");

ИсходящиеДанные = Новый Структура;
ИсходящиеДанные.Вставить("ФизическиеЛицаБезБазы",	ФизическиеЛицаБезБазы);
ИсходящиеДанные.Вставить("ПервичныеДанные",			ПервичныеДанные);
ИсходящиеДанные.Вставить("ИмяПКО",			        "ЗарплатаКВыплатеАвансом_ПД");
