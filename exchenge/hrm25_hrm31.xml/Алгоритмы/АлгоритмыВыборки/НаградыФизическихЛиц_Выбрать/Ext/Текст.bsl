﻿ИсходящиеДанные = Новый Структура;

Запрос = Новый Запрос;

Запрос.МенеджерВременныхТаблиц = ОбъектыПереносаДанных.ВыполнитьАлгоритм("ФизическиеЛица_ВременнаяТаблица", Параметры.Алгоритмы, Параметры, Запросы);

Запрос.Текст = Запросы.НаградыФизическихЛиц_Выборка.Текст;

ИсходящиеДанные.Вставить("НаградыФизическихЛиц",	Запрос.Выполнить().Выгрузить());
