﻿ИсходящиеДанные = Новый Структура;

Запрос = Новый Запрос;
Запрос.Текст = Запросы.СтавкаВзносаНаСтрахованиеОтНС_Выборка.Текст;
Если  Параметры.ПереноситьНачисленияПредыдущейПрограммы Тогда
	Запрос.УстановитьПараметр("ДатаСреза", Параметры.ДатаНачалаПереносаРасчетныхДанных);
Иначе
	Запрос.УстановитьПараметр("ДатаСреза", Параметры.МесяцНачалаЭксплуатации);
КонецЕсли;
ИсходящиеДанные.Вставить("СтавкаВзносаНаСтрахованиеОтНС",	Запрос.Выполнить().Выгрузить());
