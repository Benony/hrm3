﻿ИсходящиеДанные = Новый Структура;

Запрос = Новый Запрос;

Запрос.МенеджерВременныхТаблиц = ОбъектыПереносаДанных.ВыполнитьАлгоритм("ФизическиеЛица_ВременнаяТаблица", Параметры.Алгоритмы, Параметры, Запросы);

Запрос.Текст = Запросы.СтатусНДФЛФизическихЛиц_Выборка.Текст;

ИсходящиеДанные.Вставить("СтатусНДФЛФизическихЛиц",	Запрос.Выполнить().Выгрузить());
