﻿ИсходящиеДанные = Новый Структура;

Запрос = Новый Запрос;

Запрос.МенеджерВременныхТаблиц = ОбъектыПереносаДанных.ВыполнитьАлгоритм("ФизическиеЛица_ВременнаяТаблица", Параметры.Алгоритмы, Параметры, Запросы);

Запрос.Текст = Запросы.ФИОФизическихЛиц_Выборка.Текст;

ИсходящиеДанные.Вставить("ФИОФизическихЛиц",	Запрос.Выполнить().Выгрузить());
