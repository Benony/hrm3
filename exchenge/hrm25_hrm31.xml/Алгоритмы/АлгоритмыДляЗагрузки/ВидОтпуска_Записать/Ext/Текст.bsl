﻿ОбновитьПовторноИспользуемыеЗначения();

ВидОтпускаСсылка = ОбщегоНазначенияКлиентСервер.ПредопределенныйЭлемент("Справочник.ВидыОтпусков." + ОписаниеВидаОтпуска.ИмяПредопределенныхДанных);

Если Не ЗначениеЗаполнено(ВидОтпускаСсылка) Тогда 
	ВидОтпускаОбъект = Справочники.ВидыОтпусков.СоздатьЭлемент();

	ЗаполнитьЗначенияСвойств(ВидОтпускаОбъект, ОписаниеВидаОтпуска);
	ВидОтпускаОбъект.Записать();
	
	ОбновитьПовторноИспользуемыеЗначения();
	
	ВидОтпускаСсылка = ВидОтпускаОбъект.Ссылка;
КонецЕсли;

Если ИмяПредопределенныхДанных = ОписаниеВидаОтпуска.ИмяПредопределенныхДанных Тогда
	СсылкаНаОбъект = ВидОтпускаСсылка;
КонецЕсли;
