﻿ТаблицаВоинскийУчетФизическихЛиц	= ОбъектыПереносаДанных.Структура_Получить(ИсходящиеДанные, "ВоинскийУчетФизическихЛиц");

ВыборкаДанных	= ОбъектыПереносаДанных.НовыйОбъект("Массив");

ОбъектыПереносаДанных.ТаблицаЗначений_ДобавитьКолонку(ТаблицаВоинскийУчетФизическихЛиц, "ФизическоеЛицо");
Для Номер = 1 По ОбъектыПереносаДанных.ТаблицаЗначений_Количество(ТаблицаВоинскийУчетФизическихЛиц) Цикл
	ВоинскийУчет = ОбъектыПереносаДанных.ТаблицаЗначений_Получить(ТаблицаВоинскийУчетФизическихЛиц, Номер);
	
	ВоинскийУчет.ФизическоеЛицо	= ВоинскийУчет.ФизЛицо;
	
	Если ВоинскийУчет.КатегорияЗапаса = "До45лет" Тогда
		ВоинскийУчет.КатегорияЗапаса = "ПерваяКатегория";
		
	ИначеЕсли ВоинскийУчет.КатегорияЗапаса = "До50лет" Тогда
		ВоинскийУчет.КатегорияЗапаса = "ВтораяКатегория";
		
	КонецЕсли;
	
	ОбъектыПереносаДанных.Массив_Добавить(ВыборкаДанных, ОбъектыПереносаДанных.СтрокаТаблицыЗначенийВСтруктуру(ВоинскийУчет));
КонецЦикла;
