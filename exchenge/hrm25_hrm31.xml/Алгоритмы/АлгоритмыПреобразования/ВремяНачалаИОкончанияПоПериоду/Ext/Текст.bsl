﻿Алгоритмы				= ВидСубконто;
ВремяНачалаПериода		= ИсходящиеДанные;
ВремяОкончанияПериода	= ВходящиеДанные;
ВремяНачалаГрафика		= Источник;
ВремяОкончанияГрафика	= Приемник;

Если ВремяНачалаПериода > ВремяОкончанияГрафика Тогда
	РезультатНачало		= НачалоДня(Дата(1, 1, 1));
	
ИначеЕсли ВремяОкончанияПериода < ВремяНачалаГрафика Тогда
	РезультатНачало		= НачалоДня(Дата(1, 1, 1));
	
ИначеЕсли ВремяНачалаПериода < ВремяНачалаГрафика Тогда
	РезультатНачало		= ВремяНачалаГрафика;
	
Иначе
	РезультатНачало		= ВремяНачалаПериода;
	
КонецЕсли;

Если ВремяНачалаПериода > ВремяОкончанияГрафика Тогда
	РезультатОкончание	= НачалоДня(Дата(1, 1, 1));
	
ИначеЕсли ВремяОкончанияПериода < ВремяНачалаГрафика Тогда
	РезультатОкончание	= НачалоДня(Дата(1, 1, 1));
	
ИначеЕсли ВремяОкончанияПериода > ВремяОкончанияГрафика Тогда
	РезультатОкончание	= ВремяОкончанияГрафика;
	
Иначе
	РезультатОкончание	= ВремяОкончанияПериода;
	
КонецЕсли;

Результат = ОбъектыПереносаДанных.НовыйОбъект("Структура");
ОбъектыПереносаДанных.Структура_Установить(Результат, "Начало", РезультатНачало);
ОбъектыПереносаДанных.Структура_Установить(Результат, "Окончание", РезультатОкончание);

Субконто		= Результат;
