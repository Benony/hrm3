﻿Алгоритмы				= ВидСубконто;
Параметры 				= ИсходящиеДанные;
ПоказательСсылка		= ВходящиеДанные;
ЗапрашиватьПоказатель	= Источник;
НомерПоказателя			= Приемник;
ФормулаРасчета			= ТипПриемника;
НачислениеУдержание		= Значение;

РасчетВоеннослужащих = Неопределено;
Параметры.Свойство("РасчетВоеннослужащих",РасчетВоеннослужащих);
Если РасчетВоеннослужащих = Неопределено Тогда
	Настройки = ОбъектыПереносаДанных.ВыполнитьАлгоритм("НастройкиГосударственнойСлужбы_Заполнить", Параметры.Алгоритмы, Параметры);
	РасчетВоеннослужащих = Настройки.ИспользоватьРасчетДенежногоДовольствияВоеннослужащих;
	Параметры.Вставить("РасчетВоеннослужащих",РасчетВоеннослужащих);
КонецЕсли;

Показатель = ОбъектыПереносаДанных.ВыполнитьАлгоритм("Показатель_Описание", Алгоритмы);
ЗаполнитьЗначенияСвойств(Показатель, ПоказательСсылка);

Наименование			= Показатель.Наименование;
ИдентификаторИсходный	= Показатель.Идентификатор;


ИмяПредопределенногоПоказателя = Справочники.ПоказателиСхемМотивации.ПолучитьИмяПредопределенного(Показатель.Ссылка);
Предопределенный = ЗначениеЗаполнено(ИмяПредопределенногоПоказателя);

Идентификатор = ИдентификаторИсходный;
Идентификатор = ОбъектыПереносаДанных.ВыполнитьАлгоритм("ПереставитьПервуюЦифруИдентификатора", Алгоритмы, Идентификатор);

НазначениеПоказателя = Показатель.ВидПоказателя; 
Если НазначениеПоказателя = Перечисления.ВидыПоказателейСхемМотивации.ДляВсехОрганизаций Тогда
	
	НазначениеПоказателя	= "ДляОрганизации";
	
ИначеЕсли НазначениеПоказателя = Перечисления.ВидыПоказателейСхемМотивации.Общий Тогда
	
	НазначениеПоказателя	= "ДляОрганизации";
	
ИначеЕсли НазначениеПоказателя = Перечисления.ВидыПоказателейСхемМотивации.ПоПодразделению Тогда
	
	НазначениеПоказателя	= "ДляПодразделения";
	
Иначе
	
	НазначениеПоказателя	= "ДляСотрудника";
	
КонецЕсли;

Если Не Предопределенный Тогда
	
	НаименованиеДляВсех = "";
	Если НазначениеПоказателя	= "ДляСотрудника" Тогда
		ВозможностьИзменения = Показатель.ВозможностьИзменения;
		Если ВозможностьИзменения = Перечисления.ИзменениеПоказателейСхемМотивации.НеИзменяется и НачислениеУдержание <> "" Тогда
			НаименованиеДляВсех	= " для " + ОбъектыПереносаДанных.Структура_Получить(НачислениеУдержание, "Наименование");
		КонецЕсли;
	КонецЕсли;
	Наименование		= Наименование + НаименованиеДляВсех;
	Идентификатор		= Идентификатор + ОбъектыПереносаДанных.ВыполнитьАлгоритм("Показатель_ИдентификаторПоНаименованию", Алгоритмы, НаименованиеДляВсех);
	
Иначе
	
	Если ИмяПредопределенногоПоказателя = "ОВД" Тогда
		Идентификатор = "Оклад";
	ИначеЕсли ИмяПредопределенногоПоказателя = "ОВЗ" Тогда
		
		Если РасчетВоеннослужащих Тогда
			Идентификатор = "ОкладПоВоинскомуЗванию";
		Иначе
			Идентификатор = "ОкладПоСпециальномуЗванию";
		КонецЕсли;
		
	Иначе	
		
		Идентификатор = ?(Идентификатор = "ВремяВКалендарныхДнях", "КалендарныеДни", Идентификатор);
		Идентификатор = ?(Идентификатор = "КалендарныхДнейВмесяце", "КалендарныеДниМесяца", Идентификатор);
		Идентификатор = ?(Идентификатор = "ОтработаноВремениВДнях", "ОтработаноДней", Идентификатор);
		Идентификатор = ?(Идентификатор = "ОтработаноВремениВЧасах", "ОтработаноЧасов", Идентификатор);
		Идентификатор = ?(Идентификатор = "СдельнаяВыработка", "СдельныйЗаработок", Идентификатор);
		Идентификатор = ?(Идентификатор = "ТарифнаяСтавкаМесячная", "Оклад", Идентификатор);
		Идентификатор = ?(Идентификатор = "НормаВремениВДнях", "НормаДнейПоГрафикуПолногоРабочегоВремени", Идентификатор);
		Идентификатор = ?(Идентификатор = "НормаВремениВЧасах", "НормаЧасов", Идентификатор);
		
	КонецЕсли;
	
КонецЕсли;

СтрокаСИсходнымПоказателем = "ИсходныеДанные.Показатель"+Строка(НомерПоказателя);
Если Найти(ФормулаРасчета, СтрокаСИсходнымПоказателем) <> 0 Тогда
	
	ФормулаРасчета = СтрЗаменить(ФормулаРасчета, СтрокаСИсходнымПоказателем, Идентификатор);
	
КонецЕсли;

Процентный = Ложь;
ТипПоказателя = Показатель.ТипПоказателя;
Если ТипПоказателя	= Перечисления.ТипыПоказателейСхемМотивации.Денежный Тогда
	ТипПоказателя	= "Денежный";
ИначеЕсли ТипПоказателя	= Перечисления.ТипыПоказателейСхемМотивации.Процентный Тогда
	ТипПоказателя	= "Числовой";
	Процентный = Истина;
	Если ЗначениеЗаполнено(ФормулаРасчета) И Найти(ФормулаРасчета, "/ 100") = 0 Тогда
		ФормулаРасчета = "" + ФормулаРасчета + " / 100";
	КонецЕсли;
ИначеЕсли ТипПоказателя = Перечисления.ТипыПоказателейСхемМотивации.Стаж Тогда
	ТипПоказателя	= "ЧисловойЗависящийОтСтажа";
	ФормулаНеКонвертируется = Истина;
Иначе
	ТипПоказателя	= "Числовой";
КонецЕсли;

ВидСтажа = "";
Если ЗначениеЗаполнено(Показатель.ВидСтажа) Тогда
	
	Если Показатель.ВидСтажа = Справочники.ВидыСтажа.НепрерывныйСтаж Тогда
		КатегорияСтажа = "Непрерывный";
	ИначеЕсли Показатель.ВидСтажа = Справочники.ВидыСтажа.ОбщийНаучноПедагогическийСтаж Тогда
		КатегорияСтажа = "ОбщийНаучноПедагогический";
	ИначеЕсли Показатель.ВидСтажа = Справочники.ВидыСтажа.ОбщийСтаж Тогда
		КатегорияСтажа = "Общий";
	ИначеЕсли Показатель.ВидСтажа = Справочники.ВидыСтажа.ПедагогическийСтаж Тогда
		КатегорияСтажа = "Педагогический";
	ИначеЕсли Показатель.ВидСтажа = Справочники.ВидыСтажа.РасширенныйСтраховойСтажДляБЛ Тогда
		КатегорияСтажа = "РасширенныйСтраховой";
	ИначеЕсли Показатель.ВидСтажа = Справочники.ВидыСтажа.СтажНаНадбавкуЗаВыслугуЛет Тогда
		КатегорияСтажа = "ВыслугаЛет";
	ИначеЕсли Показатель.ВидСтажа = Справочники.ВидыСтажа.СтраховойСтажДляБЛ Тогда
		КатегорияСтажа = "Страховой";	
	Иначе
		КатегорияСтажа = "Прочее";
	КонецЕсли;
	
	ВидСтажа = Новый Структура;
	ВидСтажа.Вставить("Наименование", Показатель.ВидСтажа.Наименование);
	ВидСтажа.Вставить("КатегорияСтажа", КатегорияСтажа);
	ВидСтажа.Вставить("ПометкаУдаления", Показатель.ВидСтажа.ПометкаУдаления);
	
КонецЕсли;

Показатель.Наименование = Наименование;
Показатель.Идентификатор = Идентификатор;
Показатель.ТипПоказателя = ТипПоказателя;
Показатель.Процентный = Процентный;
Показатель.НазначениеПоказателя = НазначениеПоказателя;
Показатель.ЗначениеРассчитываетсяАвтоматически = Ложь;
Показатель.ОтображатьВДокументахНачисления = Истина;
Показатель.СпособПримененияЗначений = ?(ЗапрашиватьПоказатель, "Постоянное", "Разовое");
Показатель.СпособВводаЗначений = "ВводитсяЕдиновременно";
Показатель.ВидСтажа = ВидСтажа;
Показатель.Ссылка = Идентификатор;


Если СОКРЛП(Идентификатор) = "ОкладЗаКлассныйЧин" 
	Или СОКРЛП(Идентификатор) = "ОкладПоВоинскомуЗванию" 
	Или СОКРЛП(Идентификатор) = "ОкладПоСпециальномуЗванию" Тогда
	ЗапрашиватьПоказатель = Истина;
КонецЕсли; 

Источник 	= ЗапрашиватьПоказатель;
ТипПриемника	= ФормулаРасчета;
Субконто	= Показатель;
