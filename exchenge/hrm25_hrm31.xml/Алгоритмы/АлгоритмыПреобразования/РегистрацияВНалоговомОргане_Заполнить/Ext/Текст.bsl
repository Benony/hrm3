﻿Алгоритмы		= ВидСубконто;
Владелец		= ИсходящиеДанные;
Подразделение	= ВходящиеДанные;
КодПоОКТМО		= Источник;
КодПоОКАТО		= Приемник;
КПП				= ТипПриемника;

Если НЕ ЗначениеЗаполнено(КодПоОКТМО) Тогда
	КодПоОКТМО = ОбъектыПереносаДанных.Структура_Получить(Подразделение, "КодПоОКТМО");
	Если Не ЗначениеЗаполнено(КодПоОКТМО) Тогда
		КодПоОКТМО = ОбъектыПереносаДанных.Структура_Получить(Владелец, "КодПоОКТМО");	
	КонецЕсли;	
КонецЕсли;

Если НЕ ЗначениеЗаполнено(КодПоОКАТО) Тогда
	КодПоОКАТО = ОбъектыПереносаДанных.Структура_Получить(Подразделение, "КодПоОКАТО");
	Если Не ЗначениеЗаполнено(КодПоОКАТО) Тогда
		КодПоОКАТО = ОбъектыПереносаДанных.Структура_Получить(Владелец, "КодПоОКАТО");	
	КонецЕсли;	
КонецЕсли;

Если НЕ ЗначениеЗаполнено(КПП) Тогда
	КПП = ОбъектыПереносаДанных.Структура_Получить(Подразделение, "КПП");
	Если Не ЗначениеЗаполнено(КПП) Тогда
		КПП = ОбъектыПереносаДанных.Структура_Получить(Владелец, "КПП");	
	КонецЕсли;	
КонецЕсли;

Если ЗначениеЗаполнено(КодПоОКТМО) Или ЗначениеЗаполнено(КодПоОКАТО) Или ЗначениеЗаполнено(КПП) Тогда
	РегистрацияВНалоговомОргане = ОбъектыПереносаДанных.НовыйОбъект("Структура");
	ОбъектыПереносаДанных.Структура_Установить(РегистрацияВНалоговомОргане, "Владелец",		Владелец);
	ОбъектыПереносаДанных.Структура_Установить(РегистрацияВНалоговомОргане, "КодПоОКТМО",		КодПоОКТМО);
	ОбъектыПереносаДанных.Структура_Установить(РегистрацияВНалоговомОргане, "КодПоОКАТО",		КодПоОКАТО);
	ОбъектыПереносаДанных.Структура_Установить(РегистрацияВНалоговомОргане, "КПП",			КПП);
Иначе
	РегистрацияВНалоговомОргане = "";
КонецЕсли;

Субконто = РегистрацияВНалоговомОргане;
