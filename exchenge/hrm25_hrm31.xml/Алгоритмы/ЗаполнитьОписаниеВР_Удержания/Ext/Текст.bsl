﻿ПараметрыДляЗаполнения = ИсходящиеДанные;

Параметры            = ПараметрыДляЗаполнения.Параметры;
ВР                   = ПараметрыДляЗаполнения.ВР;
ТолькоПлановыеУдержания = ПараметрыДляЗаполнения.ТолькоПлановыеУдержания;

СвойстваУдержаний = ОбъектыПереносаДанных.ВыполнитьАлгоритм("СвойстваУдержанийПоКатегориям", Параметры.Алгоритмы, Параметры);

ОписаниеВР = Новый Структура(
"Код, 
|Наименование,
|КраткоеНаименование,
|КатегорияУдержания,
|СпособВыполненияУдержания,
|СпособРасчета,
|ФормулаРасчета,
|УчаствуетВРасчетеПервойПоловиныМесяца,
|ВидОтпуска,
|ОтборБазовых,
|Рассчитывается,
|Ссылка,
|Описание,
|ГдеЗначенияПоказателей,
|ГдеЗначение,
|Показатели,
|ЗапрашиваемыеПоказатели,
|БазовыеВидыРасчета,
|Идентификатор"); 	

ОписаниеВР.УчаствуетВРасчетеПервойПоловиныМесяца = Истина;
ОписаниеВР.ГдеЗначенияПоказателей = Новый Структура;
ОписаниеВР.Показатели = Новый ТаблицаЗначений;
ОписаниеВР.ЗапрашиваемыеПоказатели = Новый Массив;
ОписаниеВР.БазовыеВидыРасчета = Новый ТаблицаЗначений;

ОписаниеВР.Ссылка = ВР;
ОписаниеВР.ГдеЗначение = "";

// Показатели
ТЗПоказатели = Новый ТаблицаЗначений;
ТЗПоказатели.Колонки.Добавить("Показатель");
ТЗПоказатели.Колонки.Добавить("ЗапрашиватьПриВводе");

МенеджерВР = ПланыВидовРасчета.УдержанияОрганизаций;
МенеджерПоказатели = Справочники.ПоказателиСхемМотивации;

ОписаниеВР.БазовыеВидыРасчета = ВР.БазовыеВидыРасчета.Выгрузить();

ПоказателиУдержаний = Новый Массив;

ИмяПредопределенного  = МенеджерВР.ПолучитьИмяПредопределенного(ВР);
ИдентификаторЭлемента = ВР.ИдентификаторЭлемента;

ОписаниеВР.Идентификатор        = ВР.Ссылка;
Если ЗначениеЗаполнено(ИмяПредопределенного) Тогда
	ОписаниеВР.Идентификатор = ИмяПредопределенного;
ИначеЕсли ЗначениеЗаполнено(ИдентификаторЭлемента) Тогда
	ОписаниеВР.Идентификатор = ИдентификаторЭлемента;	
КонецЕсли;

// ПочтовыйСборПоИЛ
// УдержаниеЗаНеотработанныйОтпускКалендарныеДни
// УдержаниеЗаНеотработанныйОтпускШестидневка
// ИЛПроцентом
// ИЛПроцентомБезБЛ
// ИЛПроцентомДоПредела
// ИЛПроцентомДоПределаБезБЛ
// ИЛФиксированнойСуммой
// ИЛФиксированнойСуммойДоПредела

Если ВР.ЯвляетсяДСВ Или ВР.СпособРасчета = Перечисления.СпособыРасчетаОплатыТруда.ПроцентомОтОблагаемыхЕСННачислений Тогда
	
	ОписаниеВР.КатегорияУдержания = "ДСВ";
	ЗаполнитьЗначенияСвойств(ОписаниеВР, СвойстваУдержаний[ОписаниеВР.КатегорияУдержания]);
	
	Если ВР.СпособРасчета = Перечисления.СпособыРасчетаОплатыТруда.ПроцентомОтОблагаемыхЕСННачислений Тогда
		ОписаниеВР.Код						= НСтр("ru = 'ДСВЗН'");
		ОписаниеВР.Наименование				= НСтр("ru = 'Добровольные страховые взносы'");
		ОписаниеВР.КраткоеНаименование 		= НСтр("ru = 'Добр. страх. взносы'");
		ОписаниеВР.ФормулаРасчета				= "РасчетнаяБазаСтраховыеВзносы * ПроцентДСВ / 100";
		ОписаниеВР.ЗапрашиваемыеПоказатели  = ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве("ПроцентДСВ");
		ОписаниеВР.ГдеЗначенияПоказателей.Вставить("ПроцентДСВ", "Показатель1");
		ПоказателиУдержаний = ОбщегоНазначенияЗК.РазложитьСтрокуВМассивПодстрок("РасчетнаяБазаСтраховыеВзносы,ПроцентДСВ");
	Иначе
		ОписаниеВР.Код						= НСтр("ru = 'ДСВ'");
		ОписаниеВР.Наименование				= НСтр("ru = 'Добровольные страховые взносы (фиксированной суммой)'");
		ОписаниеВР.КраткоеНаименование 		= НСтр("ru = 'Добр. страх. взносы'");
		ОписаниеВР.ФормулаРасчета			= "";
		ОписаниеВР.ГдеЗначение 				= "Показатель1";
		ОписаниеВР.Рассчитывается  			= Ложь;
	КонецЕсли;
	
ИначеЕсли ИмяПредопределенного = "ПочтовыйСборПоИЛ" Или ИдентификаторЭлемента = "БанковскиеИздержкиПоИЛ" Тогда
	
	ОписаниеВР.КатегорияУдержания = "ВознаграждениеПлатежногоАгента";
	ЗаполнитьЗначенияСвойств(ОписаниеВР, СвойстваУдержаний[ОписаниеВР.КатегорияУдержания]);
	ОписаниеВР.Код						= НСтр("ru = 'АГЕНТ'");
	ОписаниеВР.Наименование				= НСтр("ru = 'Вознаграждение платежного агента'");
	ОписаниеВР.КраткоеНаименование 		= НСтр("ru = 'Возн. плат. агента'");
	
ИначеЕсли ИмяПредопределенного = "ИЛПроцентом" Или ИмяПредопределенного = "ИЛПроцентомБезБЛ"
	Или ИмяПредопределенного = "ИЛПроцентомДоПредела" Или ИмяПредопределенного = "ИЛПроцентомДоПределаБезБЛ"
	Или ИмяПредопределенного = "ИЛФиксированнойСуммой" Или ИмяПредопределенного = "ИЛФиксированнойСуммойДоПредела" 
	Или ИдентификаторЭлемента = "ИЛВПрожиточныхМинимумах" Тогда	
	
	ОписаниеВР.КатегорияУдержания = "ИсполнительныйЛист";
	ЗаполнитьЗначенияСвойств(ОписаниеВР, СвойстваУдержаний[ОписаниеВР.КатегорияУдержания]);
	ОписаниеВР.Код						= НСтр("ru = 'ИСПДК'");
	ОписаниеВР.Наименование				= НСтр("ru = 'Удержание по исполнительному документу'");
	ОписаниеВР.КраткоеНаименование 		= НСтр("ru = 'Исп. лист'");
	
ИначеЕсли Найти(ВРЕГ(ВР.Наименование), "ПРОФВЗНОС") > 0 Или  Найти(ВРЕГ(ВР.Наименование), "ПРОФСОЮЗ")  > 0 Тогда	
	
	ОписаниеВР.КатегорияУдержания = "ПрофсоюзныеВзносы";
	ЗаполнитьЗначенияСвойств(ОписаниеВР, СвойстваУдержаний[ОписаниеВР.КатегорияУдержания]);
	
	Если ВР.СпособРасчета = Перечисления.СпособыРасчетаОплатыТруда.Процентом Тогда
		ОписаниеВР.Код					= НСтр("ru = 'ПРФВЗ'");
		ОписаниеВР.Наименование			= НСтр("ru = 'Профсоюзные взносы'");
		ОписаниеВР.КраткоеНаименование 	= НСтр("ru = 'Профвзносы'");
		ОписаниеВР.ФормулаРасчета			= "РасчетнаяБаза * ПроцентПрофсоюзныхВзносов / 100";
		ОписаниеВР.ЗапрашиваемыеПоказатели  = ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве("ПроцентПрофсоюзныхВзносов");
		ОписаниеВР.ГдеЗначенияПоказателей.Вставить("ПроцентПрофсоюзныхВзносов", "Показатель1");
		ПоказателиУдержаний = ОбщегоНазначенияЗК.РазложитьСтрокуВМассивПодстрок("РасчетнаяБаза,ПроцентПрофсоюзныхВзносов");
	Иначе
		ОписаниеВР.Код					= ВР.Код;
		ОписаниеВР.Наименование			= ВР.Наименование;
		ОписаниеВР.ФормулаРасчета			= "";
		ОписаниеВР.ГдеЗначение 				= "Показатель1";
		ОписаниеВР.Рассчитывается  			= Ложь;
	КонецЕсли;
	
ИначеЕсли ВР.СпособРасчета = Перечисления.СпособыРасчетаОплатыТруда.Процентом Тогда
	
	НаименованиеПоказателя = ВР.Наименование;
	ИдентификаторПоказателя = ОбъектыПереносаДанных.ВыполнитьАлгоритм("Показатель_ИдентификаторПоНаименованию", Параметры.Алгоритмы, НаименованиеПоказателя);
	
	ОписаниеПоказателя = ОбъектыПереносаДанных.ВыполнитьАлгоритм("ОписаниеПоказателяПоИдентификатору", Параметры.Алгоритмы,"##");
	ОписаниеПоказателя.Идентификатор = ИдентификаторПоказателя;  
	ОписаниеПоказателя.Наименование = НаименованиеПоказателя;
	ОписаниеПоказателя.КраткоеНаименование = ОбъектыПереносаДанных.ВыполнитьАлгоритм("КраткоеНаименованиеПоТекстовойСтроке", Параметры.Алгоритмы, НаименованиеПоказателя);
	ОписаниеПоказателя.ЗначениеРассчитываетсяАвтоматически = Ложь;
	ОписаниеПоказателя.СпособПримененияЗначений = "Постоянное";
	ОписаниеПоказателя.Точность = 2;
	ОписаниеПоказателя.Предопределенный = Ложь;
	
	НоваяСтрока = ТЗПоказатели.Добавить();
	НоваяСтрока.Показатель = ОписаниеПоказателя;
	НоваяСтрока.ЗапрашиватьПриВводе = Истина;
	
	ОписаниеВР.КатегорияУдержания = "ПрочееУдержаниеВПользуТретьихЛиц";
	ЗаполнитьЗначенияСвойств(ОписаниеВР, СвойстваУдержаний[ОписаниеВР.КатегорияУдержания]);
	ОписаниеВР.Код					= ВР.Код;
	ОписаниеВР.Наименование			= ВР.Наименование;
	ОписаниеВР.ФормулаРасчета 		= ИдентификаторПоказателя + " / 100 * РасчетнаяБаза";
	ОписаниеВР.ГдеЗначенияПоказателей.Вставить(ИдентификаторПоказателя, "Показатель1");
	ПоказателиУдержаний = ОбщегоНазначенияЗК.РазложитьСтрокуВМассивПодстрок("РасчетнаяБаза");
	
ИначеЕсли ВР.СпособРасчета = Перечисления.СпособыРасчетаОплатыТруда.УдержаниеФиксированнойСуммой Тогда
	
	ОписаниеВР.КатегорияУдержания = "ПрочееУдержаниеВПользуТретьихЛиц";
	ЗаполнитьЗначенияСвойств(ОписаниеВР, СвойстваУдержаний[ОписаниеВР.КатегорияУдержания]);
	ОписаниеВР.Код					= ВР.Код;
	ОписаниеВР.Наименование			= ВР.Наименование;
	ОписаниеВР.ФормулаРасчета			= "";
	ОписаниеВР.ГдеЗначение 				= "Показатель1";
	ОписаниеВР.Рассчитывается  			= Ложь;
	
Иначе
	
	// не входит в состав плановых, или просто не переносим
	
КонецЕсли;

// Добавим в таблицу предопределенные показатели
Для каждого Показатель Из ПоказателиУдержаний Цикл
	НоваяСтрока = ТЗПоказатели.Добавить();
	НоваяСтрока.Показатель = ОбъектыПереносаДанных.ВыполнитьАлгоритм("ОписаниеПоказателяПоИдентификатору", Параметры.Алгоритмы,Показатель);
	НоваяСтрока.ЗапрашиватьПриВводе = ?(ОписаниеВР.ЗапрашиваемыеПоказатели.Найти(Показатель) <> Неопределено, Истина, Ложь);
КонецЦикла;
ОписаниеВР.Показатели = ТЗПоказатели;

Субконто = ОписаниеВР;
