﻿Алгоритмы								= ВидСубконто;
Параметры								= ИсходящиеДанные;
Запросы									= ВходящиеДанные;
ПринудительноОбновитьВременнуюТаблицу	= Источник;

РегистраторыНДФЛ_ВременнаяТаблица = Неопределено;
Если ЗначениеЗаполнено(ПринудительноОбновитьВременнуюТаблицу) = 0 Тогда
	РегистраторыНДФЛ_ВременнаяТаблица = ОбъектыПереносаДанных.Структура_Получить(Параметры.ВременноеХранилище, "РегистраторыНДФЛ_ВременнаяТаблица");
КонецЕсли;

Если РегистраторыНДФЛ_ВременнаяТаблица = Неопределено Тогда
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	
	НачалоГода   = НачалоГода(Параметры.МесяцНачалаЭксплуатации);
	ДатаОкончанияПериода = КонецМесяца(ДобавитьМесяц(Параметры.МесяцНачалаЭксплуатации, -1));
	
	Запрос.УстановитьПараметр("ДатаНачала", НачалоГода);
	Запрос.УстановитьПараметр("ДатаОкончания", КонецДня(ДатаОкончанияПериода));
	Запрос.УстановитьПараметр("ДатаОстатков", НачалоГода);
	Запрос.Текст = Запросы.РегистраторыНДФЛ_ВременнаяТаблица.Текст;
	Запрос.Выполнить();
	
	РегистраторыНДФЛ_ВременнаяТаблица = Запрос.МенеджерВременныхТаблиц;
	
	ОбъектыПереносаДанных.Структура_Установить(Параметры.ВременноеХранилище, "РегистраторыНДФЛ_ВременнаяТаблица", РегистраторыНДФЛ_ВременнаяТаблица);
	
КонецЕсли;

Субконто = РегистраторыНДФЛ_ВременнаяТаблица;
