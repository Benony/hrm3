﻿ВЫБРАТЬ
	ФизическиеЛицаЗнаниеЯзыков.Ссылка КАК Физлицо,
	ФизическиеЛицаЗнаниеЯзыков.Язык,
	ФизическиеЛицаЗнаниеЯзыков.СтепеньЗнанияЯзыка
ИЗ
	Справочник.ФизическиеЛица.ЗнаниеЯзыков КАК ФизическиеЛицаЗнаниеЯзыков
ГДЕ
	ФизическиеЛицаЗнаниеЯзыков.Ссылка В
			(ВЫБРАТЬ РАЗЛИЧНЫЕ
				ФизическиеЛица.Физлицо
			ИЗ
				ВТФизическиеЛица КАК ФизическиеЛица)

УПОРЯДОЧИТЬ ПО
	Физлицо
