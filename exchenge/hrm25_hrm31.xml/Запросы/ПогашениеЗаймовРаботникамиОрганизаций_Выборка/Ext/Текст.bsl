﻿ВЫБРАТЬ
	ПогашениеЗаймовРаботникамиОрганизаций.Период,
	ПогашениеЗаймовРаботникамиОрганизаций.Организация,
	ПогашениеЗаймовРаботникамиОрганизаций.ФизЛицо КАК ФизЛицо,
	ПогашениеЗаймовРаботникамиОрганизаций.ОсновнойДолг,
	ПогашениеЗаймовРаботникамиОрганизаций.Проценты,
	ВЫБОР
		КОГДА ПогашениеЗаймовРаботникамиОрганизаций.УдержаноИзЗарплаты
			ТОГДА 1
		ИНАЧЕ 0
	КОНЕЦ КАК УдержаноИзЗарплаты,
	ПогашениеЗаймовРаботникамиОрганизаций.ДоговорЗайма,
	ПогашениеЗаймовРаботникамиОрганизаций.ВидДвижения,
	NULL КАК СтатьяФинансирования,
	NULL КАК КОСГУ,
	НАЧАЛОПЕРИОДА(ПогашениеЗаймовРаботникамиОрганизаций.Период, МЕСЯЦ) КАК ПериодОтбор,
	ПогашениеЗаймовРаботникамиОрганизаций.Организация КАК ОрганизацияОтбор
ИЗ
	РегистрНакопления.ПогашениеЗаймовРаботникамиОрганизаций КАК ПогашениеЗаймовРаботникамиОрганизаций
ГДЕ
	ПогашениеЗаймовРаботникамиОрганизаций.ДоговорЗайма В
			(ВЫБРАТЬ РАЗЛИЧНЫЕ
				ДоговорыЗайма.ДоговорЗайма
			ИЗ
				ВТДоговорыЗайма КАК ДоговорыЗайма)

УПОРЯДОЧИТЬ ПО
	ОрганизацияОтбор,
	ПериодОтбор,
	ФизЛицо
ИТОГИ ПО
	ОрганизацияОтбор,
	ПериодОтбор
