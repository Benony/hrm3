﻿ВЫБРАТЬ
	РаботникиОрганизацийСрезПоследних.Сотрудник,
	РаботникиОрганизацийСрезПоследних.Сотрудник.ВидЗанятости,
	РаботникиОрганизацийСрезПоследних.ПричинаИзмененияСостояния
ПОМЕСТИТЬ ВТРаботникиОсновноеМесто
ИЗ
	РегистрСведений.РаботникиОрганизаций.СрезПоследних(
			&ДатаНачалаПереносаРасчетныхДанных,
			Сотрудник.физлицо В (&МассивФизическихЛиц)
				И Сотрудник.ВидЗанятости = ЗНАЧЕНИЕ(Перечисление.ВидыЗанятостиВОрганизации.ОсновноеМестоРаботы)) КАК РаботникиОрганизацийСрезПоследних
;

////////////////////////////////////////////////////////////////////////////////
ВЫБРАТЬ
	СписокСотрудников.Сотрудник КАК Сотрудник,
	СписокСотрудников.Сотрудник.ПодразделениеОрганизации КАК Подразделение,
	СписокСотрудников.Физлицо КАК ФизическоеЛицо,
	СписокСотрудников.Сотрудник.Организация КАК Организация
ИЗ
	(ВЫБРАТЬ
		МАКСИМУМ(РаботникиОсновноеМесто.Сотрудник) КАК Сотрудник,
		РаботникиОсновноеМесто.Сотрудник.Физлицо КАК Физлицо,
		1 КАК Приоритет
	ИЗ
		ВТРаботникиОсновноеМесто КАК РаботникиОсновноеМесто
	ГДЕ
		РаботникиОсновноеМесто.Сотрудник.ВидДоговора = ЗНАЧЕНИЕ(Перечисление.ВидыДоговоровСФизЛицами.ТрудовойДоговор)
		И РаботникиОсновноеМесто.Сотрудник.ВидЗанятости = ЗНАЧЕНИЕ(Перечисление.ВидыЗанятостиВОрганизации.ОсновноеМестоРаботы)
		И РаботникиОсновноеМесто.ПричинаИзмененияСостояния <> ЗНАЧЕНИЕ(Перечисление.ПричиныИзмененияСостояния.Увольнение)
		И РаботникиОсновноеМесто.Сотрудник.Актуальность
	
	СГРУППИРОВАТЬ ПО
		РаботникиОсновноеМесто.Сотрудник.Физлицо
	
	ОБЪЕДИНИТЬ
	
	ВЫБРАТЬ
		МАКСИМУМ(РаботникиСовместительство.Сотрудник),
		РаботникиСовместительство.Сотрудник.Физлицо,
		2
	ИЗ
		ВТРаботникиОсновноеМесто КАК РаботникиСовместительство
	ГДЕ
		РаботникиСовместительство.Сотрудник.ВидДоговора = ЗНАЧЕНИЕ(Перечисление.ВидыДоговоровСФизЛицами.ТрудовойДоговор)
		И РаботникиСовместительство.Сотрудник.ВидЗанятости = ЗНАЧЕНИЕ(Перечисление.ВидыЗанятостиВОрганизации.Совместительство)
		И РаботникиСовместительство.ПричинаИзмененияСостояния <> ЗНАЧЕНИЕ(Перечисление.ПричиныИзмененияСостояния.Увольнение)
		И РаботникиСовместительство.Сотрудник.Актуальность
	
	СГРУППИРОВАТЬ ПО
		РаботникиСовместительство.Сотрудник.Физлицо
	
	ОБЪЕДИНИТЬ
	
	ВЫБРАТЬ
		МАКСИМУМ(СотрудникиДУ.Ссылка),
		СотрудникиДУ.Физлицо,
		3
	ИЗ
		Справочник.СотрудникиОрганизаций КАК СотрудникиДУ
	ГДЕ
		СотрудникиДУ.Актуальность
		И СотрудникиДУ.Физлицо В(&МассивФизическихЛиц)
	
	СГРУППИРОВАТЬ ПО
		СотрудникиДУ.Физлицо
	
	ОБЪЕДИНИТЬ
	
	ВЫБРАТЬ
		МАКСИМУМ(РаботникиОсновноеМесто.Сотрудник),
		РаботникиОсновноеМесто.Сотрудник.Физлицо,
		4
	ИЗ
		ВТРаботникиОсновноеМесто КАК РаботникиОсновноеМесто
	ГДЕ
		РаботникиОсновноеМесто.Сотрудник.ВидДоговора = ЗНАЧЕНИЕ(Перечисление.ВидыДоговоровСФизЛицами.ТрудовойДоговор)
		И РаботникиОсновноеМесто.Сотрудник.ВидЗанятости = ЗНАЧЕНИЕ(Перечисление.ВидыЗанятостиВОрганизации.ОсновноеМестоРаботы)
		И РаботникиОсновноеМесто.ПричинаИзмененияСостояния <> ЗНАЧЕНИЕ(Перечисление.ПричиныИзмененияСостояния.Увольнение)
		И НЕ РаботникиОсновноеМесто.Сотрудник.Актуальность
	
	СГРУППИРОВАТЬ ПО
		РаботникиОсновноеМесто.Сотрудник.Физлицо
	
	ОБЪЕДИНИТЬ
	
	ВЫБРАТЬ
		МАКСИМУМ(РаботникиСовместительство.Сотрудник),
		РаботникиСовместительство.Сотрудник.Физлицо,
		5
	ИЗ
		ВТРаботникиОсновноеМесто КАК РаботникиСовместительство
	ГДЕ
		РаботникиСовместительство.Сотрудник.ВидДоговора = ЗНАЧЕНИЕ(Перечисление.ВидыДоговоровСФизЛицами.ТрудовойДоговор)
		И РаботникиСовместительство.Сотрудник.ВидЗанятости = ЗНАЧЕНИЕ(Перечисление.ВидыЗанятостиВОрганизации.Совместительство)
		И РаботникиСовместительство.ПричинаИзмененияСостояния <> ЗНАЧЕНИЕ(Перечисление.ПричиныИзмененияСостояния.Увольнение)
		И НЕ РаботникиСовместительство.Сотрудник.Актуальность
	
	СГРУППИРОВАТЬ ПО
		РаботникиСовместительство.Сотрудник.Физлицо
	
	ОБЪЕДИНИТЬ
	
	ВЫБРАТЬ
		МАКСИМУМ(СотрудникиДУ.Ссылка),
		СотрудникиДУ.Физлицо,
		6
	ИЗ
		Справочник.СотрудникиОрганизаций КАК СотрудникиДУ
	ГДЕ
		НЕ СотрудникиДУ.Актуальность
		И СотрудникиДУ.Физлицо В(&МассивФизическихЛиц)
	
	СГРУППИРОВАТЬ ПО
		СотрудникиДУ.Физлицо) КАК СписокСотрудников
		ВНУТРЕННЕЕ СОЕДИНЕНИЕ (ВЫБРАТЬ
			СписокФизическихЛиц.Физлицо КАК Физлицо,
			МИНИМУМ(СписокФизическихЛиц.Приоритет) КАК Приоритет
		ИЗ
			(ВЫБРАТЬ
				РаботникиОсновноеМесто.Сотрудник.Физлицо КАК Физлицо,
				1 КАК Приоритет
			ИЗ
				ВТРаботникиОсновноеМесто КАК РаботникиОсновноеМесто
			ГДЕ
				РаботникиОсновноеМесто.Сотрудник.ВидДоговора = ЗНАЧЕНИЕ(Перечисление.ВидыДоговоровСФизЛицами.ТрудовойДоговор)
				И РаботникиОсновноеМесто.Сотрудник.ВидЗанятости = ЗНАЧЕНИЕ(Перечисление.ВидыЗанятостиВОрганизации.ОсновноеМестоРаботы)
				И РаботникиОсновноеМесто.ПричинаИзмененияСостояния <> ЗНАЧЕНИЕ(Перечисление.ПричиныИзмененияСостояния.Увольнение)
				И РаботникиОсновноеМесто.Сотрудник.Актуальность
			
			ОБЪЕДИНИТЬ
			
			ВЫБРАТЬ
				РаботникиСовместительство.Сотрудник.Физлицо,
				2
			ИЗ
				ВТРаботникиОсновноеМесто КАК РаботникиСовместительство
			ГДЕ
				РаботникиСовместительство.Сотрудник.ВидДоговора = ЗНАЧЕНИЕ(Перечисление.ВидыДоговоровСФизЛицами.ТрудовойДоговор)
				И РаботникиСовместительство.Сотрудник.ВидЗанятости = ЗНАЧЕНИЕ(Перечисление.ВидыЗанятостиВОрганизации.Совместительство)
				И РаботникиСовместительство.ПричинаИзмененияСостояния <> ЗНАЧЕНИЕ(Перечисление.ПричиныИзмененияСостояния.Увольнение)
				И РаботникиСовместительство.Сотрудник.Актуальность
			
			ОБЪЕДИНИТЬ
			
			ВЫБРАТЬ
				СотрудникиДУ.Физлицо,
				3
			ИЗ
				Справочник.СотрудникиОрганизаций КАК СотрудникиДУ
			ГДЕ
				СотрудникиДУ.Актуальность
				И СотрудникиДУ.Физлицо В(&МассивФизическихЛиц)
			
			ОБЪЕДИНИТЬ
			
			ВЫБРАТЬ
				РаботникиОсновноеМесто.Сотрудник.Физлицо,
				4
			ИЗ
				ВТРаботникиОсновноеМесто КАК РаботникиОсновноеМесто
			ГДЕ
				РаботникиОсновноеМесто.Сотрудник.ВидДоговора = ЗНАЧЕНИЕ(Перечисление.ВидыДоговоровСФизЛицами.ТрудовойДоговор)
				И РаботникиОсновноеМесто.Сотрудник.ВидЗанятости = ЗНАЧЕНИЕ(Перечисление.ВидыЗанятостиВОрганизации.ОсновноеМестоРаботы)
				И РаботникиОсновноеМесто.ПричинаИзмененияСостояния <> ЗНАЧЕНИЕ(Перечисление.ПричиныИзмененияСостояния.Увольнение)
				И НЕ РаботникиОсновноеМесто.Сотрудник.Актуальность
			
			ОБЪЕДИНИТЬ
			
			ВЫБРАТЬ
				РаботникиСовместительство.Сотрудник.Физлицо,
				5
			ИЗ
				ВТРаботникиОсновноеМесто КАК РаботникиСовместительство
			ГДЕ
				РаботникиСовместительство.Сотрудник.ВидДоговора = ЗНАЧЕНИЕ(Перечисление.ВидыДоговоровСФизЛицами.ТрудовойДоговор)
				И РаботникиСовместительство.Сотрудник.ВидЗанятости = ЗНАЧЕНИЕ(Перечисление.ВидыЗанятостиВОрганизации.Совместительство)
				И РаботникиСовместительство.ПричинаИзмененияСостояния <> ЗНАЧЕНИЕ(Перечисление.ПричиныИзмененияСостояния.Увольнение)
				И НЕ РаботникиСовместительство.Сотрудник.Актуальность
			
			ОБЪЕДИНИТЬ
			
			ВЫБРАТЬ
				СотрудникиДУ.Физлицо,
				6
			ИЗ
				Справочник.СотрудникиОрганизаций КАК СотрудникиДУ
			ГДЕ
				НЕ СотрудникиДУ.Актуальность
				И СотрудникиДУ.Физлицо В(&МассивФизическихЛиц)) КАК СписокФизическихЛиц
		
		СГРУППИРОВАТЬ ПО
			СписокФизическихЛиц.Физлицо) КАК СписокФизическихЛиц
		ПО СписокСотрудников.Физлицо = СписокФизическихЛиц.Физлицо
			И СписокСотрудников.Приоритет = СписокФизическихЛиц.Приоритет
