﻿ВЫБРАТЬ
	РасчетыПоСтраховымВзносам.Период,
	РасчетыПоСтраховымВзносам.ВидДвижения,
	РасчетыПоСтраховымВзносам.Организация,
	РасчетыПоСтраховымВзносам.ВидПлатежа,
	РасчетыПоСтраховымВзносам.МесяцРасчетногоПериода,
	РасчетыПоСтраховымВзносам.ОблагаетсяЕНВД,
	РасчетыПоСтраховымВзносам.ПФРПоСуммарномуТарифу,
	РасчетыПоСтраховымВзносам.ПФРСтраховая,
	РасчетыПоСтраховымВзносам.ПФРНакопительная,
	РасчетыПоСтраховымВзносам.ФСС,
	РасчетыПоСтраховымВзносам.ФФОМС,
	РасчетыПоСтраховымВзносам.ТФОМС,
	РасчетыПоСтраховымВзносам.ФССНесчастныеСлучаи,
	РасчетыПоСтраховымВзносам.ПФРПоДополнительномуТарифу,
	РасчетыПоСтраховымВзносам.ПФРНаДоплатуКПенсииШахтерам,
	РасчетыПоСтраховымВзносам.ПФРЗаЗанятыхНаПодземныхИВредныхРаботах,
	РасчетыПоСтраховымВзносам.ПФРЗаЗанятыхНаТяжелыхИПрочихРаботах,
	РасчетыПоСтраховымВзносам.Организация КАК ОрганизацияОтбор,
	НАЧАЛОПЕРИОДА(РасчетыПоСтраховымВзносам.Период, МЕСЯЦ) КАК ПериодОтбор
ИЗ
	РегистрНакопления.РасчетыПоСтраховымВзносам КАК РасчетыПоСтраховымВзносам
ГДЕ
	НАЧАЛОПЕРИОДА(РасчетыПоСтраховымВзносам.Период, МЕСЯЦ) = &ДатаПереносаРасчетныхДанных

УПОРЯДОЧИТЬ ПО
	ОрганизацияОтбор,
	ПериодОтбор
ИТОГИ ПО
	ОрганизацияОтбор,
	ПериодОтбор
