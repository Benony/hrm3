﻿ВЫБРАТЬ
	СведенияОЗаймах.Организация КАК Организация,
	НАЧАЛОПЕРИОДА(СведенияОЗаймах.НачалоПогашения, МЕСЯЦ) КАК ПериодРегистрации,
	СведенияОЗаймах.ДоговорЗайма КАК ДоговорЗайма,
	СведенияОЗаймах.НачалоПогашения КАК Период,
	СведенияОЗаймах.СуммаЗайма * ВЫБОР
		КОГДА СведенияОЗаймах.КурсДокумента = 0
			ТОГДА 1
		ИНАЧЕ СведенияОЗаймах.КурсДокумента
	КОНЕЦ / ВЫБОР
		КОГДА СведенияОЗаймах.КратностьДокумента = 0
			ТОГДА 1
		ИНАЧЕ СведенияОЗаймах.КратностьДокумента
	КОНЕЦ КАК Сумма,
	ДОБАВИТЬКДАТЕ(СведенияОЗаймах.НачалоПогашения, МЕСЯЦ, СведенияОЗаймах.СрокПогашения) КАК ДатаОкончания,
	СведенияОЗаймах.ПроцентЗаПользованиеЗаймом КАК ПроцентнаяСтавка,
	СведенияОЗаймах.СуммаЗайма / ВЫБОР
		КОГДА СведенияОЗаймах.СрокПогашения = 0
			ТОГДА 1
		ИНАЧЕ СведенияОЗаймах.СрокПогашения
	КОНЕЦ КАК РазмерПогашения,
	СведенияОЗаймах.НачислятьМатериальнуюВыгоду КАК МатериальнаяВыгодаОблагаетсяНДФЛ
ИЗ
	РегистрСведений.СведенияОЗаймах КАК СведенияОЗаймах
ГДЕ
	(&ПоВсемФизическимЛицам
			ИЛИ СведенияОЗаймах.ФизЛицо = &ФизическоеЛицо)
	И СведенияОЗаймах.ОтражатьВБухгалтерскомУчете

УПОРЯДОЧИТЬ ПО
	Организация,
	ПериодРегистрации,
	ДоговорЗайма,
	Период
ИТОГИ ПО
	Организация,
	ПериодРегистрации
