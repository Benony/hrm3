﻿Запрос = Новый Запрос;
Запрос.Текст =
"ВЫБРАТЬ ПЕРВЫЕ 1
|	ВоинскийУчет.Физлицо
|ИЗ
|	РегистрСведений.ВоинскийУчет КАК ВоинскийУчет";
ИспользоватьВоинскийУчет = Не Запрос.Выполнить().Пустой();

Запрос.Текст =
"ВЫБРАТЬ
|	ДолжностиОрганизаций.КатегорияВоинскогоУчета,
|	ДолжностиОрганизаций.КатегорияУчетаЗабронированных
|ИЗ
|	Справочник.ДолжностиОрганизаций КАК ДолжностиОрганизаций
|ГДЕ
|	(ДолжностиОрганизаций.КатегорияВоинскогоУчета <> ЗНАЧЕНИЕ(Перечисление.КатегорииДолжностейДляВоинскогоУчета.ПустаяСсылка)
|			ИЛИ ДолжностиОрганизаций.КатегорияУчетаЗабронированных <> ЗНАЧЕНИЕ(Перечисление.КатегорииДолжностейДляУчетаЗабронированныхС2011Года.ПустаяСсылка))
|
|СГРУППИРОВАТЬ ПО
|	ДолжностиОрганизаций.КатегорияВоинскогоУчета,
|	ДолжностиОрганизаций.КатегорияУчетаЗабронированных";
ИспользоватьБронированиеГраждан = Не Запрос.Выполнить().Пустой();

ИспользоватьВоинскийУчет = ИспользоватьВоинскийУчет Или ИспользоватьБронированиеГраждан;

НастройкиВоинскогоУчета = Новый Структура("ИспользоватьВоинскийУчет,ИспользоватьБронированиеГраждан",ИспользоватьВоинскийУчет,ИспользоватьБронированиеГраждан);

ВыгрузитьПоПравилу(НастройкиВоинскогоУчета, , , , "НастройкиВоинскогоУчета");
