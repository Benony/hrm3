﻿Запрос = Новый Запрос;
Запрос.Текст = 
"ВЫБРАТЬ
|	Организации.Ссылка КАК Организация,
|	Константы.ИспользуетсяТрудФармацевтов,
|	Константы.ИспользуетсяТрудЧленовЛетныхЭкипажей,
|	Константы.ИспользуетсяТрудЧленовЭкипажейМорскихСудов,
|	Константы.ИспользуетсяТрудШахтеров,
|	Константы.ИспользуютсяРаботыСДосрочнойПенсией,
|	Константы.ПрименятьРезультатыСпециальнойОценкиУсловийТруда КАК ПрименяютсяРезультатыСпециальнойОценкиУсловийТруда
|ИЗ
|	Справочник.Организации КАК Организации,
|	Константы КАК Константы";

Результат = Запрос.Выполнить();
Выборка = Результат.Выбрать();
Пока Выборка.Следующий() Цикл
	
	Настройка = Новый Структура("Организация,ИспользуетсяТрудФармацевтов,ИспользуетсяТрудЧленовЛетныхЭкипажей,ИспользуетсяТрудЧленовЭкипажейМорскихСудов,
	|ИспользуетсяТрудШахтеров,ИспользуютсяРаботыСДосрочнойПенсией,ПрименяютсяРезультатыСпециальнойОценкиУсловийТруда");
	ЗаполнитьЗначенияСвойств(Настройка,Выборка);
	
	ВыгрузитьПоПравилу(Настройка, , , , "НастройкиУчетаСтраховыхВзносов");
	
КонецЦикла;
