﻿Если Параметры.ПереноситьНачисленияПредыдущейПрограммы Тогда
	Отказ = Истина;	
Иначе	
	
	ДатаПереноса = Параметры.МесяцНачалаЭксплуатации;
	ДлинаСуток = 86400;
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = ОбъектыПереносаДанных.ВыполнитьАлгоритм("РаботникиОрганизации_ВременнаяТаблица", Параметры.Алгоритмы, Параметры, Запросы);
	
	ИдентификаторыЭлемента = Новый Массив;
	ИдентификаторыЭлемента.Добавить("БанковскиеИздержкиПоИЛ");
	ИдентификаторыЭлемента.Добавить("ИЛВПрожиточныхМинимумах");
	Запрос.УстановитьПараметр("ИдентификаторыЭлемента", ИдентификаторыЭлемента);
	Запрос.УстановитьПараметр("ДатаСреза", ДатаПереноса);
	
	Запрос.Текст = 
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	РаботникиОрганизации.ФизическоеЛицо КАК Физлицо
	|ПОМЕСТИТЬ ВТФизическиеЛица
	|ИЗ
	|	ВТРаботникиОрганизации КАК РаботникиОрганизации
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	УдержанияОрганизаций.Ссылка КАК Ссылка
	|ПОМЕСТИТЬ ВТУдержания
	|ИЗ
	|	ПланВидовРасчета.УдержанияОрганизаций КАК УдержанияОрганизаций
	|ГДЕ
	|	НЕ УдержанияОрганизаций.Ссылка В (ЗНАЧЕНИЕ(ПланВидовРасчета.УдержанияОрганизаций.ИЛПроцентом), ЗНАЧЕНИЕ(ПланВидовРасчета.УдержанияОрганизаций.ИЛПроцентомБезБЛ), ЗНАЧЕНИЕ(ПланВидовРасчета.УдержанияОрганизаций.ИЛПроцентомДоПредела), ЗНАЧЕНИЕ(ПланВидовРасчета.УдержанияОрганизаций.ИЛФиксированнойСуммой), ЗНАЧЕНИЕ(ПланВидовРасчета.УдержанияОрганизаций.ИЛПроцентомДоПределаБезБЛ), ЗНАЧЕНИЕ(ПланВидовРасчета.УдержанияОрганизаций.ИЛФиксированнойСуммойДоПредела), ЗНАЧЕНИЕ(ПланВидовРасчета.УдержанияОрганизаций.ПочтовыйСборПоИЛ))
	|	И НЕ УдержанияОрганизаций.ИдентификаторЭлемента В (&ИдентификаторыЭлемента)
	|	И НЕ УдержанияОрганизаций.СпособРасчета В (ЗНАЧЕНИЕ(Перечисление.СпособыРасчетаОплатыТруда.БанковскиеИздержки))
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ПлановыеУдержания.Регистратор,
	|	ПлановыеУдержания.Организация КАК ГоловнаяОрганизация,
	|	ПлановыеУдержания.ФизЛицо КАК ФизЛицо,
	|	ПлановыеУдержания.ВидРасчета КАК ВидРасчета,
	|	ПлановыеУдержания.Показатель1,
	|	ПлановыеУдержания.Показатель2,
	|	ПлановыеУдержания.Показатель3,
	|	ПлановыеУдержания.Показатель4,
	|	ПлановыеУдержания.Показатель5,
	|	ПлановыеУдержания.Показатель6,
	|	ПлановыеУдержания.ПериодЗавершения,
	|	ВЫБОР
	|		КОГДА ПлановыеУдержания.ДействиеЗавершения = ЗНАЧЕНИЕ(Перечисление.ВидыДействияСНачислением.Прекратить)
	|			ТОГДА ИСТИНА
	|		ИНАЧЕ ЛОЖЬ
	|	КОНЕЦ КАК Завершить,
	|	ПлановыеУдержания.Регистратор.Организация КАК Организация
	|ПОМЕСТИТЬ ВТПлановыеУдержания
	|ИЗ
	|	РегистрСведений.ПлановыеУдержанияРаботниковОрганизаций.СрезПоследних(
	|			&ДатаСреза,
	|			ВидРасчета В
	|					(ВЫБРАТЬ
	|						Удержания.Ссылка
	|					ИЗ
	|						ВТУдержания КАК Удержания)
	|				И ФизЛицо В
	|					(ВЫБРАТЬ
	|						ФизическиеЛица.Физлицо
	|					ИЗ
	|						ВТФизическиеЛица КАК ФизическиеЛица)) КАК ПлановыеУдержания
	|ГДЕ
	|	ВЫБОР
	|			КОГДА ПлановыеУдержания.ПериодЗавершения <= &ДатаСреза
	|					И ПлановыеУдержания.ПериодЗавершения <> ДАТАВРЕМЯ(1, 1, 1, 0, 0, 0)
	|				ТОГДА ПлановыеУдержания.ДействиеЗавершения
	|			ИНАЧЕ ПлановыеУдержания.Действие
	|		КОНЕЦ <> ЗНАЧЕНИЕ(Перечисление.ВидыДействияСНачислением.Прекратить)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ПлановыеУдержания.ВидРасчета
	|ИЗ
	|	ВТПлановыеУдержания КАК ПлановыеУдержания
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	УдержанияОрганизацийБазовыеВидыРасчета.ВидРасчета КАК ВидРасчета
	|ИЗ
	|	ПланВидовРасчета.УдержанияОрганизаций.БазовыеВидыРасчета КАК УдержанияОрганизацийБазовыеВидыРасчета
	|ГДЕ
	|	УдержанияОрганизацийБазовыеВидыРасчета.Ссылка В
	|			(ВЫБРАТЬ РАЗЛИЧНЫЕ
	|				Удержания.ВидРасчета
	|			ИЗ
	|				ВТПлановыеУдержания КАК Удержания)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ПлановыеУдержания.Организация КАК Организация,
	|	ПлановыеУдержания.Регистратор,
	|	ПлановыеУдержания.ФизЛицо КАК ФизическоеЛицо,
	|	ПлановыеУдержания.ВидРасчета КАК ВидРасчета,
	|	ПлановыеУдержания.Показатель1,
	|	ПлановыеУдержания.Показатель2,
	|	ПлановыеУдержания.Показатель3,
	|	ПлановыеУдержания.Показатель4,
	|	ПлановыеУдержания.Показатель5,
	|	ПлановыеУдержания.Показатель6,
	|	ПлановыеУдержания.ПериодЗавершения КАК ПериодЗавершения,
	|	ПлановыеУдержания.Завершить
	|ИЗ
	|	ВТПлановыеУдержания КАК ПлановыеУдержания
	|
	|УПОРЯДОЧИТЬ ПО
	|	Организация,
	|	ВидРасчета,
	|	ПериодЗавершения,
	|	ПлановыеУдержания.ФизЛицо";
	
	Результат = Запрос.ВыполнитьПакет();
	СписокУдержаний = Результат[3].Выгрузить();
	БазовыеВР = Результат[4].Выгрузить();
	
	ПараметрыДляЗаполнения = Новый Структура("Параметры,ВР,ТолькоПлановыеУдержания");
	ПараметрыДляЗаполнения.Параметры = Параметры;
	ПараметрыДляЗаполнения.ТолькоПлановыеУдержания = Истина;
	
	ОписанияУдержаний = Новый Соответствие;
	
	Для каждого СтрокаВР Из СписокУдержаний Цикл
		
		Если Не ЗначениеЗаполнено(СтрокаВР.ВидРасчета) Тогда
			Продолжить;
		КонецЕсли;
		
		ПараметрыДляЗаполнения.ВР = СтрокаВР.ВидРасчета;
		
		ОписаниеВР = ОбъектыПереносаДанных.ВыполнитьАлгоритм("ЗаполнитьОписаниеВР_Удержания", Параметры.Алгоритмы, ПараметрыДляЗаполнения);
		Если ЗначениеЗаполнено(ОписаниеВР.Наименование) Тогда
			ОписанияУдержаний.Вставить(СтрокаВР.ВидРасчета, ОписаниеВР);
		КонецЕсли;
		
	КонецЦикла;
	
	///////////////////////////////////
	// подготовка и выгрузка удержаний и показателей
	
	// преобразуем базовые ВР
	ПараметрыДляЗаполнения = Новый Структура("Параметры,ВР,ОписаниеНачисленийСЗ,ТолькоПлановыеНачисления,ВидДоговора");
	ПараметрыДляЗаполнения.Параметры = Параметры;
	ПараметрыДляЗаполнения.ТолькоПлановыеНачисления = Истина;
	ОписанияНачислений = Новый Соответствие;
	
	Для каждого СтрокаТЗ Из БазовыеВР Цикл
		
		ПараметрыДляЗаполнения.ВР = СтрокаТЗ.ВидРасчета;
		
		ОписаниеВР = ОбъектыПереносаДанных.ВыполнитьАлгоритм("ЗаполнитьОписаниеВР_Начисления", Параметры.Алгоритмы, ПараметрыДляЗаполнения);
		Если ЗначениеЗаполнено(ОписаниеВР.Наименование) Тогда
			ОписанияНачислений.Вставить(СтрокаТЗ.ВидРасчета, ОписаниеВР);
		КонецЕсли;
		
	КонецЦикла;
	
	УдержанияКВыгрузке = Новый Массив;
	ПоказателиКВыгрузке = Новый Массив;
	ИдентификаторыПоказателейКВыгрузке = Новый Массив;
	
	Для каждого ЭлементКлючИЗначение Из ОписанияУдержаний Цикл
		
		ВР = ЭлементКлючИЗначение.Ключ;
		ОписаниеВР = ЭлементКлючИЗначение.Значение;
		
		БазовыеВидыРасчета = Новый ТаблицаЗначений;
		БазовыеВидыРасчета.Колонки.Добавить("ВидРасчета");
		Для каждого СтрокаТЗ Из ОписаниеВР.БазовыеВидыРасчета Цикл
			
			ОпиcаниеБазовыйВР = ОписанияНачислений[СтрокаТЗ.ВидРасчета];
			Если ОпиcаниеБазовыйВР <> Неопределено Тогда
				НоваяСтрока = БазовыеВидыРасчета.Добавить();
				НоваяСтрока.ВидРасчета = ОпиcаниеБазовыйВР;
			КонецЕсли;
			
		КонецЦикла;
		
		ОписаниеВР.БазовыеВидыРасчета = БазовыеВидыРасчета;
		
		УдержанияКВыгрузке.Добавить(ОписаниеВР);
		
		Для каждого СтрокаТЗ Из ОписаниеВР.Показатели Цикл
			Если ИдентификаторыПоказателейКВыгрузке.Найти(СтрокаТЗ.Показатель.Идентификатор) = Неопределено Тогда
				ПоказателиКВыгрузке.Добавить(СтрокаТЗ.Показатель);
				ИдентификаторыПоказателейКВыгрузке.Добавить(СтрокаТЗ.Показатель.Идентификатор);
			КонецЕсли;
		КонецЦикла;
		
	КонецЦикла;
	
	Для каждого ОписаниеПоказателя Из ПоказателиКВыгрузке Цикл
		ВыгрузитьПоПравилу(ОписаниеПоказателя, , , , "Показатели_УП");
	КонецЦикла;
	
	Для каждого ОписаниеВР Из УдержанияКВыгрузке Цикл
		ВыгрузитьПоПравилу(ОписаниеВР, , , , "Удержания_УП");
	КонецЦикла;
	
	
	
	// подготовка и выгрузка удержаний и показателей
	///////////////////////////////////
	
	Выборка = Результат[5].Выбрать();
	
	Пока Выборка.СледующийПоЗначениюПоля("Организация") Цикл
		
		Организация = Выборка.Организация;
		
		Пока Выборка.СледующийПоЗначениюПоля("ВидРасчета") Цикл
			
			ВР = Выборка.ВидРасчета;
			ОписаниеВР = ОписанияУдержаний[Выборка.ВидРасчета];
			Если ОписаниеВР = Неопределено Тогда
				Продолжить;
			КонецЕсли;
			
			НовоеОписаниеВР = Новый Структура("
			|Наименование");
			
			ЗаполнитьЗначенияСвойств(НовоеОписаниеВР, ОписаниеВР);
			
			Если ОписаниеВР.КатегорияУдержания = "ДСВ" Тогда
				ИмяПравилаВыгрузки = "УдержаниеДобровольныхСтраховыхВзносов";
			ИначеЕсли ОписаниеВР.КатегорияУдержания = "ПрофсоюзныеВзносы" Тогда
				ИмяПравилаВыгрузки = "УдержаниеПрофсоюзныхВзносов";
			ИначеЕсли ОписаниеВР.КатегорияУдержания = "ПрочееУдержаниеВПользуТретьихЛиц" Тогда   
				ИмяПравилаВыгрузки = "ПостоянноеУдержаниеВПользуТретьихЛиц";
			Иначе
				Продолжить;
			КонецЕсли;
			
			Пока Выборка.СледующийПоЗначениюПоля("ПериодЗавершения") Цикл
				
				ПериодЗавершения = Выборка.ПериодЗавершения;
				Если ЗначениеЗаполнено(ПериодЗавершения) Тогда
					ПериодЗавершения = ПериодЗавершения - ДлинаСуток;
				КонецЕсли;
				
				НовыйДокумент = Новый Структура;
				НовыйДокумент.Вставить("Организация",	Организация);
				НовыйДокумент.Вставить("Дата",		    ДатаПереноса);
				НовыйДокумент.Вставить("Удержание",		НовоеОписаниеВР);
				НовыйДокумент.Вставить("Действие",		"Начать");
				НовыйДокумент.Вставить("ДатаНачала",	ДатаПереноса);
				НовыйДокумент.Вставить("ДатаОкончания",	ПериодЗавершения);
				
				Если ОписаниеВР.КатегорияУдержания = "ДСВ" Тогда
					НовыйДокумент.Вставить("НеПредоставлятьСоциальныйВычетУРаботодателя",  Истина);
				ИначеЕсли ОписаниеВР.КатегорияУдержания = "ПрофсоюзныеВзносы" Тогда
					НовыйДокумент.Вставить("ПрофсоюзнаяОрганизация",  "");	
				Иначе
					НовыйДокумент.Вставить("Контрагент",  "");	
				КонецЕсли;
				
				ТабУдержания = Новый ТаблицаЗначений;
				ТабУдержания.Колонки.Добавить("ФизическоеЛицо");
				ТабУдержания.Колонки.Добавить("Размер");
				ТабУдержания.Колонки.Добавить("ИдентификаторСтрокиВидаРасчета");
				
				ТабПоказатели = Новый ТаблицаЗначений;
				ТабПоказатели.Колонки.Добавить("Показатель");
				ТабПоказатели.Колонки.Добавить("Значение");
				ТабПоказатели.Колонки.Добавить("ИдентификаторСтрокиВидаРасчета");
				
				ИдентификаторСтрокиВидаРасчета = 0;
				
				Пока Выборка.Следующий() Цикл
					
					ИдентификаторСтрокиВидаРасчета = ИдентификаторСтрокиВидаРасчета + 1;
					
					НоваяСтрока = ТабУдержания.Добавить();
					НоваяСтрока.ФизическоеЛицо = Выборка.ФизическоеЛицо;
					НоваяСтрока.ИдентификаторСтрокиВидаРасчета = ИдентификаторСтрокиВидаРасчета;
					
					Если ЗначениеЗаполнено(ОписаниеВР.ГдеЗначение) Тогда
						НоваяСтрока.Размер = Выборка[ОписаниеВР.ГдеЗначение];
					Иначе
						
						Для каждого СтрокаТЗПоказатели Из ОписаниеВР.Показатели Цикл
							
							ОписаниеПоказателя = СтрокаТЗПоказатели.Показатель;
							
							ИмяПоляЗначения = Неопределено;
							ОписаниеВР.ГдеЗначенияПоказателей.Свойство(ОписаниеПоказателя.Идентификатор, ИмяПоляЗначения);
							Если ИмяПоляЗначения = Неопределено Тогда
								Продолжить;
							КонецЕсли;
							
							ЗначениеПоказателя = Выборка[ИмяПоляЗначения];
							Если ЗначениеЗаполнено(ЗначениеПоказателя) Тогда
								НоваяСтрокаПоказателя = ТабПоказатели.Добавить();
								НоваяСтрокаПоказателя.Показатель = ОписаниеПоказателя;
								НоваяСтрокаПоказателя.Значение = ЗначениеПоказателя;
								НоваяСтрокаПоказателя.ИдентификаторСтрокиВидаРасчета = ИдентификаторСтрокиВидаРасчета;
							КонецЕсли;
							
						КонецЦикла;
						
					КонецЕсли;
					
				КонецЦикла;
				
				НовыйДокумент.Вставить("Удержания",  ТабУдержания);
				НовыйДокумент.Вставить("Показатели", ТабПоказатели);
				
				ВыгрузитьПоПравилу(НовыйДокумент, , , , ИмяПравилаВыгрузки);
				
			КонецЦикла;
			
		КонецЦикла;
		
	КонецЦикла;
	
	
КонецЕсли;
