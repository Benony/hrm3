﻿Если Параметры.ПереноситьНачисленияПредыдущейПрограммы Тогда
	ДатаОкончания = ОбъектыПереносаДанных.ВыполнитьАлгоритм("ПолучитьДатуОкончанияПоРегиструОсновныеНачисления", Параметры.Алгоритмы, Параметры);
	ДатаНачалаПереноса = Параметры.ДатаНачалаПереносаРасчетныхДанных;
Иначе
	ДатаНачалаПереноса = НачалоГода(Параметры.МесяцНачалаЭксплуатации);
	ДатаОкончания = НачалоМесяца(ДобавитьМесяц(Параметры.МесяцНачалаЭксплуатации, -1));
КонецЕсли;

Запрос = Новый Запрос;
Запрос.МенеджерВременныхТаблиц = ОбъектыПереносаДанных.ВыполнитьАлгоритм("ФизическиеЛица_ВременнаяТаблица", Параметры.Алгоритмы, Параметры, Запросы);

Запрос.Текст = 
"ВЫБРАТЬ
|	АвансовыеПлатежи.Период КАК Период,
|	АвансовыеПлатежи.Организация КАК Организация,
|	АвансовыеПлатежи.ФизЛицо,
|	АвансовыеПлатежи.Год,
|	АвансовыеПлатежи.ОбособленноеПодразделение,
|	АвансовыеПлатежи.Сумма,
|	АвансовыеПлатежи.ПодразделениеОрганизации,
|	АвансовыеПлатежи.СтавкаНалогообложенияРезидента,
|	АвансовыеПлатежи.МесяцНалоговогоПериода,
|	АвансовыеПлатежи.КодДохода,
|	АвансовыеПлатежи.КПП,
|	АвансовыеПлатежи.КодПоОКТМО,
|	АвансовыеПлатежи.ВидДвижения
|ИЗ
|	РегистрНакопления.АвансовыеПлатежиИностранцевПоНДФЛ КАК АвансовыеПлатежи
|ГДЕ
|	АвансовыеПлатежи.ФизЛицо В
|			(ВЫБРАТЬ РАЗЛИЧНЫЕ
|				ФизическиеЛица.Физлицо
|			ИЗ
|				ВТФизическиеЛица КАК ФизическиеЛица)
|	И НАЧАЛОПЕРИОДА(АвансовыеПлатежи.Период, МЕСЯЦ) = &ДатаПереносаРасчетныхДанных
|
|УПОРЯДОЧИТЬ ПО
|	АвансовыеПлатежи.ОбособленноеПодразделение";

ДатаПереноса = ДатаНачалаПереноса;

Таб = Новый ТаблицаЗначений;
Таб.Колонки.Добавить("Период");
Таб.Колонки.Добавить("ГоловнаяОрганизация");
Таб.Колонки.Добавить("ФизическоеЛицо");
Таб.Колонки.Добавить("Год");
Таб.Колонки.Добавить("Организация");
Таб.Колонки.Добавить("Сумма");
Таб.Колонки.Добавить("Подразделение");
Таб.Колонки.Добавить("РегистрацияВНалоговомОргане");
Таб.Колонки.Добавить("СтавкаНалогообложенияРезидента");
Таб.Колонки.Добавить("МесяцНалоговогоПериода");
Таб.Колонки.Добавить("КодДохода");
Таб.Колонки.Добавить("ВидДвижения");


Пока ДатаПереноса <= ДатаОкончания Цикл
	
	Запрос.УстановитьПараметр("ДатаПереносаРасчетныхДанных",	ДатаПереноса);
	
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.СледующийПоЗначениюПоля("ОбособленноеПодразделение") Цикл
		
		ОрганизацияОтбор = Выборка.ОбособленноеПодразделение;
		АвансовыеПлатежиИностранцевПоНДФЛ = Таб.СкопироватьКолонки();
		
		Пока Выборка.Следующий() Цикл
			
			РегистрацияВНалоговомОргане = ОбъектыПереносаДанных.ВыполнитьАлгоритм(
			"РегистрацияВНалоговомОргане_Заполнить",
			Параметры.Алгоритмы,
			Выборка.Организация,
			Выборка.ПодразделениеОрганизации,
			Выборка.КодПоОКТМО,
			"",
			Выборка.КПП);
			
			НоваяСтрока = АвансовыеПлатежиИностранцевПоНДФЛ.Добавить();
			НоваяСтрока.Период = Выборка.Период;
			НоваяСтрока.ГоловнаяОрганизация = Выборка.Организация;
			НоваяСтрока.ФизическоеЛицо = Выборка.ФизЛицо;
			НоваяСтрока.Год = Выборка.Год;
			НоваяСтрока.Организация = Выборка.ОбособленноеПодразделение;
			НоваяСтрока.Сумма = Выборка.Сумма;
			НоваяСтрока.Подразделение = Выборка.ПодразделениеОрганизации;
			НоваяСтрока.РегистрацияВНалоговомОргане = РегистрацияВНалоговомОргане;
			НоваяСтрока.СтавкаНалогообложенияРезидента = Выборка.СтавкаНалогообложенияРезидента;
			НоваяСтрока.МесяцНалоговогоПериода = Выборка.МесяцНалоговогоПериода;
			НоваяСтрока.КодДохода = Выборка.КодДохода;
			НоваяСтрока.ВидДвижения = Выборка.ВидДвижения;
			
		КонецЦикла;
		
		ВыборкаПоДокументу = Новый Структура();
		ВыборкаПоДокументу.Вставить("Номер",					"НДФЛ");
		ВыборкаПоДокументу.Вставить("Организация",				ОрганизацияОтбор);
		ВыборкаПоДокументу.Вставить("ПериодРегистрации",		ДатаПереноса);
		ВыборкаПоДокументу.Вставить("АвансовыеПлатежиИностранцевПоНДФЛ",	    АвансовыеПлатежиИностранцевПоНДФЛ);
		
		ВыгрузитьПоПравилу(ВыборкаПоДокументу, , , , "АвансовыеПлатежиИностранцевПоНДФЛ");
		
	КонецЦикла;
	
	ДатаПереноса = ДобавитьМесяц(ДатаПереноса, 1);
	
КонецЦикла;

Запрос.УстановитьПараметр("ДатаНачалаПереноса", ДатаНачалаПереноса);
Запрос.Текст = 
"ВЫБРАТЬ
|	ФизическиеЛица.ФизЛицо,
|	АвансовыеПлатежиИностранцевПоНДФЛ.Период,
|	АвансовыеПлатежиИностранцевПоНДФЛ.Регистратор,
|	АвансовыйПлатежИностранцаПоНДФЛ.НалоговыйПериод
|ПОМЕСТИТЬ ВТСтрокиАвансовыхПлатежей
|ИЗ
|	ВТФизическиеЛица КАК ФизическиеЛица
|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрНакопления.АвансовыеПлатежиИностранцевПоНДФЛ КАК АвансовыеПлатежиИностранцевПоНДФЛ
|			ЛЕВОЕ СОЕДИНЕНИЕ Документ.АвансовыйПлатежИностранцаПоНДФЛ КАК АвансовыйПлатежИностранцаПоНДФЛ
|			ПО АвансовыеПлатежиИностранцевПоНДФЛ.Регистратор = АвансовыйПлатежИностранцаПоНДФЛ.Ссылка
|		ПО ФизическиеЛица.ФизЛицо = АвансовыеПлатежиИностранцевПоНДФЛ.ФизЛицо
|			И (АвансовыеПлатежиИностранцевПоНДФЛ.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход))
|ГДЕ
|	АвансовыйПлатежИностранцаПоНДФЛ.Ссылка ЕСТЬ НЕ NULL 
|	И АвансовыеПлатежиИностранцевПоНДФЛ.Период >= &ДатаНачалаПереноса
|;
|
|////////////////////////////////////////////////////////////////////////////////
|ВЫБРАТЬ
|	ФизическиеЛица.ФизЛицо,
|	МИНИМУМ(АвансовыеПлатежиИностранцевПоНДФЛ.Период) КАК Период,
|	АвансовыеПлатежиИностранцевПоНДФЛ.НалоговыйПериод
|ПОМЕСТИТЬ ВТДатыРегистрацииУведомленийАвансовыеПлатежи
|ИЗ
|	ВТФизическиеЛица КАК ФизическиеЛица
|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ВТСтрокиАвансовыхПлатежей КАК АвансовыеПлатежиИностранцевПоНДФЛ
|		ПО ФизическиеЛица.ФизЛицо = АвансовыеПлатежиИностранцевПоНДФЛ.ФизЛицо
|
|СГРУППИРОВАТЬ ПО
|	ФизическиеЛица.ФизЛицо,
|	АвансовыеПлатежиИностранцевПоНДФЛ.НалоговыйПериод
|;
|
|////////////////////////////////////////////////////////////////////////////////
|ВЫБРАТЬ
|	СотрудникиОрганизаций.ФизЛицо,
|	МИНИМУМ(АвансовыеПлатежиИностранцевПоНДФЛ.Регистратор) КАК Регистратор
|ПОМЕСТИТЬ ВТДокументыУведомленийАвансовыеПлатежи
|ИЗ
|	ВТДатыРегистрацииУведомленийАвансовыеПлатежи КАК СотрудникиОрганизаций
|		ЛЕВОЕ СОЕДИНЕНИЕ ВТСтрокиАвансовыхПлатежей КАК АвансовыеПлатежиИностранцевПоНДФЛ
|		ПО СотрудникиОрганизаций.ФизЛицо = АвансовыеПлатежиИностранцевПоНДФЛ.ФизЛицо
|			И СотрудникиОрганизаций.Период = АвансовыеПлатежиИностранцевПоНДФЛ.Период
|
|СГРУППИРОВАТЬ ПО
|	СотрудникиОрганизаций.ФизЛицо
|;
|
|////////////////////////////////////////////////////////////////////////////////
|ВЫБРАТЬ
|	ВТДокументыУведомленийАвансовыеПлатежи.ФизЛицо КАК ФизическоеЛицо,
|	ВЫБОР
|		КОГДА ВТДокументыУведомленийАвансовыеПлатежи.Регистратор.Организация.ГоловнаяОрганизация = ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка)
|			ТОГДА ВТДокументыУведомленийАвансовыеПлатежи.Регистратор.Организация
|		ИНАЧЕ ВТДокументыУведомленийАвансовыеПлатежи.Регистратор.Организация.ГоловнаяОрганизация
|	КОНЕЦ КАК ГоловнаяОрганизация,
|	ВТДокументыУведомленийАвансовыеПлатежи.Регистратор.ДатаУведомления КАК ДатаУведомления,
|	ВТДокументыУведомленийАвансовыеПлатежи.Регистратор.НомерВходящегоДокумента КАК НомерУведомления,
|	ВТДокументыУведомленийАвансовыеПлатежи.Регистратор.КодНалоговогоОргана КАК КодНалоговогоОргана,
|	ВТДокументыУведомленийАвансовыеПлатежи.Регистратор.НалоговыйПериод КАК Год
|ИЗ
|	ВТДокументыУведомленийАвансовыеПлатежи КАК ВТДокументыУведомленийАвансовыеПлатежи";
Выборка = Запрос.Выполнить().Выбрать();

ОписаниеПолей = "Год,ГоловнаяОрганизация,ФизическоеЛицо,ДатаУведомления,КодНалоговогоОргана,НомерУведомления";
Пока Выборка.Следующий() Цикл
	РеквизитыУведомления = Новый Структура(ОписаниеПолей);
	ЗаполнитьЗначенияСвойств(РеквизитыУведомления, Выборка);
	ВыгрузитьПоПравилу(РеквизитыУведомления, , , , "РеквизитыУведомленияИФНСНаЗачетАвансовыхПлатежей");
КонецЦикла;
