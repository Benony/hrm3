﻿Запрос = Новый Запрос;

Если Параметры.ПереноситьНачисленияПредыдущейПрограммы Тогда
	ДатаОкончанияПереноса = ОбъектыПереносаДанных.ВыполнитьАлгоритм("ПолучитьДатуОкончанияПоРегиструОсновныеНачисления", Параметры.Алгоритмы, Параметры);
	ДатаНачалаПереноса = Параметры.ДатаНачалаПереносаРасчетныхДанных;
	Запрос.МенеджерВременныхТаблиц = ОбъектыПереносаДанных.ВыполнитьАлгоритм("Сотрудники_ВременнаяТаблица", Параметры.Алгоритмы, Параметры, Запросы);
	УсловиеЗапроса = "И ПособияПоУходуЗаРебенкомДоПолутораЛет.Сотрудник В (ВЫБРАТЬ РАЗЛИЧНЫЕ Сотрудники.Сотрудник ИЗ ВТСотрудники КАК Сотрудники)";
Иначе
	ДатаНачалаПереноса = НачалоГода(Параметры.МесяцНачалаЭксплуатации);
	ДатаОкончанияПереноса = НачалоМесяца(ДобавитьМесяц(Параметры.МесяцНачалаЭксплуатации, -1));
	УсловиеЗапроса = "";
КонецЕсли;

Запрос.УстановитьПараметр("ДатаНачалаПереноса",	ДатаНачалаПереноса);
Запрос.УстановитьПараметр("ДатаОкончанияПереноса",	ДатаОкончанияПереноса);

Запрос.Текст = 
"ВЫБРАТЬ
|	ПособияПоУходуЗаРебенкомДоПолутораЛет.Период,
|	ПособияПоУходуЗаРебенкомДоПолутораЛет.Организация,
|	ПособияПоУходуЗаРебенкомДоПолутораЛет.ФизЛицо КАК ФизЛицо,
|	ПособияПоУходуЗаРебенкомДоПолутораЛет.ОблагаетсяЕНВД,
|	ПособияПоУходуЗаРебенкомДоПолутораЛет.ВидЗанятости,
|	ПособияПоУходуЗаРебенкомДоПолутораЛет.УходЗаПервымРебенком,
|	ПособияПоУходуЗаРебенкомДоПолутораЛет.СтраховыеСлучаи,
|	ПособияПоУходуЗаРебенкомДоПолутораЛет.СуммаВсего,
|	ПособияПоУходуЗаРебенкомДоПолутораЛет.СуммаСверхНорм,
|	ПособияПоУходуЗаРебенкомДоПолутораЛет.ВыплатаЗаСчетФедеральногоБюджета,
|	ПособияПоУходуЗаРебенкомДоПолутораЛет.Сотрудник КАК Сотрудник,
|	ПособияПоУходуЗаРебенкомДоПолутораЛет.ДатаСтраховогоСлучая,
|	ПособияПоУходуЗаРебенкомДоПолутораЛет.Организация КАК ОрганизацияОтбор,
|	НАЧАЛОПЕРИОДА(ПособияПоУходуЗаРебенкомДоПолутораЛет.Период, МЕСЯЦ) КАК ПериодОтбор
|ИЗ
|	РегистрНакопления.ПособияПоУходуЗаРебенкомДоПолутораЛет КАК ПособияПоУходуЗаРебенкомДоПолутораЛет
|ГДЕ
|	НАЧАЛОПЕРИОДА(ПособияПоУходуЗаРебенкомДоПолутораЛет.Период, МЕСЯЦ) МЕЖДУ &ДатаНачалаПереноса И &ДатаОкончанияПереноса
|	И ИСТИНА
|
|УПОРЯДОЧИТЬ ПО
|	ОрганизацияОтбор,
|	ПериодОтбор,
|	ФизЛицо,
|	Сотрудник";

Запрос.Текст = СтрЗаменить(Запрос.Текст, "И ИСТИНА", УсловиеЗапроса);

Результат = Запрос.Выполнить();
Выборка = Результат.Выбрать();

Пока Выборка.СледующийПоЗначениюПоля("ОрганизацияОтбор") Цикл
	
	Пока Выборка.СледующийПоЗначениюПоля("ПериодОтбор") Цикл
		
		
		Таб = Новый ТаблицаЗначений;
		
		Таб.Колонки.Добавить("Сотрудник");
		Таб.Колонки.Добавить("ФизическоеЛицо");
		Таб.Колонки.Добавить("Организация");
		Таб.Колонки.Добавить("Период");
		Таб.Колонки.Добавить("ВидЗанятости");
		Таб.Колонки.Добавить("ФинансированиеФедеральнымБюджетом");
		Таб.Колонки.Добавить("ДатаСтраховогоСлучая");
		Таб.Колонки.Добавить( "СтраховыеСлучаи");
		Таб.Колонки.Добавить("СтраховыеСлучаиПоУходуЗаПервымРебенком");
		Таб.Колонки.Добавить("СуммаВсегоПоУходуЗаПервымРебенком");
		Таб.Колонки.Добавить("СуммаСверхНормПоУходуЗаПервымРебенком");
		Таб.Колонки.Добавить( "СтраховыеСлучаиПоУходуЗаВторымРебенком");
		Таб.Колонки.Добавить("СуммаВсегоПоУходуЗаВторымРебенком");
		Таб.Колонки.Добавить("СуммаСверхНормПоУходуЗаВторымРебенком");
		
		Пока Выборка.Следующий() Цикл
			
			НоваяСтрока = Таб.Добавить();
			НоваяСтрока.Сотрудник									= Выборка.Сотрудник;
			НоваяСтрока.ФизическоеЛицо								= Выборка.ФизЛицо;
			НоваяСтрока.Организация									= Выборка.Организация;
			НоваяСтрока.Период										= НачалоДня(Выборка.Период);
			НоваяСтрока.ВидЗанятости								= Выборка.ВидЗанятости;
			НоваяСтрока.ФинансированиеФедеральнымБюджетом			= Выборка.ВыплатаЗаСчетФедеральногоБюджета;
			НоваяСтрока.ДатаСтраховогоСлучая						= Выборка.ДатаСтраховогоСлучая;
			НоваяСтрока.СтраховыеСлучаи								= Выборка.СтраховыеСлучаи;
			
			Если Выборка.УходЗаПервымРебенком = 1 Тогда
				НоваяСтрока.СтраховыеСлучаиПоУходуЗаПервымРебенком	= Выборка.СтраховыеСлучаи;
				НоваяСтрока.СуммаВсегоПоУходуЗаПервымРебенком		= Выборка.СуммаВсего;
				НоваяСтрока.СуммаСверхНормПоУходуЗаПервымРебенком	= Выборка.СуммаСверхНорм;
			Иначе
				НоваяСтрока.СтраховыеСлучаиПоУходуЗаВторымРебенком	= Выборка.СтраховыеСлучаи;
				НоваяСтрока.СуммаВсегоПоУходуЗаВторымРебенком		= Выборка.СуммаВсего;
				НоваяСтрока.СуммаСверхНормПоУходуЗаВторымРебенком	= Выборка.СуммаСверхНорм;
			КонецЕсли;
		КонецЦикла;
		
		ВыборкаПоДокументу = Новый Структура;
		ВыборкаПоДокументу.Вставить("Номер",						"ПСС");
		ВыборкаПоДокументу.Вставить("Организация",					Выборка.ОрганизацияОтбор);
		ВыборкаПоДокументу.Вставить("ПериодРегистрации",			Выборка.ПериодОтбор);
		ВыборкаПоДокументу.Вставить("ПособияПоУходуЗаРебенком",		Таб);
		
		ВыгрузитьПоПравилу(ВыборкаПоДокументу, , , , "ПособияПоУходуЗаРебенком_ПД");
		
	КонецЦикла;
	
КонецЦикла;
