﻿Если Параметры.ПереноситьНачисленияПредыдущейПрограммы Тогда
	
	ДатаОкончания = ОбъектыПереносаДанных.ВыполнитьАлгоритм("ПолучитьДатуОкончанияПоРегиструОсновныеНачисления", Параметры.Алгоритмы, Параметры);
	ДатаНачалаРаботыСПрограммой = ДобавитьМесяц(ДатаОкончания, 1);
	
	МесяцНачалаПереноса = ДобавитьМесяц(НачалоГода(ДатаОкончания), -36);
	МесяцНачалаПереноса = Мин(МесяцНачалаПереноса,Параметры.ДатаНачалаПереносаРасчетныхДанных);
	МесяцОкончанияПереноса = ДатаОкончания;
	
Иначе
	
	ДатаНачалаРаботыСПрограммой = НачалоМесяца(Параметры.МесяцНачалаЭксплуатации);
	МесяцНачалаПереноса = ДобавитьМесяц(ДатаНачалаРаботыСПрограммой, -36);
	МесяцОкончанияПереноса = ДобавитьМесяц(ДатаНачалаРаботыСПрограммой, -1);
	
КонецЕсли;

Запрос = Новый Запрос;
Запрос.МенеджерВременныхТаблиц = ОбъектыПереносаДанных.ВыполнитьАлгоритм("Сотрудники_ВременнаяТаблица_ДляФСС", Параметры.Алгоритмы, Параметры, Запросы);

Запрос.Текст = 
"ВЫБРАТЬ
|	ОсновныеНачисленияОрганизаций.Ссылка
|ПОМЕСТИТЬ ВТДниБолезниУхода
|ИЗ
|	ПланВидовРасчета.ОсновныеНачисленияОрганизаций КАК ОсновныеНачисленияОрганизаций
|ГДЕ
|	ОсновныеНачисленияОрганизаций.ЗачетНормыВремени
|	И НЕ ОсновныеНачисленияОрганизаций.ЗачетОтработанногоВремени
|	И НЕ ЕСТЬNULL(ОсновныеНачисленияОрганизаций.КодДоходаСтраховыеВзносы.ВходитВБазуФСС, ЛОЖЬ)
|	И ОсновныеНачисленияОрганизаций.СпособРасчета <> ЗНАЧЕНИЕ(Перечисление.СпособыРасчетаОплатыТруда.НулеваяСумма)
|	И НЕ ОсновныеНачисленияОрганизаций.ОбозначениеВТабелеУчетаРабочегоВремени В (ЗНАЧЕНИЕ(Справочник.КлассификаторИспользованияРабочегоВремени.Прогулы), ЗНАЧЕНИЕ(Справочник.КлассификаторИспользованияРабочегоВремени.НеоплачиваемыйОтпускПоРазрешениюРаботодателя), ЗНАЧЕНИЕ(Справочник.КлассификаторИспользованияРабочегоВремени.НеоплачиваемыйОтпускПоЗаконодательству), ЗНАЧЕНИЕ(Справочник.КлассификаторИспользованияРабочегоВремени.НеоплачиваемыйДополнительныйОтпуск), ЗНАЧЕНИЕ(Справочник.КлассификаторИспользованияРабочегоВремени.ОтпускНаОбучениеНеоплачиваемый), ЗНАЧЕНИЕ(Справочник.КлассификаторИспользованияРабочегоВремени.ОтстранениеОтРаботыБезОплаты))
|	И ОсновныеНачисленияОрганизаций.КодДоходаСтраховыеВзносы <> ЗНАЧЕНИЕ(Справочник.ДоходыПоСтраховымВзносам.РаспределятьПоБазовымНачислениям)
|
|ОБЪЕДИНИТЬ
|
|ВЫБРАТЬ
|	ЗНАЧЕНИЕ(ПланВидовРасчета.ОсновныеНачисленияОрганизаций.ОтсутствиеПоБолезни)
|
|ОБЪЕДИНИТЬ
|
|ВЫБРАТЬ
|	ЗНАЧЕНИЕ(ПланВидовРасчета.ОсновныеНачисленияОрганизаций.ОтсутствиеПоБолезниПоБеременности)
|
|ОБЪЕДИНИТЬ
|
|ВЫБРАТЬ
|	ЗНАЧЕНИЕ(ПланВидовРасчета.ОсновныеНачисленияОрганизаций.ОтпускПоУходуЗаРебенкомБезОплаты)
|
|ОБЪЕДИНИТЬ
|
|ВЫБРАТЬ
|	ЗНАЧЕНИЕ(ПланВидовРасчета.ОсновныеНачисленияОрганизаций.ПособиеПоУходуЗаРебенкомДо1_5Лет)
|
|ОБЪЕДИНИТЬ
|
|ВЫБРАТЬ
|	ЗНАЧЕНИЕ(ПланВидовРасчета.ОсновныеНачисленияОрганизаций.ПособиеПоУходуЗаРебенкомДо3Лет)
|;
|
|////////////////////////////////////////////////////////////////////////////////
|ВЫБРАТЬ
|	Начисления.Ссылка КАК ВидРасчета
|ПОМЕСТИТЬ ВТОтработанныеДни
|ИЗ
|	ПланВидовРасчета.ОсновныеНачисленияОрганизаций КАК Начисления
|ГДЕ
|	Начисления.ЗачетОтработанногоВремени
|;
|
|////////////////////////////////////////////////////////////////////////////////
|ВЫБРАТЬ
|	Начисления.Ссылка КАК ВидРасчета
|ПОМЕСТИТЬ ВТНеОтработанныеДни
|ИЗ
|	ПланВидовРасчета.ОсновныеНачисленияОрганизаций КАК Начисления
|		ЛЕВОЕ СОЕДИНЕНИЕ ВТДниБолезниУхода КАК ВТДниБолезниУхода
|		ПО Начисления.Ссылка = ВТДниБолезниУхода.Ссылка
|ГДЕ
|	НЕ Начисления.ЗачетОтработанногоВремени
|	И Начисления.ЗачетНормыВремени
|	И ВТДниБолезниУхода.Ссылка ЕСТЬ NULL 
|;
|
|////////////////////////////////////////////////////////////////////////////////
|ВЫБРАТЬ
|	ВТДниБолезниУхода.Ссылка КАК ВидРасчета,
|	ИСТИНА КАК ДниБолезни,
|	ЛОЖЬ КАК ОтработанныеДни
|ПОМЕСТИТЬ ВТВидыРасчетаОтбор
|ИЗ
|	ВТДниБолезниУхода КАК ВТДниБолезниУхода
|
|ОБЪЕДИНИТЬ ВСЕ
|
|ВЫБРАТЬ
|	ВТОтработанныеДни.ВидРасчета,
|	ЛОЖЬ,
|	ИСТИНА
|ИЗ
|	ВТОтработанныеДни КАК ВТОтработанныеДни
|
|ОБЪЕДИНИТЬ ВСЕ
|
|ВЫБРАТЬ
|	ВТНеОтработанныеДни.ВидРасчета,
|	ЛОЖЬ,
|	ЛОЖЬ
|ИЗ
|	ВТНеОтработанныеДни КАК ВТНеОтработанныеДни
|
|ИНДЕКСИРОВАТЬ ПО
|	ВидРасчета
|;
|
|////////////////////////////////////////////////////////////////////////////////
|ВЫБРАТЬ
|	ВТВидыРасчетаОтбор.ВидРасчета,
|	ВТВидыРасчетаОтбор.ДниБолезни,
|	ВТВидыРасчетаОтбор.ОтработанныеДни,
|	ОсновныеНачисления.ВидПособияСоциальногоСтрахования,
|	ОсновныеНачисления.ИдентификаторЭлемента
|ИЗ
|	ВТВидыРасчетаОтбор КАК ВТВидыРасчетаОтбор
|		ЛЕВОЕ СОЕДИНЕНИЕ ПланВидовРасчета.ОсновныеНачисленияОрганизаций КАК ОсновныеНачисления
|		ПО ВТВидыРасчетаОтбор.ВидРасчета = ОсновныеНачисления.Ссылка";
СвойстваНачислений = Запрос.Выполнить().Выгрузить();
СвойстваНачислений.Колонки.Добавить("ОписаниеВР");

ОписаниеСвойствНачислений = ОбъектыПереносаДанных.ВыполнитьАлгоритм("СвойстваНачисленийПоКатегориям", Параметры.Алгоритмы, Параметры);

Для каждого СтрокаТЗ Из СвойстваНачислений Цикл
	
	Если СтрокаТЗ.ДниБолезни Тогда
		
		ИмяПредопределенного  = ПланыВидовРасчета.ОсновныеНачисленияОрганизаций.ПолучитьИмяПредопределенного(СтрокаТЗ.ВидРасчета);
		ИдентификаторЭлемента = СтрокаТЗ.ИдентификаторЭлемента;
		
		Если (ИмяПредопределенного = "ПособиеПоУходуЗаРебенкомДо3Лет" Или ИмяПредопределенного = "ОтпускПоУходуЗаРебенкомБезОплаты") Тогда
			КатегорияНачисленияИлиНеоплаченногоВремени = "ПособиеПоУходуЗаРебенкомДоТрехЛет";
		ИначеЕсли ИмяПредопределенного = "ПособиеПоУходуЗаРебенкомДо1_5Лет" Тогда
			КатегорияНачисленияИлиНеоплаченногоВремени = "ПособиеПоУходуЗаРебенкомДоПолутораЛет";
		ИначеЕсли ИмяПредопределенного = "ОтпускПоБеременностиИРодам" Тогда
			КатегорияНачисленияИлиНеоплаченногоВремени = "ОтпускПоБеременностиИРодам";
		ИначеЕсли ИмяПредопределенного = "ОтсутствиеПоБолезниПоБеременности" Тогда
			КатегорияНачисленияИлиНеоплаченногоВремени = "ОтпускПоБеременностиИРодамБезОплаты";
		ИначеЕсли ИмяПредопределенного = "ОплатаПоСреднемуБЛ" Тогда
			КатегорияНачисленияИлиНеоплаченногоВремени = "ОплатаБольничногоЛиста";
		ИначеЕсли ИмяПредопределенного = "ОтсутствиеПоБолезни" Тогда	
			КатегорияНачисленияИлиНеоплаченногоВремени = "БолезньБезОплаты";
		ИначеЕсли ИмяПредопределенного = "ОплатаБЛПоТравмеНаПроизводстве" Тогда
			КатегорияНачисленияИлиНеоплаченногоВремени = "ОплатаБольничногоНесчастныйСлучайНаПроизводстве";
		ИначеЕсли ИмяПредопределенного = "ОплатаВыходныхДнейПоУходуЗаДетьмиИнвалидами" Тогда
			КатегорияНачисленияИлиНеоплаченногоВремени = "ОплатаДнейУходаЗаДетьмиИнвалидами";
		ИначеЕсли ИдентификаторЭлемента = "ОплатаБолезниЗаСчетРаботодателя" Тогда
			КатегорияНачисленияИлиНеоплаченногоВремени = "ОплатаБольничногоЛистаЗаСчетРаботодателя";
		ИначеЕсли СтрокаТЗ.ВидПособияСоциальногоСтрахования = Перечисления.ВидыПособийСоциальногоСтрахования.БеременностьРоды Тогда
			КатегорияНачисленияИлиНеоплаченногоВремени = "ОтпускПоБеременностиИРодам";
		ИначеЕсли СтрокаТЗ.ВидПособияСоциальногоСтрахования = Перечисления.ВидыПособийСоциальногоСтрахования.Нетрудоспособность Тогда
			КатегорияНачисленияИлиНеоплаченногоВремени = "ОплатаБольничногоЛиста";
		ИначеЕсли СтрокаТЗ.ВидПособияСоциальногоСтрахования = Перечисления.ВидыПособийСоциальногоСтрахования.НетрудоспособностьНесчастныйСлучай Тогда
			КатегорияНачисленияИлиНеоплаченногоВремени = "ОплатаБольничногоНесчастныйСлучайНаПроизводстве";
		ИначеЕсли СтрокаТЗ.ВидПособияСоциальногоСтрахования = Перечисления.ВидыПособийСоциальногоСтрахования.НетрудоспособностьПрофзаболевание Тогда
			КатегорияНачисленияИлиНеоплаченногоВремени = "ОплатаБольничногоПрофзаболевание";
		ИначеЕсли СтрокаТЗ.ВидПособияСоциальногоСтрахования = Перечисления.ВидыПособийСоциальногоСтрахования.ПоУходуЗаРебенкомДоПолутораЛет Тогда
			КатегорияНачисленияИлиНеоплаченногоВремени = "ПособиеПоУходуЗаРебенкомДоПолутораЛет";
		ИначеЕсли СтрокаТЗ.ВидПособияСоциальногоСтрахования = Перечисления.ВидыПособийСоциальногоСтрахования.ДополнительныеВыходныеДниПоУходуЗаДетьмиИнвалидами Тогда
			КатегорияНачисленияИлиНеоплаченногоВремени = "ОплатаДнейУходаЗаДетьмиИнвалидами";
		ИначеЕсли СтрокаТЗ.ВидПособияСоциальногоСтрахования = Перечисления.ВидыПособийСоциальногоСтрахования.ДополнительныйОтпускПослеНесчастныхСлучаев Тогда
			КатегорияНачисленияИлиНеоплаченногоВремени = "ОтпускНаСанаторноКурортноеЛечение";	
		КонецЕсли;
		
		ОписаниеВР = ОписаниеСвойствНачислений[КатегорияНачисленияИлиНеоплаченногоВремени];
		Если ОписаниеВР = Неопределено Тогда
			ОписаниеВР = ОписаниеСвойствНачислений["ОплатаБольничногоЛиста"];	
		КонецЕсли;
		ОписаниеВР.Вставить("КатегорияНачисленияИлиНеоплаченногоВремени", КатегорияНачисленияИлиНеоплаченногоВремени);
		СтрокаТЗ.ОписаниеВР = ОписаниеВР;
		
	КонецЕсли;
	
КонецЦикла;

Запрос.УстановитьПараметр("МесяцНачалаПереноса", МесяцНачалаПереноса);
Запрос.УстановитьПараметр("МесяцОкончанияПереносаКонецМесяца", КонецМесяца(МесяцОкончанияПереноса));
Запрос.Текст = 
"ВЫБРАТЬ
|	РегламентированныйПроизводственныйКалендарь.ДатаКалендаря КАК ДатаКалендаря,
|	НАЧАЛОПЕРИОДА(РегламентированныйПроизводственныйКалендарь.ДатаКалендаря, МЕСЯЦ) КАК ПериодОтбор
|ИЗ
|	РегистрСведений.РегламентированныйПроизводственныйКалендарь КАК РегламентированныйПроизводственныйКалендарь
|ГДЕ
|	РегламентированныйПроизводственныйКалендарь.ДатаКалендаря МЕЖДУ &МесяцНачалаПереноса И &МесяцОкончанияПереносаКонецМесяца
|
|УПОРЯДОЧИТЬ ПО
|	ПериодОтбор,
|	ДатаКалендаря";
ДанныеКалендаря = Запрос.Выполнить().Выгрузить();

ТекстЗапроса = 
"ВЫБРАТЬ
|	ОсновныеНачисленияРаботниковОрганизаций.ПериодРегистрации КАК ПериодРегистрации,
|	ОсновныеНачисленияРаботниковОрганизаций.Организация,
|	ОсновныеНачисленияРаботниковОрганизаций.ОбособленноеПодразделение КАК ОбособленноеПодразделение,
|	ОсновныеНачисленияРаботниковОрганизаций.Сотрудник КАК Сотрудник,
//Нети (Батрасова А. 01.07.2018, ошибка при переносе рабочих ОПТ
//|	ОсновныеНачисленияРаботниковОрганизаций.ФизЛицо,
|	ВЫБОР
|		КОГДА ОсновныеНачисленияРаботниковОрганизаций.ФизЛицо.нвЭталон <> ЗНАЧЕНИЕ(Справочник.ФизическиеЛица.ПустаяСсылка)
|			ТОГДА ОсновныеНачисленияРаботниковОрганизаций.ФизЛицо.нвЭталон
|		ИНАЧЕ ОсновныеНачисленияРаботниковОрганизаций.ФизЛицо
|	КОНЕЦ КАК ФизЛицо,
//Нети )Батрасова А. 01.07.2018
|	ОсновныеНачисленияРаботниковОрганизаций.ВидРасчета КАК ВидРасчета,
|	ОсновныеНачисленияРаботниковОрганизаций.ПериодДействияНачало,
|	ОсновныеНачисленияРаботниковОрганизаций.ПериодДействияКонец,
|	ВЫБОР
|		КОГДА ОсновныеНачисленияРаботниковОрганизаций.ОбособленноеПодразделение = ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка)
|			ТОГДА ОсновныеНачисленияРаботниковОрганизаций.Организация
|		ИНАЧЕ ОсновныеНачисленияРаботниковОрганизаций.ОбособленноеПодразделение
|	КОНЕЦ КАК ОрганизацияОтбор,
|	ОсновныеНачисленияРаботниковОрганизаций.Сторно
|ИЗ
|	РегистрРасчета.ОсновныеНачисленияРаботниковОрганизаций КАК ОсновныеНачисленияРаботниковОрганизаций
|ГДЕ
|	ОсновныеНачисленияРаботниковОрганизаций.Сотрудник В
|			(ВЫБРАТЬ РАЗЛИЧНЫЕ
|				Сотрудники.Сотрудник
|			ИЗ
|				ВТСотрудникиДляФСС КАК Сотрудники)
|	И ОсновныеНачисленияРаботниковОрганизаций.ПериодДействия = &ТекущийМесяц
|	И ОсновныеНачисленияРаботниковОрганизаций.ПериодРегистрации < &ДатаНачалаРаботыСПрограммой
|	И ОсновныеНачисленияРаботниковОрганизаций.ВидРасчета В
|			(ВЫБРАТЬ РАЗЛИЧНЫЕ
|				ОтборПоВР.ВидРасчета
|			ИЗ
|				ВТВидыРасчетаОтбор КАК ОтборПоВР)
|
|УПОРЯДОЧИТЬ ПО
|	ОрганизацияОтбор,
|	Сотрудник,
|	ВидРасчета,
|	ПериодРегистрации";

Запрос.УстановитьПараметр("ДатаНачалаРаботыСПрограммой", ДатаНачалаРаботыСПрограммой);
ТекущийМесяц = МесяцНачалаПереноса;

ДанныеОВремени = Новый ТаблицаЗначений;
ДанныеОВремени.Колонки.Добавить("ФизическоеЛицо");
ДанныеОВремени.Колонки.Добавить("ГоловнаяОрганизация");
ДанныеОВремени.Колонки.Добавить("Месяц");
ДанныеОВремени.Колонки.Добавить("ВидБолезниУходаЗаДетьми");
ДанныеОВремени.Колонки.Добавить("КатегорияНачисленияИлиНеоплаченногоВремени");
ДанныеОВремени.Колонки.Добавить("ДокументОснование");
Для НомерДняМесяца = 1 По 31 Цикл
	ДанныеОВремени.Колонки.Добавить("ОтработанДень" + НомерДняМесяца);
	ДанныеОВремени.Колонки.Добавить("НеотработанДень" + НомерДняМесяца);
	ДанныеОВремени.Колонки.Добавить("БолезньУходЗаДетьмиДень" + НомерДняМесяца);
КонецЦикла;

ОтборСвойстваНачислений = Новый Структура("ВидРасчета");
ОтборСтрок = Новый Структура("ФизическоеЛицо,КатегорияНачисленияИлиНеоплаченногоВремени");
ОтборКалендаря = Новый Структура("ПериодОтбор");

Пока ТекущийМесяц <= МесяцОкончанияПереноса Цикл
	
	ОтборКалендаря.ПериодОтбор = ТекущийМесяц;
	КалендарныеДни = ДанныеКалендаря.НайтиСтроки(ОтборКалендаря);
	Если КалендарныеДни.Количество() = 0 Тогда
		ТекущийМесяц = ДобавитьМесяц(ТекущийМесяц, 1);
		Продолжить;
	КонецЕсли;
	
	Запрос.Текст = ТекстЗапроса;
	Запрос.УстановитьПараметр("ТекущийМесяц", ТекущийМесяц);
	Выборка = Запрос.Выполнить().Выбрать();
	
	Пока Выборка.СледующийПоЗначениюПоля("ОрганизацияОтбор") Цикл
		
		ВыборкаПоДокументу = Новый Структура;
		ВыборкаПоДокументу.Вставить("Номер",				"ЗП_СЗФСС");
		ВыборкаПоДокументу.Вставить("Организация",			Выборка.ОрганизацияОтбор);
		ВыборкаПоДокументу.Вставить("ПериодРегистрации",	ТекущийМесяц);
		
		СреднийФССДанныеОВремени = ДанныеОВремени.СкопироватьКолонки();
		
		Пока Выборка.Следующий() Цикл
			
			Если Не ЗначениеЗаполнено(Выборка.ФизЛицо) Тогда
				Продолжить;
			КонецЕсли;
			
			ОтборСвойстваНачислений.ВидРасчета = Выборка.ВидРасчета;
			СведенияОНачислении = СвойстваНачислений.НайтиСтроки(ОтборСвойстваНачислений);
			Если СведенияОНачислении.Количество() = 0 Тогда
				Продолжить;
			КонецЕсли;
			
			ОтработанДень				= СведенияОНачислении[0].ОтработанныеДни;
			БолезньУходЗаДетьмиДень		= СведенияОНачислении[0].ДниБолезни;
			ВидБолезниУходаЗаДетьми     = СведенияОНачислении[0].ОписаниеВР;
			Если ВидБолезниУходаЗаДетьми = Неопределено Тогда
				КатегорияНачисленияИлиНеоплаченногоВремени = "";
			Иначе
				КатегорияНачисленияИлиНеоплаченногоВремени = ВидБолезниУходаЗаДетьми.КатегорияНачисленияИлиНеоплаченногоВремени;
			КонецЕсли;
			
			ОтборСтрок.ФизическоеЛицо = Выборка.ФизЛицо;
			ОтборСтрок.КатегорияНачисленияИлиНеоплаченногоВремени = КатегорияНачисленияИлиНеоплаченногоВремени;
			СуществующаяСтрока = СреднийФССДанныеОВремени.НайтиСтроки(ОтборСтрок);
			Если СуществующаяСтрока.Количество() = 0 Тогда
				
				НоваяСтрока = СреднийФССДанныеОВремени.Добавить();
				НоваяСтрока.ФизическоеЛицо						= Выборка.ФизЛицо;
				НоваяСтрока.ГоловнаяОрганизация					= Выборка.Организация;
				НоваяСтрока.Месяц								= ТекущийМесяц;
				НоваяСтрока.ВидБолезниУходаЗаДетьми				= ВидБолезниУходаЗаДетьми;
				НоваяСтрока.ДокументОснование					= ВыборкаПоДокументу;
				
				НоваяСтрока.КатегорияНачисленияИлиНеоплаченногоВремени = КатегорияНачисленияИлиНеоплаченногоВремени;
				
				Для НомерДняМесяца = 1 По 31 Цикл
					НоваяСтрока["ОтработанДень" + НомерДняМесяца] = Ложь;
					НоваяСтрока["НеотработанДень" + НомерДняМесяца] = Ложь;
					НоваяСтрока["БолезньУходЗаДетьмиДень" + НомерДняМесяца] = Ложь;
				КонецЦикла;
				
			Иначе
				
				НоваяСтрока = СуществующаяСтрока[0];
				
			КонецЕсли;
			
			Для каждого СтрокаКалендарныйДень Из КалендарныеДни Цикл
				
				ДатаКалендаря = СтрокаКалендарныйДень.ДатаКалендаря;
				Если (ДатаКалендаря < Выборка.ПериодДействияНачало) Или (ДатаКалендаря > НачалоДня(Выборка.ПериодДействияКонец)) Тогда
					Продолжить;
				КонецЕсли;
				НомерДняМесяца = День(ДатаКалендаря);
				
				Если БолезньУходЗаДетьмиДень Тогда
					
					НоваяСтрока["БолезньУходЗаДетьмиДень" + НомерДняМесяца] = Не Выборка.Сторно;
					НоваяСтрока["ОтработанДень" + НомерДняМесяца]   = Ложь;
					НоваяСтрока["НеотработанДень" + НомерДняМесяца] = Ложь;
					
				Иначе
					
					НоваяСтрока["БолезньУходЗаДетьмиДень" + НомерДняМесяца] = Ложь;
					НоваяСтрока["ОтработанДень" + НомерДняМесяца]   = ОтработанДень И Не Выборка.Сторно;
					НоваяСтрока["НеотработанДень" + НомерДняМесяца] = Не ОтработанДень И Не Выборка.Сторно;
					
				КонецЕсли;
				
			КонецЦикла;
			
		КонецЦикла;
		
		Если СреднийФССДанныеОВремени.Количество() > 0 Тогда
			
			ВыборкаПоДокументу = Новый Структура;
			ВыборкаПоДокументу.Вставить("Номер",				"ЗП_СЗФСС");
			ВыборкаПоДокументу.Вставить("Организация",			Выборка.ОрганизацияОтбор);
			ВыборкаПоДокументу.Вставить("ПериодРегистрации",	ТекущийМесяц);
			ВыборкаПоДокументу.Вставить("ДанныеОВремениДляРасчетаСреднегоФСС",	СреднийФССДанныеОВремени);
			
			ВыгрузитьПоПравилу(ВыборкаПоДокументу, , , , "ДанныеДляРасчетаСреднегоФСС_ПД");
			
		КонецЕсли;
		
	КонецЦикла;
	
	ТекущийМесяц = ДобавитьМесяц(ТекущийМесяц, 1);
	
КонецЦикла;
