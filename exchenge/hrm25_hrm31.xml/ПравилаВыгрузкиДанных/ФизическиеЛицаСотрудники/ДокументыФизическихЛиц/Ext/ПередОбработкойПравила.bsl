﻿Запрос = Новый Запрос;
Запрос.МенеджерВременныхТаблиц = ОбъектыПереносаДанных.ВыполнитьАлгоритм("ФизическиеЛица_ВременнаяТаблица", Параметры.Алгоритмы, Параметры, Запросы);
Запрос.Текст = 
"ВЫБРАТЬ
|	ПаспортныеДанныеФизЛиц.Период,
|	ПаспортныеДанныеФизЛиц.ФизЛицо,
|	ПаспортныеДанныеФизЛиц.ДокументВид КАК ВидДокумента,
|	ПаспортныеДанныеФизЛиц.ДокументСерия КАК Серия,
|	ПаспортныеДанныеФизЛиц.ДокументНомер КАК Номер,
|	ПаспортныеДанныеФизЛиц.ДокументДатаВыдачи КАК ДатаВыдачи,
|	ПаспортныеДанныеФизЛиц.ДокументКемВыдан КАК КемВыдан,
|	ПаспортныеДанныеФизЛиц.ДокументКодПодразделения КАК КодПодразделения,
|	ИСТИНА КАК ЯвляетсяДокументомУдостоверяющимЛичность
|ИЗ
|	РегистрСведений.ПаспортныеДанныеФизЛиц КАК ПаспортныеДанныеФизЛиц
|ГДЕ
|	ПаспортныеДанныеФизЛиц.ФизЛицо В
|			(ВЫБРАТЬ РАЗЛИЧНЫЕ
|				ФизическиеЛица.Физлицо
|			ИЗ
|				ВТФизическиеЛица КАК ФизическиеЛица)
|
|УПОРЯДОЧИТЬ ПО
|	ПаспортныеДанныеФизЛиц.ФизЛицо";
ВыборкаДанных = Запрос.Выполнить();
