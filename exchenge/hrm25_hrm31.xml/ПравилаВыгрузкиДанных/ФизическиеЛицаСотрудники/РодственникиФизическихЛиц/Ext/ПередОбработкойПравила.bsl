﻿Запрос = Новый Запрос;
Запрос.МенеджерВременныхТаблиц = ОбъектыПереносаДанных.ВыполнитьАлгоритм("ФизическиеЛица_ВременнаяТаблица", Параметры.Алгоритмы, Параметры, Запросы);

Запрос.Текст = 
"ВЫБРАТЬ
|	ФизическиеЛицаСоставСемьи.Ссылка КАК Владелец,
|	ФизическиеЛицаСоставСемьи.СтепеньРодства,
|	ФизическиеЛицаСоставСемьи.Имя КАК Наименование,
|	ФизическиеЛицаСоставСемьи.ДатаРождения
|ИЗ
|	Справочник.ФизическиеЛица.СоставСемьи КАК ФизическиеЛицаСоставСемьи
|ГДЕ
|	ФизическиеЛицаСоставСемьи.Ссылка В
|			(ВЫБРАТЬ РАЗЛИЧНЫЕ
|				ФизическиеЛица.Физлицо
|			ИЗ
|				ВТФизическиеЛица КАК ФизическиеЛица)
|
|УПОРЯДОЧИТЬ ПО
|	Владелец";

Выборка = Запрос.Выполнить().Выбрать();

РодственникиФизическихЛиц = Новый Структура("Владелец,СтепеньРодства,Наименование,ДатаРождения");

Пока Выборка.Следующий() Цикл
	
	ЗаполнитьЗначенияСвойств(РодственникиФизическихЛиц, Выборка);
	ВыгрузитьПоПравилу(РодственникиФизическихЛиц, , , , "РодственникиФизическихЛиц");
	
КонецЦикла;
