﻿Если НаборЗаписей.Количество() > 0 Тогда
	
	Если НаборЗаписей[0].ИспользоватьНачисленияПоДоговорам Тогда
		
		Настройка = РегистрыСведений.НастройкиРасчетаЗарплатыРасширенный.СоздатьНаборЗаписей();  
		Настройка.Прочитать();
		Если Настройка.Количество() > 0 Тогда
			Настройка[0].ИспользоватьНачисленияПоДоговорам = Истина;
		Иначе
			НоваяЗапись = Настройка.Добавить();
			НоваяЗапись.ИспользоватьНачисленияПоДоговорам = Истина;
		КонецЕсли;
		
		Настройка.ОбменДанными.Загрузка = Истина;
		Настройка.Записать();
		
	КонецЕсли;	
	
КонецЕсли;

Отказ = Истина;
