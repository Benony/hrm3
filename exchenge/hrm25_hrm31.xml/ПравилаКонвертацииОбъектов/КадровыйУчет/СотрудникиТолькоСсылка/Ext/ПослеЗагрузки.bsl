﻿ЗаписьЖурналаРегистрации("Обмен данными.2.5 -> 3.1. Сотрудники ТС запись", УровеньЖурналаРегистрации.Примечание, , , "Запись " + Объект.Наименование + " новый " + Объект.ЭтоНовый() +" "+ ПараметрыОбъекта["пТекGUID"] + " " + Объект.Ссылка.УникальныйИдентификатор());
//Нети (Батрасова А. 24.09.2018, наряд 0000026873
Если Объект.ЭтоНовый() Тогда
	Если ЗначениеЗаполнено(ПараметрыОбъекта["GUIDУпр"]) Тогда  
		ТекущийИД = Новый УникальныйИдентификатор(ПараметрыОбъекта["пТекGUID"]);
		НовыйИД = Новый УникальныйИдентификатор(ПараметрыОбъекта["GUIDУпр"]); 
		Если ТекущийИД <> НовыйИД Тогда
			НовыйСотр = Справочники.Сотрудники.СоздатьЭлемент();
			НовыйСотрСсылка = Справочники.Сотрудники.ПолучитьСсылку(НовыйИД);
			Объект.УстановитьСсылкуНового(НовыйСотрСсылка);
			ЗаписьЖурналаРегистрации("Обмен данными.2.5 -> 3.1. Сотрудники ТС запись", УровеньЖурналаРегистрации.Примечание, , , "Записан " + Объект.Наименование+" " + Строка(НовыйИД));
		КонецЕсли;
	ИначеЕсли ЗначениеЗаполнено(ПараметрыОбъекта["пТекGUID"]) Тогда
		ТекущийИД = Новый УникальныйИдентификатор(ПараметрыОбъекта["пТекGUID"]); 
		НовыйСотр = Справочники.Сотрудники.СоздатьЭлемент();
		НовыйСотрСсылка = Справочники.Сотрудники.ПолучитьСсылку(ТекущийИД);
		Объект.УстановитьСсылкуНового(НовыйСотрСсылка);
		ЗаписьЖурналаРегистрации("Обмен данными.2.5 -> 3.1. Сотрудники ТС запись", УровеньЖурналаРегистрации.Примечание, , , "Записан " + Объект.Наименование+" " + Строка(ТекущийИД));
	КонецЕсли;
КонецЕсли;
//Нети )Батрасова А. 24.09.2018
