﻿//Нети (Батрасова А. 24.09.2018, наряд 0000026873
НайденПоУпр = Ложь;
Если ЗначениеЗаполнено(ПараметрыОбъекта["GUIDУпр"]) Тогда  
	ТекущийИД = Новый УникальныйИдентификатор(ПараметрыОбъекта["пТекGUID"]);
	НовыйИД = Новый УникальныйИдентификатор(ПараметрыОбъекта["GUIDУпр"]); 
	Если ТекущийИД <> НовыйИД Тогда
		СсылкаНового = Справочники.Сотрудники.ПолучитьСсылку(НовыйИД); 
		НовыйОбъект = СсылкаНового.ПолучитьОбъект();
		Если НовыйОбъект <> Неопределено Тогда
			НайденПоУпр = Истина;
			СсылкаНаОбъект = НовыйОбъект.Ссылка;
			ЗаписьЖурналаРегистрации("Обмен данными.2.5 -> 3.1. СотрудниуТолькоСсылка", УровеньЖурналаРегистрации.Примечание, , , "Найден GUIDУпр" + НовыйИД);
		КонецЕсли;
	КонецЕсли;
КонецЕсли;
Если НЕ НайденПоУпр И ЗначениеЗаполнено(ПараметрыОбъекта["пТекGUID"]) Тогда
	ТекущийИД = Новый УникальныйИдентификатор(ПараметрыОбъекта["пТекGUID"]);
	СсылкаНового = Справочники.Сотрудники.ПолучитьСсылку(ТекущийИД); 
	НовыйОбъект = СсылкаНового.ПолучитьОбъект();
	Если НовыйОбъект <> Неопределено Тогда
		СсылкаНаОбъект = НовыйОбъект.Ссылка;
		ЗаписьЖурналаРегистрации("Обмен данными.2.5 -> 3.1. СотрудниуТолькоСсылка", УровеньЖурналаРегистрации.Примечание, , , "Найден пТекGUID" + ТекущийИД);
	КонецЕсли;
КонецЕсли;
ПрекратитьПоиск = Истина;
//Нети )Батрасова А. 24.09.2018
