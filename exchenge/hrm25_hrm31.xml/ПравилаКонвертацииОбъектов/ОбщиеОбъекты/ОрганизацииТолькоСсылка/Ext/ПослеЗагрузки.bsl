﻿// Нети (05.03.2019 Ващенко Е.В. наряд 0000029688
Если Объект.ЭтоНовый() Тогда
	Если ЗначениеЗаполнено(ПараметрыОбъекта["нвGUIDСУТ"]) И ПараметрыОбъекта["нвGUIDСУТ"] <> ПараметрыОбъекта["ГУИД"] Тогда
		ЗаписьЖурналаРегистрации("Обмен данными.2.5 -> 3.1. Организация", УровеньЖурналаРегистрации.Примечание, , , "Для организации ["+СокрЛП(Объект.Наименование)+"] "+ "значение в реквизите нвGUIDСУТ ["+ПараметрыОбъекта["нвGUIDСУТ"]+"] не является ГУИДом!");
	КонецЕсли;
	ГУИДОбъекта = Новый УникальныйИдентификатор(ПараметрыОбъекта["ГУИД"]); 
	СсылкаНаЭлемент = Справочники.Организации.ПолучитьСсылку(ГУИДОбъекта); 
	Объект.УстановитьСсылкуНового(СсылкаНаЭлемент);
КонецЕсли;
// Нети )05.03.2019 Ващенко Е.В.

Если НЕ нвНастройкиПовтИсп.ЭтоБазаУпр() Тогда Объект.ДополнительныеСвойства.Удалить("ПроверятьБизнесЛогикуПриЗаписи"); КонецЕсли; //Нети (Батрасова А. 02.08.2018, перенос в рабочие базы РН
