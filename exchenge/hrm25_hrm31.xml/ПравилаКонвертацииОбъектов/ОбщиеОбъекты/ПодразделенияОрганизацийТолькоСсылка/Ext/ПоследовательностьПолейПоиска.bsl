﻿// Нети (05.03.2019 Ващенко Е.В. наряд 0000029688
ГУИДОбъекта = Новый УникальныйИдентификатор(ПараметрыОбъекта["ГУИД"]); 
СсылкаНаЭлемент = Справочники.ПодразделенияОрганизаций.ПолучитьСсылку(ГУИДОбъекта); 
НовыйОбъект = СсылкаНаЭлемент.ПолучитьОбъект();
Если НовыйОбъект <> Неопределено Тогда
	СсылкаНаОбъект = СсылкаНаЭлемент;
КонецЕсли;

ПрекратитьПоиск = Истина;
// Нети )05.03.2019 Ващенко Е.В.
