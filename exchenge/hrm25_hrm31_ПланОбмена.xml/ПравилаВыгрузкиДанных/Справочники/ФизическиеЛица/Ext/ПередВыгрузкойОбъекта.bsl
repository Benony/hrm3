﻿Запрос = Новый Запрос;
//Запрос.УстановитьПараметр("МассивФизлиц", Параметры.МассивФизлиц);
Запрос.УстановитьПараметр("МассивФизлиц", Объект.Ссылка);
Запрос.Текст =
"ВЫБРАТЬ
|	ВложенныйЗапрос.ФизЛицо,
|	ПаспортныеДанныеФизЛиц.ДатаРегистрацииПоМестуЖительства
|ИЗ
|	(ВЫБРАТЬ
|		МАКСИМУМ(ПаспортныеДанныеФизЛиц.Период) КАК Период,
|		ПаспортныеДанныеФизЛиц.ФизЛицо КАК ФизЛицо
|	ИЗ
|		РегистрСведений.ПаспортныеДанныеФизЛиц КАК ПаспортныеДанныеФизЛиц
|	ГДЕ
|		ПаспортныеДанныеФизЛиц.ФизЛицо В (&МассивФизлиц)
|	
|	СГРУППИРОВАТЬ ПО
|		ПаспортныеДанныеФизЛиц.ФизЛицо) КАК ВложенныйЗапрос
|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.ПаспортныеДанныеФизЛиц КАК ПаспортныеДанныеФизЛиц
|		ПО ВложенныйЗапрос.Период = ПаспортныеДанныеФизЛиц.Период
|			И ВложенныйЗапрос.ФизЛицо = ПаспортныеДанныеФизЛиц.ФизЛицо";
Выборка = Запрос.Выполнить().Выбрать();

ДатыРегистрации = Новый Соответствие;
Пока Выборка.Следующий() Цикл
	ДатыРегистрации.Вставить(Выборка.Физлицо, Выборка.ДатаРегистрацииПоМестуЖительства);
КонецЦикла;

Запрос.Текст = Запросы.ФизическиеЛица_Выборка.Текст;
ТаблицаФизическиеЛица = Запрос.Выполнить().Выгрузить();
ТаблицаФизическиеЛица.Колонки.Добавить("ГруппаДоступа");
ТаблицаФизическиеЛица.Колонки.Добавить("КонтактнаяИнформация");
ТаблицаФизическиеЛица.Колонки.Добавить("ДатаРегистрации");

Запрос.Текст = СтрЗаменить(Запросы.КонтактнаяИнформацияОбъекта_Выборка.Текст, "#ИмяОбъекта", "Справочник.ФизическиеЛица");
ТаблицаКонтактнаяИнформация = Запрос.Выполнить().Выгрузить();
ТаблицаКонтактнаяИнформация.Индексы.Добавить("Объект");

ВыборкаФЛ = Новый Массив;
Отбор = Новый Структура("Объект");

Для каждого ФизическоеЛицо Из ТаблицаФизическиеЛица Цикл
	
	ФизическоеЛицо.ГруппаДоступа = ФизическоеЛицо.ГруппаДоступаФизическогоЛица;
	
	Отбор.Объект = ФизическоеЛицо.Ссылка;
	СтрокиКИ = ТаблицаКонтактнаяИнформация.НайтиСтроки(Отбор);
	
	КонтактнаяИнформация = ОбъектыПереносаДанных.ВыполнитьАлгоритм("КонтактнаяИнформация_Преобразовать", Алгоритмы, СтрокиКИ);
	ФизическоеЛицо.КонтактнаяИнформация = КонтактнаяИнформация;
	
	ДатаРегистрации = ДатыРегистрации[ФизическоеЛицо.Ссылка];
	Если ЗначениеЗаполнено(ДатаРегистрации) Тогда
		ФизическоеЛицо.ДатаРегистрации = ДатаРегистрации;
	КонецЕсли;
	
	ВыборкаФЛ.Добавить(ОбъектыПереносаДанных.СтрокаТаблицыЗначенийВСтруктуру(ФизическоеЛицо));
	
КонецЦикла;

Для Каждого ФЛ Из ВыборкаФЛ Цикл
	ВыгрузитьПоПравилу(ФЛ,,,,"ФизическиеЛица");
КонецЦикла; 

Отказ = Истина;
