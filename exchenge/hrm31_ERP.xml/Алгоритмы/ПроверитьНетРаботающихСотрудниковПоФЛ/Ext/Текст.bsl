﻿Запрос = Новый Запрос;
Запрос.Текст = "ВЫБРАТЬ
|	СУММА(ВЫБОР
|			КОГДА КадроваяИсторияСотрудниковСрезПоследних.ВидСобытия <> ЗНАЧЕНИЕ(Перечисление.ВидыКадровыхСобытий.Увольнение)
|				ТОГДА 1
|			ИНАЧЕ 0
|		КОНЕЦ) КАК МестРаботы,
|	КадроваяИсторияСотрудниковСрезПоследних.ФизическоеЛицо КАК ФизическоеЛицо
|ИЗ
|	РегистрСведений.КадроваяИсторияСотрудников.СрезПоследних(&ТекДата, ФизическоеЛицо = &Физлицо) КАК КадроваяИсторияСотрудниковСрезПоследних
|
|СГРУППИРОВАТЬ ПО
|	КадроваяИсторияСотрудниковСрезПоследних.ФизическоеЛицо";
Запрос.УстановитьПараметр("Физлицо", Физлицо);
Запрос.УстановитьПараметр("ТекДата", ТекущаяДата());

Выборка = Запрос.Выполнить().Выбрать();
Если Выборка.Следующий() Тогда
	ВсеСотрФЛУволены = ?(Выборка.МестРаботы = 0, Истина, Ложь);		
КонецЕсли;
