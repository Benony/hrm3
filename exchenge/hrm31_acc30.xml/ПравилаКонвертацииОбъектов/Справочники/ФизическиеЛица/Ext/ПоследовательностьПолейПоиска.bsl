﻿ПоляПоискаМассив = Новый Массив;
Если ЗначениеЗаполнено(СвойстваПоиска["СтраховойНомерПФР"]) Тогда
	ПоляПоискаМассив.Добавить("СтраховойНомерПФР");
ИначеЕсли ЗначениеЗаполнено(СвойстваПоиска["ИНН"]) Тогда
	ПоляПоискаМассив.Добавить("ИНН");
ИначеЕсли ЗначениеЗаполнено(СвойстваПоиска["ДатаРождения"]) Тогда
	ПоляПоискаМассив.Добавить("Наименование,ДатаРождения");
КонецЕсли;
ПоляПоискаМассив.Добавить("Наименование");

Если НомерВариантаПоиска <= ПоляПоискаМассив.Количество() Тогда
	СтрокаИменСвойствПоиска = ПоляПоискаМассив[НомерВариантаПоиска-1];
Иначе	
	ПрекратитьПоиск = Истина;
КонецЕсли;
