﻿Если Параметры.НеВыгружатьПерсональныеДанныеФизическихЛиц Тогда
	Отказ = Истина;
Иначе
	Если Объект.ФормаРасчетов = Перечисления.ФормыОплаты.Безналичная Тогда
		ИмяПКО = "ДоговорЗаймаСотрудникуБанк";
	Иначе
		ИмяПКО = "ДоговорЗаймаСотрудникуКасса";
	КонецЕсли;
КонецЕсли;
