﻿// Выполним процедуры для нового сотрудника, без них некорректно отображаются данные под пользователями с разграничением прав
Если НЕ ОбъектНайден 
   И ПараметрыОбъекта <> Неопределено 
   И ПараметрыОбъекта.Получить("ГуидСотрудника") <> Неопределено Тогда
	СсылкаСотрудника = Справочники.Сотрудники.ПолучитьСсылку(Новый УникальныйИдентификатор(ПараметрыОбъекта.Получить("ГуидСотрудника")));
	УстановитьПривилегированныйРежим(Истина);
	
	НаборЗаписей = РегистрыСведений.ДанныеДляПодбораСотрудников.СоздатьНаборЗаписей();
	НаборЗаписей.Отбор.Сотрудник.Установить(СсылкаСотрудника);
	НаборЗаписей.Отбор.ПоДоговоруГПХ.Установить(Ложь);
	
	Запись = НаборЗаписей.Добавить();
	Запись.Наименование = Объект.Наименование;
	Запись.Сотрудник = СсылкаСотрудника;
	Запись.Организация = Объект.ГоловнаяОрганизация;
	Запись.ФизическоеЛицо = Объект.ФизическоеЛицо;
	Запись.ИдентификаторЗаписи = Новый УникальныйИдентификатор;
	Запись.ПоДоговоруГПХ = Ложь; 
	Запись.ЭтоГоловнойСотрудник = Истина;
	
	НаборЗаписей.Записать();
	
	УстановитьПривилегированныйРежим(Ложь);
	
	// 2.
	ТаблицаАнализаИзменений = КадровыйУчет.ТаблицаАнализаИзменений();
	
	СтрокаТаблицы = ТаблицаАнализаИзменений.Добавить();
	СтрокаТаблицы.Сотрудник = СсылкаСотрудника;
	СтрокаТаблицы.Организация = Объект.ГоловнаяОрганизация;
	СтрокаТаблицы.ФлагИзменений = 1;
	
	КадровыйУчет.ОбработатьИзменениеОрганизацийВНабореПоТаблицеИзменений(ТаблицаАнализаИзменений);
	
	//3.
	Если ПолучитьФункциональнуюОпцию("ИспользоватьНачислениеЗарплаты") Тогда
			
		УстановитьПривилегированныйРежим(Истина);
		
		НаборЗаписей = РегистрыСведений.ОсновныеСотрудникиФизическихЛиц.СоздатьНаборЗаписей();
		НаборЗаписей.Отбор.ФизическоеЛицо.Установить(Объект.ФизическоеЛицо);
		НаборЗаписей.Отбор.ГоловнаяОрганизация.Установить(Объект.ГоловнаяОрганизация);
		
		НоваяЗаписьРС = НаборЗаписей.Добавить();
		НоваяЗаписьРС.ГоловнаяОрганизация = Объект.ГоловнаяОрганизация;
		НоваяЗаписьРС.ФизическоеЛицо = Объект.ФизическоеЛицо;
		НоваяЗаписьРС.ДатаНачала = '00010101';
		НоваяЗаписьРС.ДатаОкончания = ЗарплатаКадрыПериодическиеРегистры.МаксимальнаяДата();
		НоваяЗаписьРС.Сотрудник = СсылкаСотрудника;
		
		НаборЗаписей.Записать();
		
		УстановитьПривилегированныйРежим(Ложь);
	КонецЕсли;
	
	Объект.ДополнительныеСвойства.Вставить("ПроверятьБизнесЛогикуПриЗаписи", Истина); 
	Объект.ДополнительныеСвойства.Вставить("СсылкаНового", СсылкаСотрудника);
КонецЕсли;

//принудительное обновление данных для подбора сотрудников, т.к. если в УПР было изменено ФИО, и сотрудник тоже зарегистрирован к обмену, сотрудник загружается
//с обновленным наименованием, поэтому в РС Данные для подбора сотрудников наименование не обновляется
Если ОбъектНайден и НЕ Отказ Тогда
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
		|	ФИОФизическихЛицСрезПоследних.ФизическоеЛицо КАК ФизическоеЛицо,
		|	ВЫРАЗИТЬ(ФИОФизическихЛицСрезПоследних.Фамилия + ВЫБОР
		|			КОГДА ФИОФизическихЛицСрезПоследних.Имя = """"
		|				ТОГДА """"
		|			ИНАЧЕ "" "" + ФИОФизическихЛицСрезПоследних.Имя
		|		КОНЕЦ + ВЫБОР
		|			КОГДА ФИОФизическихЛицСрезПоследних.Отчество = """"
		|				ТОГДА """"
		|			ИНАЧЕ "" "" + ФИОФизическихЛицСрезПоследних.Отчество
		|		КОНЕЦ КАК СТРОКА(50)) КАК Наименование
		|ИЗ
		|	РегистрСведений.ФИОФизическихЛиц.СрезПоследних(&ТекДата, ФизическоеЛицо = &ФизическоеЛицо) КАК ФИОФизическихЛицСрезПоследних";
	Запрос.УстановитьПараметр("ТекДата", ТекущаяДата());
	Запрос.УстановитьПараметр("ФизическоеЛицо", Объект.ФизическоеЛицо);
	
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() Тогда
		Если Выборка.Наименование <> Объект.Ссылка.Наименование Тогда
			УстановитьПривилегированныйРежим(Истина);
	
			НаборЗаписей = РегистрыСведений.ДанныеДляПодбораСотрудников.СоздатьНаборЗаписей();
			НаборЗаписей.Отбор.Сотрудник.Установить(Объект.Ссылка);
			
			НаборЗаписей.Прочитать();
			
			Для каждого Запись Из НаборЗаписей Цикл
				Запись.Наименование = Выборка.Наименование;
			КонецЦикла;
			
			НаборЗаписей.Записать();
			
			УстановитьПривилегированныйРежим(Ложь);
		КонецЕсли;
	КонецЕсли;
КонецЕсли;
