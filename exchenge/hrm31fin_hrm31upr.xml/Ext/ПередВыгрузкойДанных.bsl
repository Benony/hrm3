﻿//Нети (Батрасова А. 18.12.2018, наряд 0000028198
Параметры.ТолькоКомпенсацияОтпускФинУпр = нвНастройкиПовтИсп.ЗначениеНастройкиПрограммы("ТолькоКомпенсацияОтпускФинУпр", Ложь);
	//вынести в параметры
Отбор = Новый Структура;
Отбор.Вставить("ВАрхиве", Ложь);
Отбор.Вставить("ПометкаУдаления", Ложь);
Отбор.Вставить("ВидОтпуска", ОбщегоНазначенияКлиентСервер.ПредопределенныйЭлемент("Справочник.ВидыОтпусков.Северный"));  
//Нети (Батрасова А. 21.05.2019, наряд 0000031054
//НачисленияКомп = РасчетЗарплаты.НачисленияПоКатегории(Перечисления.КатегорииНачисленийИНеоплаченногоВремени.КомпенсацияОтпуска, Отбор);  
НачисленияКомп = ПланыВидовРасчета.Начисления.НачисленияПоКатегории(Перечисления.КатегорииНачисленийИНеоплаченногоВремени.КомпенсацияОтпуска, Отбор);  
//Нети )Батрасова А. 21.05.2019

Если НачисленияКомп.Количество() > 0 Тогда
	ВРКомпенсацияОтпускаСеверный = НачисленияКомп[0];
Иначе
	ВРКомпенсацияОтпускаСеверный = ПланыВидовРасчета.Начисления.ПустаяСсылка();
КонецЕсли;

Параметры.ВРКомпенсацияОтпускаСеверный = ВРКомпенсацияОтпускаСеверный;
//Нети )Батрасова А. 18.12.2018

// Нети (15.02.2019 Ващенко Е.В. наряд 0000029024
Параметры.ИспользуетсяОтборПоОрганизациям = (ЗначениеЗаполнено(УзелДляОбмена) И УзелДляОбмена.СписокОрганизацийДляВыгрузкиНачислений.Количество()>0);
Если Параметры.ИспользуетсяОтборПоОрганизациям Тогда 
	Параметры.СписокОрганизацийДляВыгрузкиНачислений = УзелДляОбмена.СписокОрганизацийДляВыгрузкиНачислений;	
КонецЕсли;
// Нети )15.02.2019 Ващенко Е.В.

//Нети (Батрасова А. 13.11.2019, наряд 0000034071
Запрос = Новый Запрос;
Запрос.Текст = "ВЫБРАТЬ
	|	ДополнительныеРеквизитыИСведения.Ссылка КАК Ссылка
	|ИЗ
	|	ПланВидовХарактеристик.ДополнительныеРеквизитыИСведения КАК ДополнительныеРеквизитыИСведения
	|ГДЕ
	|   ДополнительныеРеквизитыИСведения.Имя = &Имя";
	//|	И ДополнительныеРеквизитыИСведения.ВладелецДополнительныхЗначений = &ВладелецДополнительныхЗначений";
Запрос.УстановитьПараметр("Имя", "нвАУП");
Запрос.УстановитьПараметр("ВладелецДополнительныхЗначений", УправлениеСвойствами.НаборСвойствПоИмени("Справочник_Сотрудники"));

Выборка = Запрос.Выполнить().Выбрать();
Если Выборка.Следующий() Тогда
	Параметры.РеквизитНеАУП = Выборка.Ссылка;	
Иначе
	Параметры.РеквизитНеАУП = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.ПустаяСсылка();	
КонецЕсли;
//Нети )Батрасова А. 13.11.2019
//Нети (Батрасова А. 18.11.2019, наряд 0000034071
Параметры.ВыгружатьСтраховыеВзносыВПерерасчетВУПР = нвНастройкиПовтИсп.ЗначениеНастройкиПрограммы("ВыгружатьСтраховыеВзносыВПерерасчетВУПР", Ложь); 
Параметры.ВыгружатьНДФЛВРазрезеЦФО = нвНастройкиПовтИсп.ЗначениеНастройкиПрограммы("ВыгружатьНДФЛВРазрезеЦФО", Ложь); 
//Нети )Батрасова А. 18.11.2019

//НВД 2020.01.31 РешетнякГВ, 0000035238 (
СтруктураПроверкиНаличияРеквизита = Новый Структура("ВремяНачалаОбмена");
ЗаполнитьЗначенияСвойств(СтруктураПроверкиНаличияРеквизита, УзелДляОбмена);

Если СтруктураПроверкиНаличияРеквизита["ВремяНачалаОбмена"] <> Неопределено Тогда

	Если УзелДляОбмена.ВремяНачалаОбмена <> "" Тогда 
		ЧасНачалаОбмена = СтроковыеФункцииКлиентСервер.СтрокаВЧисло(Лев(УзелДляОбмена.ВремяНачалаОбмена, 2));
		МинутаНачалаОбмена = СтроковыеФункцииКлиентСервер.СтрокаВЧисло(Прав(УзелДляОбмена.ВремяНачалаОбмена, 2)); 
		ТекущийЧас = Час(ТекущаяДата());
		ТекущаяМинута = Минута(ТекущаяДата());
		Если ТекущийЧас >= ЧасНачалаОбмена И ТекущийЧас < 24
			И ТекущаяМинута >= МинутаНачалаОбмена И ТекущаяМинута < 60 Тогда 
				Параметры.ВремяНачалаОбменаНаступило = Истина;
		Иначе
			Параметры.ВремяНачалаОбменаНаступило = Ложь;
		КонецЕсли;
	Иначе
		Параметры.ВремяНачалаОбменаНаступило = Истина;
	КонецЕсли;

Иначе
	
	Параметры.ВремяНачалаОбменаНаступило = Истина;
	
КонецЕсли;
//)НВД 2020.01.31 РешетнякГВ
