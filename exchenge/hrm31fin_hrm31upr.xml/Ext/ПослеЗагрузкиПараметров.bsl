﻿// Нети (23.05.2019 Ващенко Е.В. наряд 0000031090
Параметры.Вставить("УзелИБИсточника",ПланыОбмена.нвОбменЗарплата3Зарплата3.ПустаяСсылка());

Если ЗначениеЗаполнено(Параметры.ПрефиксУзлаОбменаИсточника) Тогда
	УзелИБИсточника = ПланыОбмена.нвОбменЗарплата3Зарплата3.НайтиПоКоду(Параметры.ПрефиксУзлаОбменаИсточника);
	Если ЗначениеЗаполнено(УзелИБИсточника) Тогда
		Параметры.Вставить("УзелИБИсточника",УзелИБИсточника);
	КонецЕсли; 
КонецЕсли;
// Нети )23.05.2019 Ващенко Е.В.
