﻿МетаданныеДокумента = Объект.Метаданные();

ИмяПериодРегистрации = "ПериодРегистрации"; // если будет другое имя реквизита, то можно добавить проверкой через метаданные
// Нети (09.04.2019 Ващенко Е.В. наряд 0000030330
Если МетаданныеДокумента.Реквизиты.Найти(ИмяПериодРегистрации) = Неопределено Тогда
	ИмяПериодРегистрации = "МесяцНачисления";
КонецЕсли;
// Нети )09.04.2019 Ващенко Е.В.

КешСотрудников = Новый ТаблицаЗначений;
КешСотрудников.Колонки.Добавить("ПодразделенияФин",Новый ОписаниеТипов("СправочникСсылка.ПодразделенияОрганизаций"));
КешСотрудников.Колонки.Добавить("Сотрудник",Новый ОписаниеТипов("СправочникСсылка.Сотрудники"));
КешСотрудников.Колонки.Добавить("Физлицо",Новый ОписаниеТипов("СправочникСсылка.ФизическиеЛица"));
КешСотрудников.Колонки.Добавить("ПодразделенияУпр",Новый ОписаниеТипов("СправочникСсылка.ПодразделенияОрганизаций"));

СтруктураПоискаВКешПоСотруднику = Новый Структура("ПодразделенияФин,Сотрудник");
СтруктураПоискаВКешПоФизлицу = Новый Структура("ПодразделенияФин,Физлицо");

ЗапросПодразделения = Новый Запрос;
ЗапросПодразделения.Текст = 
"ВЫБРАТЬ РАЗРЕШЕННЫЕ ПЕРВЫЕ 1
|	СоответствиеПодразделений.ПодразделениеУпр КАК ПодразделениеУпр
|ПОМЕСТИТЬ втПодразделениеУпр
|ИЗ
|	РегистрСведений.нвуСоответствиеПодразделений КАК СоответствиеПодразделений
|ГДЕ
|	СоответствиеПодразделений.ПодразделениеФин = &ПодразделениеФин
|;
|
|////////////////////////////////////////////////////////////////////////////////
|ВЫБРАТЬ РАЗРЕШЕННЫЕ
|	КадроваяИстория.Организация КАК Организация,
|	КадроваяИстория.Подразделение КАК Подразделение
|ИЗ
|	РегистрСведений.КадроваяИсторияСотрудников.СрезПоследних(
|			&ДатаСреза,
|			ВЫБОР КОГДА &Сотрудник = НЕОПРЕДЕЛЕНО
|					ТОГДА ИСТИНА
|				  ИНАЧЕ Сотрудник = &Сотрудник
|			КОНЕЦ
|			И ВЫБОР КОГДА &Физлицо = НЕОПРЕДЕЛЕНО
|					ТОГДА ИСТИНА
|				  ИНАЧЕ ФизическоеЛицо = &Физлицо
|			КОНЕЦ) КАК КадроваяИстория
|ГДЕ
|	КадроваяИстория.Подразделение В ИЕРАРХИИ
|			(ВЫБРАТЬ
|				втПодразделениеУпр.ПодразделениеУпр
|			ИЗ
|				втПодразделениеУпр)";


Для каждого МетаТЧДокумента Из МетаданныеДокумента.ТабличныеЧасти Цикл

	РеквизитыТЧДокумента = МетаТЧДокумента.Реквизиты;
	ИмяТЧДокумента = МетаТЧДокумента.Имя;
	
	ЕстьКолонкаПодразделение = ?(РеквизитыТЧДокумента.Найти("Подразделение")=Неопределено, Ложь, Истина);
	ЕстьКолонкаСотрудник = ?(РеквизитыТЧДокумента.Найти("Сотрудник")=Неопределено, Ложь, Истина);
	ЕстьКолонкаФизлицо = ?(РеквизитыТЧДокумента.Найти("ФизическоеЛицо")=Неопределено, Ложь, Истина);
	
	Если НЕ ЕстьКолонкаПодразделение Тогда
		Продолжить;
	КонецЕсли; 	
	
	Для каждого СтрокаТЧ Из Объект[ИмяТЧДокумента] Цикл
		
		Если ЕстьКолонкаПодразделение И ЗначениеЗаполнено(СтрокаТЧ.Подразделение)
		   И ТипЗнч(СтрокаТЧ.Подразделение) = Тип("СправочникСсылка.ПодразделенияОрганизаций") Тогда
		    // все хорошо
		Иначе
			Продолжить;				
		КонецЕсли; 
		
		Если (ЕстьКолонкаСотрудник И ЗначениеЗаполнено(СтрокаТЧ.Сотрудник)) 
		 ИЛИ (ЕстьКолонкаФизлицо И ЗначениеЗаполнено(СтрокаТЧ.ФизическоеЛицо)) Тогда
			// все хорошо
	 	Иначе
			Продолжить;
		КонецЕсли; 
		
		// сначала проверим в Кеш
		ПодразделениеУпрТЧ = Неопределено;
		ЕстьЗначениеВКеш = Ложь;
		Если ЕстьКолонкаСотрудник И ЗначениеЗаполнено(СтрокаТЧ.Сотрудник) Тогда
			СтруктураПоискаВКешПоСотруднику.Вставить("ПодразделенияФин",СтрокаТЧ.Подразделение);
			СтруктураПоискаВКешПоСотруднику.Вставить("Сотрудник",СтрокаТЧ.Сотрудник);
			
			НайденныеСтрокиКеша = КешСотрудников.НайтиСтроки(СтруктураПоискаВКешПоСотруднику);
			
			Если НайденныеСтрокиКеша.Количество() > 0 Тогда
				ПодразделениеУпрТЧ = НайденныеСтрокиКеша[0].ПодразделенияУпр;
				ЕстьЗначениеВКеш = Истина;
			КонецЕсли; 
		ИначеЕсли ЕстьКолонкаФизлицо И ЗначениеЗаполнено(СтрокаТЧ.ФизическоеЛицо) Тогда 	
			СтруктураПоискаВКешПоФизлицу.Вставить("ПодразделенияФин",СтрокаТЧ.Подразделение);
			СтруктураПоискаВКешПоФизлицу.Вставить("Физлицо",СтрокаТЧ.ФизическоеЛицо);
			
			НайденныеСтрокиКеша = КешСотрудников.НайтиСтроки(СтруктураПоискаВКешПоФизлицу);
			
			Если НайденныеСтрокиКеша.Количество() > 0 Тогда
				ПодразделениеУпрТЧ = НайденныеСтрокиКеша[0].ПодразделенияУпр;
				ЕстьЗначениеВКеш = Истина;
			КонецЕсли;
		КонецЕсли;
		
		Если ЕстьЗначениеВКеш Тогда
			СтрокаТЧ.Подразделение = ПодразделениеУпрТЧ;
			Продолжить;
		КонецЕсли; 
		
		// в Кеш не нашлось, теперь можно через запрос			
		ЗапросПодразделения.УстановитьПараметр("ДатаСреза",Объект[ИмяПериодРегистрации]);
		ЗапросПодразделения.УстановитьПараметр("ПодразделениеФин",СтрокаТЧ.Подразделение);
		ЗапросПодразделения.УстановитьПараметр("Сотрудник",?(ЕстьКолонкаСотрудник И ЗначениеЗаполнено(СтрокаТЧ.Сотрудник),СтрокаТЧ.Сотрудник,Неопределено));
		ЗапросПодразделения.УстановитьПараметр("Физлицо",?(ЕстьКолонкаФизлицо И ЗначениеЗаполнено(СтрокаТЧ.ФизическоеЛицо),СтрокаТЧ.ФизическоеЛицо,Неопределено));
		Выборка = ЗапросПодразделения.Выполнить().Выбрать();
		
		Если Выборка.Следующий() Тогда
			ПодразделениеУпрТЧ = Выборка.Подразделение;
		//Нети (Батрасова А. 05.02.2019, наряд 0000028585
		//по сотрудникам, принятым в течении месяца регистрации, данных на начало месяца нет, поэтому подразделение мигрировало без замены на подразделение УПР
		Иначе
			ЗапросПодразделения.УстановитьПараметр("ДатаСреза", КонецМесяца(Объект[ИмяПериодРегистрации]));
			Выборка = ЗапросПодразделения.Выполнить().Выбрать(); 
		    Если Выборка.Следующий() Тогда
				ПодразделениеУпрТЧ = Выборка.Подразделение;  
			КонецЕсли;	
		//Нети )Батрасова А. 05.02.2019
		КонецЕсли;
	
		СтрокаТЧ.Подразделение = ПодразделениеУпрТЧ;
		
		Если ЕстьКолонкаСотрудник И ЗначениеЗаполнено(СтрокаТЧ.Сотрудник) Тогда
			НоваяСтрокаКеш = КешСотрудников.Добавить();
			НоваяСтрокаКеш.ПодразделенияФин = СтрокаТЧ.Подразделение;
			НоваяСтрокаКеш.Сотрудник = СтрокаТЧ.Сотрудник;
			НоваяСтрокаКеш.Физлицо = ?(ЕстьКолонкаФизлицо И ЗначениеЗаполнено(СтрокаТЧ.ФизическоеЛицо),СтрокаТЧ.ФизическоеЛицо,СтрокаТЧ.Сотрудник.ФизическоеЛицо);
			НоваяСтрокаКеш.ПодразделенияУпр = ПодразделениеУпрТЧ;
		КонецЕсли; 
		
	КонецЦикла; 
	
КонецЦикла;
