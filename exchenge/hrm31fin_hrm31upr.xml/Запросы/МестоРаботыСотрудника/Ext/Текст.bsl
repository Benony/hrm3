﻿ВЫБРАТЬ РАЗРЕШЕННЫЕ
	КадроваяИстория.Организация КАК Организация,
	КадроваяИстория.Подразделение КАК Подразделение
ИЗ
	РегистрСведений.КадроваяИсторияСотрудников.СрезПоследних(
			&ДатаСреза,
			ВЫБОР
					КОГДА &Сотрудник = НЕОПРЕДЕЛЕНО
						ТОГДА ИСТИНА
					ИНАЧЕ Сотрудник = &Сотрудник
				КОНЕЦ
				И ФизическоеЛицо = &Физлицо) КАК КадроваяИстория
