﻿//Нети (Батрасова А. 05.11.2019, наряд 0000034071
Отказ = НЕ Объект.ДокументРассчитан;

Если ВходящиеДанные = Неопределено Тогда
	ВходящиеДанные = Новый Структура();
КонецЕсли;

Если НЕ Отказ Тогда 
	
	ВходящиеДанные.Вставить("НастройкаРозницы", Ложь);
	
	Если нвНастройкиПовтИсп.ЗначениеНастройкиПрограммы("ТолькоКомпенсацияОтпускФинУпр", Ложь) Тогда

	ИначеЕсли Параметры.ИспользуетсяОтборПоОрганизациям  
			И Параметры.СписокОрганизацийДляВыгрузкиНачислений.Найти(Объект.Организация, "Организация") <> Неопределено Тогда
			
		// установим параметр которым будет определять выгружать или нет кадровые данные в документах
		ВходящиеДанные.Вставить("НастройкаРозницы", Истина);
	Иначе
		Отказ = Истина;
	КонецЕсли;
	
КонецЕсли;
//Нети )Батрасова А. 05.11.2019
