﻿// Нети (12.02.2019 Ващенко Е.В. наряд 0000029024
Отказ = НЕ Объект.ПерерасчетВыполнен;

Если ВходящиеДанные = Неопределено Тогда
	ВходящиеДанные = Новый Структура();
КонецЕсли;

Если НЕ Отказ Тогда 
	
	ВходящиеДанные.Вставить("НастройкаРозницы", Ложь);
	
	Если Параметры.ИспользуетсяОтборПоОрганизациям  
	   И Параметры.СписокОрганизацийДляВыгрузкиНачислений.Найти(Объект.Организация, "Организация") <> Неопределено Тогда
		// установим параметр которым будет определять выгружать или нет кадровые данные в документах
		ВходящиеДанные.Вставить("НастройкаРозницы", Истина);
	КонецЕсли;
	
КонецЕсли;
// Нети )12.02.2019 Ващенко Е.В.
