﻿//НВД 2020.08.11 РешетнякГВ наряд 0000037628(
Отказ = НЕ Объект.НачисленияУтверждены;

Если НЕ Отказ Тогда 
	
	Если нвНастройкиПовтИсп.ЗначениеНастройкиПрограммы("ТолькоКомпенсацияОтпускФинУпр", Ложь) Тогда
		
	ИначеЕсли Параметры.ИспользуетсяОтборПоОрганизациям  
			И Параметры.СписокОрганизацийДляВыгрузкиНачислений.Найти(Объект.Организация, "Организация") <> Неопределено Тогда
		
	Иначе
		Отказ = Истина;
	КонецЕсли;
	
КонецЕсли;
//)НВД 2020.08.11 РешетнякГВ
