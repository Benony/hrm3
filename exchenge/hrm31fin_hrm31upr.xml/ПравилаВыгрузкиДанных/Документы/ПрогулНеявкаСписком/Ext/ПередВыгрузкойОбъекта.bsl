﻿Отказ = НЕ Объект.ПерерасчетВыполнен; //НВД 2020.02.05 РешетнякГВ, 0000033905 ( задача доработки 000034310

//Нети (Батрасова А. 08.06.2020, наряд 0000037391
Если ВходящиеДанные = Неопределено Тогда
	ВходящиеДанные = Новый Структура();
КонецЕсли;

Если НЕ Отказ Тогда 
	
	ВходящиеДанные.Вставить("НастройкаРозницы", Ложь);
	
	Если Параметры.ИспользуетсяОтборПоОрганизациям  
			И Параметры.СписокОрганизацийДляВыгрузкиНачислений.Найти(Объект.Организация, "Организация") <> Неопределено Тогда
			
		// установим параметр которым будет определять выгружать или нет кадровые данные в документах
		ВходящиеДанные.Вставить("НастройкаРозницы", Истина);
	Иначе
		Отказ = Истина;
	КонецЕсли;
	
КонецЕсли;
//Нети )Батрасова А. 08.06.2020
