﻿//Нети (Батрасова А. 26.04.2019, задача внедрения 000029295
//ДнейКомпенсацииСев = 0;
//Если Параметры.ТолькоКомпенсацияОтпускФинУпр Тогда	
//	СтрокиПоСев = Источник.ДополнительныеОтпуска.НайтиСтроки(Новый Структура("ВидОтпуска", ОбщегоНазначенияКлиентСервер.ПредопределенныйЭлемент("Справочник.ВидыОтпусков.Северный")));
//	Для Каждого СтрСев Из СтрокиПоСев Цикл
//		ДнейКомпенсацииСев = ДнейКомпенсацииСев + СтрСев.КоличествоДнейКомпенсации;	
//	КонецЦикла;
//КонецЕсли;
//Значение = ДнейКомпенсацииСев;

ТЗ = Источник.ДополнительныеОтпуска.Выгрузить().СкопироватьКолонки();
Если Параметры.ТолькоКомпенсацияОтпускФинУпр Тогда	
	ТЗ = Источник.ДополнительныеОтпуска.Выгрузить(Новый Структура("ВидОтпуска", ОбщегоНазначенияКлиентСервер.ПредопределенныйЭлемент("Справочник.ВидыОтпусков.Северный")));
КонецЕсли;

Значение = ЗначениеВСтрокуВнутр(ТЗ);
//Нети )Батрасова А. 26.04.2019
