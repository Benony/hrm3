﻿// Нети (07.02.2019 Ващенко Е.В. наряд 0000028515 

// пересчитаем реквизит Удержано
Выполнить(Алгоритмы.ПересчитатьРеквизитУдержано);// Нети (19.02.2019 Ващенко Е.В. наряд 0000029024 пересчитаем Удержано

Объект.ДополнительныеСвойства.Вставить("ВидОперацииРасчетУдержаний",Истина); // Нети (22.05.2019 Ващенко Е.В. наряд 0000031090

//1. преобразуем подразделение и организацию фин в упр
Выполнить(Алгоритмы.ПреобразоватьОрганизациюИзФинВУпр); //Невада (15.02.2019 Решетняк Г.В. наряд 0000029207

Выполнить(Алгоритмы.ПреобразоватьПодразделениеИзФинВУпр);

//2. теперь из начисление зарплаты делаем расчет удержания
ВидОперации = нвНастройкиПовтИсп.ЗначениеОпределяемыеЗначенияСправочников("ВидОперацииРасчетУдержаний");

// Нети (09.04.2019 Ващенко Е.В. наряд 0000029911
Если НЕ ЗначениеЗаполнено(ВидОперации) Тогда
	Отказ = Истина;

	ТекстСообщения = "В ""Определяемые значения справочников (Невада)"" не установлено значение для ""Вид операции ""Расчет удержаний"""": Отказ = %1
	|Документ %2 не будет загружен!";
	ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения, Отказ, Объект.Ссылка);
	ЗаписьЖурналаРегистрации("Обмен данными.нвОбменЗарплата3Зарплата3.Получение данных", УровеньЖурналаРегистрации.Предупреждение, , Объект.Ссылка, ТекстСообщения);
КонецЕсли;
// Нети )09.04.2019 Ващенко Е.В.

Если НЕ ОбъектНайден Тогда
	СсылкаНаДокумент = Объект.ПолучитьСсылкуНового();
Иначе
	СсылкаНаДокумент = Объект.Ссылка;
КонецЕсли;

Если ОбщегоНазначения.ПодсистемаСуществует("ЗарплатаКадрыПриложения.ОперацииРасчетаЗарплаты")
   И ЗначениеЗаполнено(ВидОперации) Тогда
   
	Модуль = ОбщегоНазначения.ОбщийМодуль("ОперацииРасчетаЗарплаты");
	Модуль.ЗаписатьВидОперацииДокумента(СсылкаНаДокумент, ВидОперации);
КонецЕсли;

//3. заполним состав документа - служебные процедуры
// Невада (22.10.2019 Фомичева И.Н. наряд 0000033881 в новом релизе изменено имя функции
//ОписаниеДокумента = ЗарплатаКадрыСоставДокументов.ОписаниеДокументаПоИсточнику(Объект);
ОписаниеДокумента = ЗарплатаКадрыСоставДокументов.ОписаниеОбъектаПоИсточнику(Объект);
// Невада )22.10.2019 Фомичева И.Н.

Если ОписаниеДокумента <> Неопределено Тогда
	
	СотрудникиФизическиеЛица = ЗарплатаКадрыСоставДокументов.СотрудникиФизическиеЛица(Объект, ОписаниеДокумента);
	ФизическиеЛица = ОбщегоНазначения.ВыгрузитьКолонку(СотрудникиФизическиеЛица, "ФизическоеЛицо", Истина);

	Если ОписаниеДокумента.ЗаполнятьФизическиеЛицаПоСотрудникам Тогда
		ЗарплатаКадрыСоставДокументов.ЗаполнитьФизическиеЛицаПоСотрудникамВДокументе(Объект, ОписаниеДокумента);
	КонецЕсли;

	Если ОписаниеДокумента.ЗаполнятьТабличнуюЧастьФизическиеЛицаДокумента Тогда
		ЗарплатаКадрыСоставДокументов.ЗаполнитьТабличнуюЧастьФизическиеЛицаДокумента(Объект, ОписаниеДокумента, ФизическиеЛица);
	КонецЕсли;

	Если ОписаниеДокумента.ИспользоватьКраткийСостав Тогда
		ЗарплатаКадрыСоставДокументов.ЗаполнитьКраткийСоставДокумента(Объект, ОписаниеДокумента, СотрудникиФизическиеЛица);
	КонецЕсли;

	Если ОписаниеДокумента.ИспользоватьОграничениеДоступа Тогда
		ЗарплатаКадрыСоставДокументов.ЗаполнитьРегистрФизическиеЛицаДокументов(СсылкаНаДокумент, ФизическиеЛица);
	КонецЕсли;
	
	Если ОписаниеДокумента.ЗаполнятьСоставДокументов Тогда
		ЗарплатаКадрыСоставДокументов.ЗаполнитьРегистрСоставДокументовЗарплатаКадры(СсылкаНаДокумент, СотрудникиФизическиеЛица, Объект.Дата);
	КонецЕсли;
КонецЕсли;
// Нети )07.02.2019 Ващенко Е.В.
