﻿// Нети (06.02.2019 Ващенко Е.В. наряд 0000028515
ГУИДДокумента = Новый УникальныйИдентификатор(ПараметрыОбъекта["GUID"]);
СсылкаНаДокумент = Документы.НачислениеЗарплаты.ПолучитьСсылку(ГУИДДокумента);

Если СсылкаНаДокумент.ПолучитьОбъект() <> Неопределено Тогда
	СсылкаНаОбъект = СсылкаНаДокумент;
КонецЕсли;

ПрекратитьПоиск = Истина;
// Нети )06.02.2019 Ващенко Е.В.
