﻿Если Параметры.НеВыгружатьПерсональныеДанныеФизическихЛиц Тогда
	Отказ = Истина;
ИначеЕсли ЗарплатаКадрыПриложенияВызовСервера.ЕстьПодтверждениеВыплатыДоходовПоВедомости(Объект.Ссылка) Тогда
	Отказ = Истина;
Иначе
	Зарплата = Объект.Зарплата.Выгрузить().СкопироватьКолонки();
	Запросы.ЗапросДепоненты.УстановитьПараметр("Ведомость", Объект.Ссылка);   	
	ТЗДепоненты = Запросы.ЗапросДепоненты.Выполнить().Выгрузить();
	
	Для Каждого Стр Из Объект.Зарплата Цикл
		СтрокаПоСотр = ТЗДепоненты.Найти(Стр.ФизическоеЛицо);  
		Если СтрокаПоСотр = Неопределено Тогда
			НоваяСтр = Зарплата.Добавить();	
			ЗаполнитьЗначенияСвойств(НоваяСтр, Стр);
		КонецЕсли; 
	КонецЦикла;

	Зарплата.Свернуть("ФизическоеЛицо","КВыплате");
	Зарплата.Колонки.КВыплате.Имя = "Сумма";
	ИсходящиеДанные = Новый Структура("Зарплата");
	ИсходящиеДанные.Зарплата = Зарплата;
КонецЕсли;
