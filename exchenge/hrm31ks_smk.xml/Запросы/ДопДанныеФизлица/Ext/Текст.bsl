﻿ВЫБРАТЬ РАЗРЕШЕННЫЕ
	ВЫБОР
		КОГДА КадроваяИсторияСотрудниковСрезПоследних.ВидСобытия = ЗНАЧЕНИЕ(Перечисление.ВидыКадровыхСобытий.Увольнение)
			ТОГДА 2
		ИНАЧЕ 1
	КОНЕЦ КАК ПриоритетУволен,
	ВЫБОР
		КОГДА ВидыЗанятостиСотрудниковСрезПоследних.ВидЗанятости = ЗНАЧЕНИЕ(Перечисление.ВидыЗанятости.ОсновноеМестоРаботы)
			ТОГДА 1
		КОГДА ВидыЗанятостиСотрудниковСрезПоследних.ВидЗанятости = ЗНАЧЕНИЕ(Перечисление.ВидыЗанятости.ВнутреннееСовместительство)
			ТОГДА 2
		ИНАЧЕ 3
	КОНЕЦ КАК ПриоритетВидЗанятости,
	КадроваяИсторияСотрудниковСрезПоследних.Сотрудник КАК Сотрудник,
	КадроваяИсторияСотрудниковСрезПоследних.ФизическоеЛицо КАК ФизическоеЛицо,
	КадроваяИсторияСотрудниковСрезПоследних.Организация КАК Организация,
	ПРЕДСТАВЛЕНИЕ(КадроваяИсторияСотрудниковСрезПоследних.Подразделение) КАК Подразделение,
	КадроваяИсторияСотрудниковСрезПоследних.Должность КАК Должность,
	ВЫБОР
		КОГДА КадроваяИсторияСотрудниковСрезПоследних.ВидСобытия = ЗНАЧЕНИЕ(Перечисление.ВидыКадровыхСобытий.Увольнение)
			ТОГДА ИСТИНА
		ИНАЧЕ ЛОЖЬ
	КОНЕЦ КАК Уволен
ИЗ
	РегистрСведений.КадроваяИсторияСотрудников.СрезПоследних(, ФизическоеЛицо = &Физлицо) КАК КадроваяИсторияСотрудниковСрезПоследних
		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ВидыЗанятостиСотрудников.СрезПоследних(, ФизическоеЛицо = &Физлицо) КАК ВидыЗанятостиСотрудниковСрезПоследних
		ПО КадроваяИсторияСотрудниковСрезПоследних.Сотрудник = ВидыЗанятостиСотрудниковСрезПоследних.Сотрудник

УПОРЯДОЧИТЬ ПО
	ПриоритетУволен,
	ПриоритетВидЗанятости
