﻿//Нэти (Козлов Е.А. 18.02.2019 Наряд№ 0000029374    
ВЫБРАТЬ
	ФИОФизЛицСрезПоследних.Период КАК Период,
	ФИОФизЛицСрезПоследних.Фамилия КАК Фамилия,
	ФИОФизЛицСрезПоследних.Имя КАК Имя,
	ФИОФизЛицСрезПоследних.Отчество КАК Отчество,
	//НВД 2020.05.07 РешетнякГВ, наряд 0000036868(
	//ФИОФизЛицСрезПоследних.ИнициалыИмени КАК ИнициалыИмени, 
	ФИОФизЛицСрезПоследних.Инициалы КАК Инициалы,
	//)НВД 2020.05.07 РешетнякГВ
	ФИОФизЛицСрезПоследних.ФизическоеЛицо КАК ФизическоеЛицо
ИЗ
	РегистрСведений.ФИОФизическихЛиц.СрезПоследних(&ДатаАктуальности, ФизическоеЛицо = &ФизическоеЛицо) КАК ФИОФизЛицСрезПоследних
//Нэти )Козлов Е.А. 18.02.2019 Наряд№ 0000029374
