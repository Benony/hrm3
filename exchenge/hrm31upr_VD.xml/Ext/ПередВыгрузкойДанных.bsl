﻿ТаблицаСвойств = Новый ТаблицаЗначений;
ТаблицаСвойств.Колонки.Добавить("Физлицо");
ТаблицаСвойств.Колонки.Добавить("ИмяСвойства");
ТаблицаСвойств.Колонки.Добавить("Значение");

Выполнить(Алгоритмы.Конв_ПередВыгрузкойДанных_ДопСвойства);

Параметры.ТаблицаСвойств = ТаблицаСвойств;

//Нети (Батрасова А. 25.08.2017, наряд 0000021346
ТаблицаДанныхСотрудниковКУдалению = Новый ТаблицаЗначений;
ТаблицаДанныхСотрудниковКУдалению.Колонки.Добавить("НомерПриказа");
ТаблицаДанныхСотрудниковКУдалению.Колонки.Добавить("ДатаПриказа");
ТаблицаДанныхСотрудниковКУдалению.Колонки.Добавить("ВидПриказаСтрока");

Параметры.ТаблицаДанныхСотрудниковКУдалению = ТаблицаДанныхСотрудниковКУдалению;
//Нети )Батрасова А. 25.08.2017
