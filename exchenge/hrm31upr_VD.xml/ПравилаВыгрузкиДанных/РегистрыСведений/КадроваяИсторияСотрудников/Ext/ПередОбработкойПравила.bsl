﻿//ЭтотУзел = ПланыОбмена.нвОбменВещевоеДовольствие.ЭтотУзел();
//ВыборкаУзлы = ПланыОбмена.нвОбменВещевоеДовольствие.Выбрать();
//Пока ВыборкаУзлы.Следующий() Цикл	
//	ТекущийУзел = ВыборкаУзлы.Ссылка;
//	Если ТекущийУзел <> ЭтотУзел Тогда 
//		Продолжить;
//	КонецЕсли;
//	
//	// обход по объектам, зарегистрированным для выгрузки
//	ВыборкаИзменения = ПланыОбмена.ВыбратьИзменения(ТекущийУзел,ТекущийУзел.НомерОтправленного+1);
//	Пока ВыборкаИзменения.Следующий() Цикл
//		ОбъектИзм = ВыборкаИзменения.Получить();
//		Если ТипЗнч(ОбъектИзм) = Тип("РегистрСведенийНаборЗаписей.РаботникиОрганизаций") Тогда	
//			//Нети (Батрасова А. 23.08.2017, наряд 0000021346
//			Регистратор = ОбъектИзм.Отбор.Регистратор.Значение; 
//			Если ОбъектИзм.Количество() = 0 И ЗначениеЗаполнено(Регистратор) Тогда	
//				ЧислитсяРаботающим = Истина;
//				Если ТипЗнч(Регистратор) =  Тип("ДокументСсылка.УвольнениеИзОрганизаций") Тогда
//					 ВидПриказа = "Увольнение";
//					 ЧислитсяРаботающим = Ложь;
//				ИначеЕсли ТипЗнч(Регистратор) = Тип("ДокументСсылка.ПриемНаРаботуВОрганизацию") Тогда
//					 ВидПриказа = "ПриемНаРаботу";
//				Иначе
//					 ВидПриказа = "Прочие";
//				КонецЕсли;
//				 
//			    НоваяСтр = Параметры.ТаблицаДанныхСотрудниковКУдалению.Добавить();
//				НоваяСтр.ДатаПриказа = НачалоДня(Регистратор.Дата);
//				НоваяСтр.НомерПриказа = Регистратор.Номер;
//				НоваяСтр.ВидПриказаСтрока = ВидПриказа;
//				НоваяСтр.ЧислитсяРаботающим = ЧислитсяРаботающим;
//			КонецЕсли;
// 			//Нети )Батрасова А. 23.08.2017
//			Для Каждого Стр Из ОбъектИзм Цикл
//				Отбор = Новый ТаблицаЗначений;
//				Отбор.Колонки.Добавить ("Имя");
//				Отбор.Колонки.Добавить ("Значение");
//		        Отбор.Колонки.Добавить ("Использование");

//		        СтрокаОтбора = Отбор.Добавить();
//		        СтрокаОтбора.Имя = "Организация";
//		        СтрокаОтбора.Значение = Стр.Организация;
//		        СтрокаОтбора.Использование = Истина;
//				  
//				СтрокаОтбора = Отбор.Добавить();
//		        СтрокаОтбора.Имя = "Сотрудник";
//		        СтрокаОтбора.Значение = Стр.Сотрудник;
//		        СтрокаОтбора.Использование = Истина;
//				
//				СтрокаОтбора = Отбор.Добавить();
//		        СтрокаОтбора.Имя = "Период";
//		        СтрокаОтбора.Значение = Стр.Период;
//		        СтрокаОтбора.Использование = Истина;
//			
//				Строки = Новый ТаблицаЗначений;
//		        Строки.Колонки.Добавить("Организация");
//				Строки.Колонки.Добавить("Сотрудник");
//				Строки.Колонки.Добавить("Должность");
//				Строки.Колонки.Добавить("Активность");
//				Строки.Колонки.Добавить("Период");
//				//Строки.Колонки.Добавить("Подразделение");   
//				Строки.Колонки.Добавить("ЧислитсяРаботающим");  
//				Строки.Колонки.Добавить("НомерПриказа");
//				Строки.Колонки.Добавить("ДатаПриказа");
//				Строки.Колонки.Добавить("ВидПриказа");
//				
//		        Строка = Строки.Добавить();
//		        ЗаполнитьЗначенияСвойств(Строка, Стр);
//				//Строка.Подразделение = Стр.ПодразделениеОрганизации;
//				Строка.ЧислитсяРаботающим = ?(Стр.ПричинаИзмененияСостояния = Перечисления.ПричиныИзмененияСостояния.Увольнение, Ложь, Истина);
//				Строка.ДатаПриказа = Стр.Регистратор.Дата;
//				Строка.НомерПриказа = Стр.Регистратор.Номер;
//				
//				ПричинаИзмененияСостояния = Стр.ПричинаИзмененияСостояния;
//				ВидПриказа = "";
//				Выполнить(Алгоритмы.ОпределитьВидПриказа);
//				Строка.ВидПриказа = ВидПриказа;

//    			Набор= Новый Структура("Отбор, Строки");
//	    		Набор.Отбор = Отбор;
//	    		Набор.Строки = Строки;
//				
//				//ВыгрузитьРегистр(Набор,,,,"ДанныеСотрудников");
//				ВыгрузитьПоПравилу(Набор, , , , "ДанныеСотрудников");
//				
//				//строки с Периодом По
//				Если ЗначениеЗаполнено(Стр.ПериодЗавершения) Тогда
//					Отбор = Новый ТаблицаЗначений;
//					Отбор.Колонки.Добавить ("Имя");
//					Отбор.Колонки.Добавить ("Значение");
//					Отбор.Колонки.Добавить ("Использование");

//					СтрокаОтбора = Отбор.Добавить();
//					СтрокаОтбора.Имя = "Организация";
//					СтрокаОтбора.Значение = Стр.Организация;
//					СтрокаОтбора.Использование = Истина;
//					  
//					СтрокаОтбора = Отбор.Добавить();
//					СтрокаОтбора.Имя = "Сотрудник";
//					СтрокаОтбора.Значение = Стр.Сотрудник;
//					СтрокаОтбора.Использование = Истина;
//					
//					СтрокаОтбора = Отбор.Добавить();
//					СтрокаОтбора.Имя = "Период";
//					СтрокаОтбора.Значение = Стр.ПериодЗавершения;
//					СтрокаОтбора.Использование = Истина;

//				
//					Строки = Новый ТаблицаЗначений;
//					Строки.Колонки.Добавить("Организация");
//					Строки.Колонки.Добавить("Сотрудник");
//					Строки.Колонки.Добавить("Должность");
//					Строки.Колонки.Добавить("Активность");
//					Строки.Колонки.Добавить("Период");
//					//Строки.Колонки.Добавить("Подразделение");   
//					Строки.Колонки.Добавить("ЧислитсяРаботающим");  
//					Строки.Колонки.Добавить("НомерПриказа");
//					Строки.Колонки.Добавить("ДатаПриказа");
//					Строки.Колонки.Добавить("ВидПриказа");
//					
//					Строка = Строки.Добавить();
//					ЗаполнитьЗначенияСвойств(Строка, Стр);
//					Строка.Период = Стр.ПериодЗавершения;
//					//Строка.Подразделение = Стр.ПодразделениеОрганизацииЗавершения;
//					Строка.Должность = Стр.ДолжностьЗавершения;
//					Строка.ЧислитсяРаботающим = ?(Стр.ПричинаИзмененияСостоянияЗавершения = Перечисления.ПричиныИзмененияСостояния.Увольнение, Ложь, Истина);
//					Строка.ДатаПриказа = Стр.Регистратор.Дата;
//					Строка.НомерПриказа = Стр.Регистратор.Номер;
//					
//					ПричинаИзмененияСостояния = Стр.ПричинаИзмененияСостоянияЗавершения;
//					ВидПриказа = "";
//					Выполнить(Алгоритмы.ОпределитьВидПриказа);
//					Строка.ВидПриказа = ВидПриказа;

//					Набор= Новый Структура("Отбор, Строки");
//					Набор.Отбор = Отбор;
//					Набор.Строки = Строки;
//					
//					//ВыгрузитьРегистр(Набор,,,,"ДанныеСотрудников");
//					ВыгрузитьПоПравилу(Набор, , , , "ДанныеСотрудников");
//					
//				КонецЕсли;
//			КонецЦикла;
//		КонецЕсли;
//	КонецЦикла;				
//КонецЦикла;
