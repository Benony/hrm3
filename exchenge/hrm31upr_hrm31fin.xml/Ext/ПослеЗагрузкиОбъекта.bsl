﻿//Нети (Батрасова А. 26.08.2019, наряд 0000032710
Если Не Отказ Тогда
	ОбъектМетаданных = Метаданные.НайтиПоТипу(ТипЗнч(Объект));
	Если Метаданные.Документы.Содержит(ОбъектМетаданных) Тогда
		Выполнить(Алгоритмы.ЗаполнитьДатуЗапретаПослеЗагрузки);
	КонецЕсли;
КонецЕсли;
//Нети )Батрасова А. 26.08.2019
