﻿Если ЗначениеЗаполнено(ОбъектКоллекции.нвОрганизацияФИН) Тогда
	Значение = ОбъектКоллекции.нвОрганизацияФИН;
Иначе
	МассивЗначений = нвРаботаСоСправочниками.ДопСвойстваСотрудниковФизлиц(ОбъектКоллекции.Сотрудник, "ПодразделениеФин.Владелец").ВыгрузитьКолонку("ПодразделениеФинВладелец");
	Если МассивЗначений.Количество() > 0 Тогда
		Значение = МассивЗначений[0];
	КонецЕсли;
КонецЕсли;
