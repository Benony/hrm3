﻿//НВД 2020.03.02 РешетнякГВ, 0000036025(
Запросы.ПодразделениеФИН.УстановитьПараметр("ПодразделениеУПР", пПодразделениеУПР);
Выборка = Запросы.ПодразделениеФИН.Выполнить().Выбрать();
//НВД 2020.03.18 РешетнякГВ, 0000036025(
//Если Выборка.Следующий() Тогда
//	Значение = Выборка.ПодразделениеФИН
//КонецЕсли;
Если Выборка.Количество() = 1 Тогда
	Если Выборка.Следующий() Тогда
		Значение = Выборка.ПодразделениеФИН
	КонецЕсли;
ИначеЕсли Выборка.Количество() > 1 Тогда
	МассивЗначений = нвРаботаСоСправочниками.ДопСвойстваСотрудниковФизлиц(пСотрудник.Ссылка, "ПодразделениеФин.Владелец").ВыгрузитьКолонку("ПодразделениеФинВладелец");
	пОрганизацияФИН = МассивЗначений[0];
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
					|	нвуСоответствиеПодразделений.ПодразделениеФин КАК ПодразделениеФин
					|ИЗ
					|	РегистрСведений.нвуСоответствиеПодразделений КАК нвуСоответствиеПодразделений
					|ГДЕ
					|	нвуСоответствиеПодразделений.ПодразделениеУпр = &ПодразделениеУпр
					|	И нвуСоответствиеПодразделений.ОрганизацияФин = &ОрганизацияФин";
	Запрос.УстановитьПараметр("ПодразделениеУПР", пПодразделениеУПР);
	Запрос.УстановитьПараметр("ОрганизацияФИН", пОрганизацияФИН);
	Значение = Запрос.Выполнить().Выгрузить()[0].ПодразделениеФин;
КонецЕсли;
//)НВД 2020.03.18 РешетнякГВ
//)НВД 2020.03.02 РешетнякГВ
