﻿Отказ = пДокумент.нвНеВыгружатьВФИН; 

Если Отказ И нвНастройкиПовтИсп.ЗначениеНастройкиПрограммы("ОбменыРегистрироватьСобытияОбмена", Ложь) Тогда
	ЭтоСотрудник = (ТипЗнч(пДокумент) = Тип("СправочникСсылка.ФизическиеЛица") ИЛИ ТипЗнч(пДокумент) = Тип("СправочникОбъект.ФизическиеЛица"));
	ТекстСообщения = НСтр("ru = 'Для документа %1 установлен признак ""Не выгружать в ФИН"". Документ не был выгружен.'");
	ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения, пДокумент);
	
	Если ЭтоСотрудник Тогда
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "документа", "физлица");
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "Документ", "Сотрудник");
	КонецЕсли;
	
	нвУправлениеЖурналомРегистрации.ЗаписатьСобытиеВЖурналРегистрации("ПравилаОбмена", ТекстСообщения, пДокумент);
КонецЕсли;

//Нети (Батрасова А. 28.09.2018, наряд 0000027011
//проверяем заполненность ШЕ ФИН в приеме на рабочу/кадровом перемещении
Если Не Отказ и (ТипЗнч(пДокумент.Ссылка) = Тип("ДокументСсылка.ПриемНаРаботу") ИЛИ ТипЗнч(пДокумент) = Тип("ДокументСсылка.КадровыйПеревод")) Тогда 
	Если Не ЗначениеЗаполнено(пДокумент.нвДолжностьПоШтатномуРасписаниюФИН) Тогда
		Отказ = Истина;
			
		Если нвНастройкиПовтИсп.ЗначениеНастройкиПрограммы("ОбменыРегистрироватьСобытияОбмена", Ложь) Тогда
			ТекстСообщения = "В документе не заполена должность по ШР ФИН. Документ не будет выгружен.";
			нвУправлениеЖурналомРегистрации.ЗаписатьСобытиеВЖурналРегистрации("ПравилаОбмена", ТекстСообщения, пДокумент);
		КонецЕсли;
	КонецЕсли;
КонецЕсли;
//Нети )Батрасова А. 28.09.2018
