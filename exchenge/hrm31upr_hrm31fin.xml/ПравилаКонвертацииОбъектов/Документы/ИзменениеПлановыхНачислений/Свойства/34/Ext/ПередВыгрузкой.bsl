﻿СотрудникиДляПроверки = ОбъектКоллекции.Сотрудник;
СтатусыДляПроверки = нвРаботаСоСправочниками.ДопСвойстваСотрудниковФизлиц(СотрудникиДляПроверки, "Статус").ВыгрузитьКолонку("Статус");
Отказ = НЕ нвРаботаСоСправочниками.СтатусыСотрудниковПринимаютЗначения(
	СтатусыДляПроверки,
	нвНастройкиПовтИсп.СтатусыДляМиграцииВБазуФин());
	
пСотрудник = ОбъектКоллекции.Сотрудник;
пДокумент = Источник;
Выполнить(Алгоритмы.ПроверитьФИНДанныеСотрудника);
