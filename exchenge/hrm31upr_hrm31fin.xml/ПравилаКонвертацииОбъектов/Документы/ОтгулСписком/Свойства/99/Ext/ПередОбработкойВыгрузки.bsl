﻿Запрос = новый Запрос;
		Запрос.УстановитьПараметр("Ссылка",Источник.Ссылка);
		запрос.Текст =
		"ВЫБРАТЬ
		|	ОтгулСпискомРаспределениеРезультатовНачислений.Ссылка КАК Ссылка,
		|	ОтгулСпискомРаспределениеРезультатовНачислений.НомерСтроки КАК НомерСтроки,
		|	ОтгулСпискомРаспределениеРезультатовНачислений.ИдентификаторСтроки КАК ИдентификаторСтроки,
		|	ОтгулСпискомРаспределениеРезультатовНачислений.Территория КАК Территория,
		|	ОтгулСпискомРаспределениеРезультатовНачислений.СтатьяФинансирования КАК СтатьяФинансирования,
		|	ОтгулСпискомРаспределениеРезультатовНачислений.СтатьяРасходов КАК СтатьяРасходов,
		|	ОтгулСпискомРаспределениеРезультатовНачислений.СпособОтраженияЗарплатыВБухучете КАК СпособОтраженияЗарплатыВБухучете,
		|	ОтгулСпискомРаспределениеРезультатовНачислений.ОблагаетсяЕНВД КАК ОблагаетсяЕНВД,
		|	ОтгулСпискомРаспределениеРезультатовНачислений.Результат КАК Результат,
		|	ОтгулСпискомРаспределениеРезультатовНачислений.ПодразделениеУчетаЗатрат КАК ПодразделениеУчетаЗатрат,
		|	ОтгулСпискомНачисления.Сотрудник КАК Сотрудник
		|ИЗ
		|	Документ.ОтгулСписком.РаспределениеРезультатовНачислений КАК ОтгулСпискомРаспределениеРезультатовНачислений
		|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ОтгулСписком.Начисления КАК ОтгулСпискомНачисления
		|		ПО ОтгулСпискомНачисления.Ссылка = &Ссылка
		|		И ОтгулСпискомРаспределениеРезультатовНачислений.ИдентификаторСтроки = ОтгулСпискомНачисления.ИдентификаторСтрокиВидаРасчета
		|ГДЕ
		|	ОтгулСпискомРаспределениеРезультатовНачислений.Ссылка = &Ссылка";
		КоллекцияОбъектов = запрос.выполнить().Выгрузить();
