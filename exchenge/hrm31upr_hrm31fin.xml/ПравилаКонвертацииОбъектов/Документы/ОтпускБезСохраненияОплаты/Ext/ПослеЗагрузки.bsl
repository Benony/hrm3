﻿Если Не Отказ Тогда
	Если ПолучитьФункциональнуюОпцию("ИспользоватьРасчетЗарплатыРасширенная") Тогда
		Объект.Начисления.Очистить(); // Нети (13.02.2019 Ващенко Е.В. наряд 0000029024
		
		УстановитьПривилегированныйРежим(Истина);
		
		МенеджерРасчета = РасчетЗарплатыРасширенный.СоздатьМенеджерРасчета(Объект.ПериодРегистрации, Объект.Организация);
		
		МенеджерДокумента = Документы.ОтпускБезСохраненияОплаты;
		МенеджерДокумента.ЗаполнитьНастройкиМенеджераРасчета(МенеджерРасчета, Объект.Ссылка, Объект.ИсправленныйДокумент);
		
		ТаблицаНачислений = МенеджерРасчета.ТаблицаИсходныеДанныеНачисленияЗарплатыПоНачислениям();
		НоваяСтрока = ТаблицаНачислений.Добавить();
		НоваяСтрока.Сотрудник = Объект.Сотрудник;
		НоваяСтрока.Начисление = Объект.ВидРасчета;
		НоваяСтрока.ДатаНачала = Объект.ДатаНачала;
		НоваяСтрока.ДатаОкончания = Объект.ДатаОкончания;
		
		МенеджерРасчета.ЗаполнитьНачисленияСотрудникаЗаПериод(Объект.Сотрудник, ТаблицаНачислений);
		МенеджерРасчета.РассчитатьЗарплату();
		
		МенеджерДокумента.РасчетЗарплатыВДанные(Объект, МенеджерРасчета.Зарплата);	
	КонецЕсли;
КонецЕсли;
