﻿//Нети (Батрасова А. 05.08.2018, наряд 0000027393
Если НЕ нвуСтруктураКомпанииПовтИсп.ТипУчетаОрганизации(Источник.Ссылка.Владелец) = "ФИН" Тогда
	//ВыгрузитьПоПравилу(Источник.Ссылка,,,,"ШтатноеРасписаниеТолькоСсылка"); //Нети (Батрасова А. 30.08.2019 наряд 0000032845
	Отказ = Истина;	
КонецЕсли;
//Нети )Батрасова А. 05.08.2019

КлючВыгружаемыхДанных = Источник.Ссылка.УникальныйИдентификатор(); //Нети (Батрасова А. 16.11.2018, задача внедрения 000026019
//Нети (Батрасова А. 13.07.2018, наряд 0000025903
//Нети (Батрасова А. 29.10.2018, наряд 0000027393
//Если ТипЗнч(Параметры) = Тип("Структура") И Параметры.Свойство("ТЗСхлопнутыеШЕФин") = Истина Тогда
Если нвНастройкиПовтИсп.ЗначениеНастройкиПрограммы("СхлопыватьШЕФИНПриОбмене", Ложь)
	И ТипЗнч(Параметры) = Тип("Структура") И Параметры.Свойство("ТЗСхлопнутыеШЕФин") = Истина Тогда	
//Нети )Батрасова А. 29.10.2018
	ТЗСхлопнутыеШЕФин = Параметры.ТЗСхлопнутыеШЕФин;
	СтрИсточник = ТЗСхлопнутыеШЕФин.Найти(Источник.Ссылка, "Ссылка"); 
	Если СтрИсточник <> Неопределено Тогда 
		Если ЗначениеЗаполнено(СтрИсточник.Эталон) Тогда  
			//Нети (Батрасова А. 16.11.2018, задача внедрения 000026019 
			//Отказ = Истина;
			//ВыгрузитьПоПравилу(СтрИсточник.Эталон,,,,"ШтатноеРасписание");
			Источник = СтрИсточник.Эталон;
			//Нети )Батрасова А. 16.11.2018
		Иначе
			ВходящиеДанные = Новый Структура;
			ВходящиеДанные.Вставить("КоличествоСтавок", СтрИсточник.КоличествоСтавок);
		КонецЕсли;
	КонецЕсли;
	
	//КлючВыгружаемыхДанных = Источник.Ссылка.УникальныйИдентификатор(); //Нети (Батрасова А. 16.11.2018, задача внедрения 000026019  
КонецЕсли;
//Нети )Батрасова А. 13.07.2018
