﻿// Выполним процедуры для нового сотрудника, без них некорректно отображаются данные под пользователями с разграничением прав
Если НЕ ОбъектНайден 
   И ПараметрыОбъекта <> Неопределено 
   И ПараметрыОбъекта.Получить("ГуидСотрудника") <> Неопределено Тогда
	//Нети (Батрасова А. 04.03.2019, наряд 0000029675
	//ГуидСотрудника = Новый УникальныйИдентификатор(ПараметрыОбъекта["ГуидСотрудника"]);
	//СсылкаСотрудника = Справочники.Сотрудники.ПолучитьСсылку(Новый УникальныйИдентификатор(Значение));
	СсылкаСотрудника = Справочники.Сотрудники.ПолучитьСсылку(Новый УникальныйИдентификатор(ПараметрыОбъекта.Получить("ГуидСотрудника")));
	//Нети )Батрасова А. 04.03.2019
	
	// 1.
	//РегистрыСведений.ДанныеДляПодбораСотрудников.ДобавитьДанныеНовогоСотрудника(Объект);
	УстановитьПривилегированныйРежим(Истина);
	
	НаборЗаписей = РегистрыСведений.ДанныеДляПодбораСотрудников.СоздатьНаборЗаписей();
	НаборЗаписей.Отбор.Сотрудник.Установить(СсылкаСотрудника);
	НаборЗаписей.Отбор.ПоДоговоруГПХ.Установить(Ложь);
	
	Запись = НаборЗаписей.Добавить();
	Запись.Наименование = Объект.Наименование;
	Запись.Сотрудник = СсылкаСотрудника;
	Запись.Организация = Объект.ГоловнаяОрганизация;
	Запись.ФизическоеЛицо = Объект.ФизическоеЛицо;
	Запись.ИдентификаторЗаписи = Новый УникальныйИдентификатор;
	Запись.ПоДоговоруГПХ = Ложь;
	//Нети (Батрасова А. 16.05.2019, наряд 0000030890
	//Запись.ЭтоГоловнойСотрудник = (СсылкаСотрудника = Объект.ГоловнойСотрудник); 
	Запись.ЭтоГоловнойСотрудник = Истина;
	//Нети )Батрасова А. 16.05.2019
	
	НаборЗаписей.Записать();
	
	УстановитьПривилегированныйРежим(Ложь);

	
	// 2.
	ТаблицаАнализаИзменений = КадровыйУчет.ТаблицаАнализаИзменений();
	
	СтрокаТаблицы = ТаблицаАнализаИзменений.Добавить();
	СтрокаТаблицы.Сотрудник = СсылкаСотрудника;
	СтрокаТаблицы.Организация = Объект.ГоловнаяОрганизация;
	СтрокаТаблицы.ФлагИзменений = 1;
	
	КадровыйУчет.ОбработатьИзменениеОрганизацийВНабореПоТаблицеИзменений(ТаблицаАнализаИзменений);
	
	//3.
	Если ПолучитьФункциональнуюОпцию("ИспользоватьНачислениеЗарплаты") Тогда
			
		УстановитьПривилегированныйРежим(Истина);
		
		НаборЗаписей = РегистрыСведений.ОсновныеСотрудникиФизическихЛиц.СоздатьНаборЗаписей();
		НаборЗаписей.Отбор.ФизическоеЛицо.Установить(Объект.ФизическоеЛицо);
		НаборЗаписей.Отбор.ГоловнаяОрганизация.Установить(Объект.ГоловнаяОрганизация);
		
		НоваяЗаписьРС = НаборЗаписей.Добавить();
		НоваяЗаписьРС.ГоловнаяОрганизация = Объект.ГоловнаяОрганизация;
		НоваяЗаписьРС.ФизическоеЛицо = Объект.ФизическоеЛицо;
		НоваяЗаписьРС.ДатаНачала = '00010101';
		НоваяЗаписьРС.ДатаОкончания = ЗарплатаКадрыПериодическиеРегистры.МаксимальнаяДата();
		НоваяЗаписьРС.Сотрудник = СсылкаСотрудника;
		
		НаборЗаписей.Записать();
		
		УстановитьПривилегированныйРежим(Ложь);
	КонецЕсли;
	//Нети (Батрасова А. 16.05.2019, наряд 0000030890
	Объект.ДополнительныеСвойства.Вставить("ПроверятьБизнесЛогикуПриЗаписи", Истина); 
	Объект.ДополнительныеСвойства.Вставить("СсылкаНового", СсылкаСотрудника);
	//Нети )Батрасова А. 16.05.2019
КонецЕсли;

//Нети (Батрасова А. 15.05.2019, наряд 0000030964
//принудительное обновление данных для подбора сотрудников, т.к. если в УПР было изменено ФИО, и сотрудник тоже зарегистрирован к обмену, сотрудник загружается
//с обновленным наименованием, поэтому в РС Данные для подбора сотрудников наименование не обновляется
Если ОбъектНайден и НЕ Отказ Тогда
	//НВД 16.02.2020 НестеренкоПА, 0000035493 (Наименование из РС ФИО брать нельзя, т.к. наименование получается ФИО + представление, в данном случае необходимо просто перезаписать РС ДанныеДляПодбора)
	//Запрос = Новый Запрос;
	//Запрос.Текст = "ВЫБРАТЬ
	//	|	ФИОФизическихЛицСрезПоследних.ФизическоеЛицо КАК ФизическоеЛицо,
	//	|	ВЫРАЗИТЬ(ФИОФизическихЛицСрезПоследних.Фамилия + ВЫБОР
	//	|			КОГДА ФИОФизическихЛицСрезПоследних.Имя = """"
	//	|				ТОГДА """"
	//	|			ИНАЧЕ "" "" + ФИОФизическихЛицСрезПоследних.Имя
	//	|		КОНЕЦ + ВЫБОР
	//	|			КОГДА ФИОФизическихЛицСрезПоследних.Отчество = """"
	//	|				ТОГДА """"
	//	|			ИНАЧЕ "" "" + ФИОФизическихЛицСрезПоследних.Отчество
	//	|		КОНЕЦ КАК СТРОКА(50)) КАК Наименование
	//	|ИЗ
	//	|	РегистрСведений.ФИОФизическихЛиц.СрезПоследних(&ТекДата, ФизическоеЛицо = &ФизическоеЛицо) КАК ФИОФизическихЛицСрезПоследних";
	//Запрос.УстановитьПараметр("ТекДата", ТекущаяДата());
	//Запрос.УстановитьПараметр("ФизическоеЛицо", Объект.ФизическоеЛицо);
	//
	//Выборка = Запрос.Выполнить().Выбрать();
	//Если Выборка.Следующий() Тогда
	//	Если Выборка.Наименование <> Объект.Ссылка.Наименование Тогда
	//		//Нети (Батрасова А. 23.05.2019, задача внедрения 000029857
	//		//РегистрыСведений.ДанныеДляПодбораСотрудников.ОбновитьНаименованияСотрудника(Объект.Ссылка, Выборка.Наименование);
			УстановитьПривилегированныйРежим(Истина);
	
			НаборЗаписей = РегистрыСведений.ДанныеДляПодбораСотрудников.СоздатьНаборЗаписей();
			НаборЗаписей.Отбор.Сотрудник.Установить(Объект.Ссылка);
			
			НаборЗаписей.Прочитать();
			
			Для каждого Запись Из НаборЗаписей Цикл
				//НВД 16.02.2020 НестеренкоПА, 0000035493 (Вместо ФИО записываем наименование объекта)
				//Запись.Наименование = Выборка.Наименование;
				Запись.Наименование = Объект.Наименование;
			КонецЦикла;
			
			НаборЗаписей.Записать();
			
			УстановитьПривилегированныйРежим(Ложь);
	//		//Нети )Батрасова А. 23.05.2019
	//	КонецЕсли;
	//КонецЕсли;		
	//НВД 16.02.2020 НестеренкоПА, 0000035493
КонецЕсли;
//Нети )Батрасова А. 15.05.2019
