﻿Если Не Источник.ЭтоГруппа Тогда   	
	// Регистр "ФИОФизическийхЛиц"
	Запросы.ФИОФизЛиц.УстановитьПараметр("ДатаАктуальности", ТекущаяДата());
	Запросы.ФИОФизЛиц.УстановитьПараметр("ФизическоеЛицо", Источник.Ссылка);    	
	
	Выборка = Запросы.ФИОФизЛиц.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда  		
		ИсходящиеДанные	= Новый Структура("ФизическоеЛицо, Фамилия, Имя, Отчество, Период, Инициалы");
		ЗаполнитьЗначенияСвойств(ИсходящиеДанные, Выборка);
		
		ВыгрузитьПоПравилу( , , ИсходящиеДанные, , "ФИОФизическихЛиц");   		
	КонецЕсли;
	
	//Регистр ГражданстроФизическихЛиц
	Запросы.ГражданствоФизическихЛиц.УстановитьПараметр("ДатаАктуальности", ТекущаяДата());
	Запросы.ГражданствоФизическихЛиц.УстановитьПараметр("ФизическоеЛицо", Источник.Ссылка);
	Результат = Запросы.ГражданствоФизическихЛиц.Выполнить();
	
	Если НЕ Результат.Пустой() Тогда
		Выборка = Результат.Выбрать();		
		Выборка.Следующий();
		
		ИсходящиеДанные	= Новый Структура("ФизическоеЛицо, Период, Страна, ИНН");
		ЗаполнитьЗначенияСвойств(ИсходящиеДанные, Выборка);
		
		ВыгрузитьПоПравилу( , , ИсходящиеДанные, , "ГражданствоФизическихЛиц"); 
	КонецЕсли;
	
	//Регистр ДокументыФизическихЛиц
	Запросы.ДокументыФизическихЛиц.УстановитьПараметр("ДатаАктуальности", ТекущаяДата());
	Запросы.ДокументыФизическихЛиц.УстановитьПараметр("ФизическоеЛицо", Источник.Ссылка);
	Результат = Запросы.ДокументыФизическихЛиц.Выполнить();
	
	Если НЕ Результат.Пустой() Тогда
		Выборка = Результат.Выбрать();
		
		Пока Выборка.Следующий() Цикл		
			ИсходящиеДанные	= Новый Структура("Физлицо, Период, ВидДокумента, ДатаВыдачи, КемВыдан, КодПодразделения, Номер, Представление, Серия, СрокДействия, ЯвляетсяДокументомУдостоверяющимЛичность, ИмяЛатиницей, ФамилияЛатиницей");
			ЗаполнитьЗначенияСвойств(ИсходящиеДанные, Выборка);
			
			ВыгрузитьПоПравилу( , , ИсходящиеДанные, , "ДокументыФизическихЛиц"); 
		КонецЦикла;
	КонецЕсли;		
КонецЕсли;
