﻿Если ЗначениеЗаполнено(УзелДляОбмена) Тогда
	
	ЭтоУпрОрганизация = ОбщегоНазначенияЗК.ЭтоЦФОРозницы(Источник.Ссылка);
	
	Если УзелДляОбмена.ИспользоватьФильтрПоУпрОрганизациям И ЭтоУпрОрганизация Тогда
		
		Если ЗначениеЗаполнено(Параметры.СписокУпрОрганизаций) Тогда
			
			ОрганизацияРазрешена = Параметры.СписокУпрОрганизаций.Найти(Источник.Ссылка, "Организация");

			Если ОрганизацияРазрешена = Неопределено Тогда
				Отказ = Истина;
			КонецЕсли;
		Иначе
			Отказ = Истина;
		КонецЕсли;
		
	ИначеЕсли УзелДляОбмена.ИспользоватьФильтрПоФинОрганизациям И НЕ ЭтоУпрОрганизация Тогда
		
		Если ЗначениеЗаполнено(Параметры.СписокФинОрганизаций) Тогда
			
			ОрганизацияРазрешена = Параметры.СписокФинОрганизаций.Найти(Источник.Ссылка, "Организация");

			Если ОрганизацияРазрешена = Неопределено Тогда
				Отказ = Истина;
			КонецЕсли;
		Иначе
			Отказ = Истина;
		КонецЕсли;
		
	КонецЕсли;
	
КонецЕсли;
