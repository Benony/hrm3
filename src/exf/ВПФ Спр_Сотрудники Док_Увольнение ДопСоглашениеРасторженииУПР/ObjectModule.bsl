﻿Функция СведенияОВнешнейОбработке() Экспорт
	
	ПараметрыРегистрации = Новый Структура;
	МассивНазначений = Новый Массив;
	МассивНазначений.Добавить("Справочник.Сотрудники");
	МассивНазначений.Добавить("Документ.Увольнение");
	ПараметрыРегистрации.Вставить("Вид", "ПечатнаяФорма");
	ПараметрыРегистрации.Вставить("Назначение", МассивНазначений);
	ПараметрыРегистрации.Вставить("Наименование", "Допсоглашение о расторжении (УПР)");
	ПараметрыРегистрации.Вставить("БезопасныйРежим", ЛОЖЬ);
	ПараметрыРегистрации.Вставить("Версия", "1.0"); 
	ПараметрыРегистрации.Вставить("Информация", "Внешняя печатная форма дополнительного соглашения к трудовому договору (расторжение, УПР)."); 
	ТаблицаКоманд = ПолучитьТаблицуКоманд();
	ДобавитьКоманду(ТаблицаКоманд, "Допсоглашение о расторжении (УПР)", "ДопсоглашениеОРасторжении", "ВызовСерверногоМетода", Истина, "ПечатьMXL");
	ПараметрыРегистрации.Вставить("Команды", ТаблицаКоманд);

	Возврат ПараметрыРегистрации;

КонецФункции

Функция ПолучитьТаблицуКоманд()
	
	Команды = Новый ТаблицаЗначений;
	Команды.Колонки.Добавить("Представление", Новый ОписаниеТипов("Строка"));
	Команды.Колонки.Добавить("Идентификатор", Новый ОписаниеТипов("Строка"));
	Команды.Колонки.Добавить("Использование", Новый ОписаниеТипов("Строка"));
	Команды.Колонки.Добавить("ПоказыватьОповещение", Новый ОписаниеТипов("Булево"));
	Команды.Колонки.Добавить("Модификатор", Новый ОписаниеТипов("Строка"));
	Возврат Команды;

КонецФункции

Процедура ДобавитьКоманду(ТаблицаКоманд, Представление, Идентификатор, Использование, ПоказыватьОповещение = Ложь, Модификатор = "")

	НоваяКоманда = ТаблицаКоманд.Добавить();
	НоваяКоманда.Представление = Представление; 
	НоваяКоманда.Идентификатор = Идентификатор;
	НоваяКоманда.Использование = Использование;
	НоваяКоманда.ПоказыватьОповещение = ПоказыватьОповещение;
	НоваяКоманда.Модификатор = Модификатор;

КонецПроцедуры

Процедура Печать(МассивОбъектов, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	
	Если ТипЗнч(МассивОбъектов[0].Ссылка) = Тип("СправочникСсылка.Сотрудники") Тогда
		СсылкаНаСотрудника = МассивОбъектов[0].Ссылка;
	ИначеЕсли ТипЗнч(МассивОбъектов[0].Ссылка) = Тип("ДокументСсылка.Увольнение") Тогда
		СсылкаНаСотрудника = МассивОбъектов[0].Сотрудник.Ссылка;
	КонецЕсли;
	ЭтотОбъект.СсылкаНаОбъект = СсылкаНаСотрудника;
	УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(КоллекцияПечатныхФорм, "ДопсоглашениеОРасторжении", "Допсоглашение о расторжении", СформироватьПечатнуюФорму(СсылкаНаСотрудника, ОбъектыПечати));

КонецПроцедуры

Функция СформироватьПечатнуюФорму(СсылкаНаОбъект, ОбъектыПечати)

	ТабДокумент = Новый ТабличныйДокумент;
	ТабДокумент.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_ДопсоглашениеОРасторженииУПР";
	
	// получаем данные для печати
	Выборка = СформироватьЗапросДляПечати().Выбрать();
	
	// запоминаем области макета
	Макет = ПолучитьМакет("ДопсоглашениеОРасторженииУПР");
	ОбластьМакетаДоговор = Макет.ПолучитьОбласть("Допсоглашение");
	
	Выборка.Следующий();
	ОбластьМакетаДоговор.Параметры.Заполнить(Выборка);
	ОбластьМакетаДоговор.Параметры.ДатаСоглашения = Формат(ТекущаяДата(), "ДЛФ=DD");
	ОбластьМакетаДоговор.Параметры.ФИОСотрудникаТП = СклонениеПредставленийОбъектов.ПросклонятьПредставлениеПоВсемПадежам(СсылкаНаОбъект.ФизическоеЛицо.ФИО, Ложь, Неопределено, Ложь).ТворительныйПадеж;
	ОбластьМакетаДоговор.Параметры.Должность = НРег(СклонениеПредставленийОбъектов.ПросклонятьПредставлениеПоВсемПадежам(Выборка.Должность, Ложь, Неопределено, Ложь).РодительныйПадеж);
	ОбластьМакетаДоговор.Параметры.ЫмОй = ?(Выборка.Пол = "Мужской", "ым", "ой");
	ОбластьМакетаДоговор.Параметры.ГодПодписания = Год(ТекущаяДата());
	ОбластьМакетаДоговор.Параметры.ТрудовойДоговорДата = Формат(Выборка.ТрудовойДоговорДата, "ДЛФ=DD");
	ВыборкаДатыУвольнения = НайтиДокументУвольнение(СсылкаНаОбъект);
	Если НЕ ВыборкаДатыУвольнения = Ложь Тогда
		ОбластьМакетаДоговор.Параметры.ДатаУвольнения = Формат(ВыборкаДатыУвольнения.ДатаУвольнения, "ДЛФ=DD");
		УвольнениеСсылка = ВыборкаДатыУвольнения.УвольнениеСсылка;
		СуммаВыходногоПособия = УвольнениеСсылка.Начисления.Итог("Результат") + УвольнениеСсылка.Пособия.Итог("Результат") + УвольнениеСсылка.Льготы.Итог("Результат") - УвольнениеСсылка.НДФЛ.Итог("Налог") - УвольнениеСсылка.Удержания.Итог("Результат") - УвольнениеСсылка.ПогашениеЗаймов.Итог("ПогашениеЗайма") - УвольнениеСсылка.ПогашениеЗаймов.Итог("ПогашениеПроцентов") - УвольнениеСсылка.ПогашениеЗаймов.Итог("НалогНаМатериальнуюВыгоду");
		ОбластьМакетаДоговор.Параметры.РублиВыходногоПособия = Цел(СуммаВыходногоПособия);
		ОбластьМакетаДоговор.Параметры.КопейкиВыходногоПособия = Прав(СуммаВыходногоПособия, 2);
		ОбластьМакетаДоговор.Параметры.СуммаВыходногоПособияПрописью = ЧислоПрописью(СуммаВыходногоПособия, , "рубль, рубля, рублей, м, копейка, копейки, копеек, ж, 2" );
	Иначе
		Сообщить("Не обнаружена информация о проведенном документе Увольнение по сотруднику " + СсылкаНаОбъект);
	КонецЕсли;
	Если НЕ Выборка.ПодразделениеСотрудникаСсылка = Справочники.ПодразделенияОрганизаций.ПустаяСсылка() Тогда
		ОтветственноеЛицо = НайтиОтветственноеЛицоДляПодразделенияСотрудника(Выборка.ПодразделениеСотрудникаСсылка);
		Если НЕ ОтветственноеЛицо = Ложь Тогда
			//заполняем параметры макета ВПФ
			ДолжностьРаботодателя = ОтветственноеЛицо.Должность;
			ОбластьМакетаДоговор.Параметры.ДолжностьРаботодателя = СклонениеПредставленийОбъектов.ПросклонятьПредставлениеПоВсемПадежам(ДолжностьРаботодателя, Ложь, Неопределено, Ложь).РодительныйПадеж;
			ФизЛицоРаботодателя = ОтветственноеЛицо.ФизическоеЛицо;
			ОбластьМакетаДоговор.Параметры.ФИОРаботодателяРП = СклонениеПредставленийОбъектов.ПросклонятьПредставлениеПоВсемПадежам(ФизЛицоРаботодателя.ФИО, Ложь, Неопределено, Ложь).РодительныйПадеж;
			ОбластьМакетаДоговор.Параметры.ФИОРаботодателя = ФизЛицоРаботодателя.ФИО;
			ОбластьМакетаДоговор.Параметры.ДокументПраваПодписи = ?(ОтветственноеЛицо.ДокументПраваПодписи = "Доверенность", "доверенности", "приказа");
	        ОбластьМакетаДоговор.Параметры.НомерДокументаПраваПодписи = ОтветственноеЛицо.НомерДокументаПраваПодписи;
	        ОбластьМакетаДоговор.Параметры.ДатаДокументаПраваПодписи = Формат(ОтветственноеЛицо.ДатаДокументаПраваПодписи, "ДЛФ=D");
		Иначе
			Сообщить("Не обнаружена информация об ответственном лице с правом подписи");	
		КонецЕсли;
	КонецЕсли;
	
	// выводим данные
	ТабДокумент.Вывести(ОбластьМакетаДоговор);
	
	Возврат ТабДокумент;

КонецФункции

Функция СформироватьЗапросДляПечати() Экспорт
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	ТолькоРазрешенные = Истина;
	ДатаСрезаИнформации = ТекущаяДата();
	КадровыеДанные = "ФИОПолные, Подразделение, Организация, Должность, Пол, ТрудовойДоговорДата";
	МассивСотрудников = Новый Массив;
	МассивСотрудников.Добавить(СсылкаНаОбъект);
	КадровыеДанныеСотрудников = КадровыйУчет.КадровыеДанныеСотрудников(ТолькоРазрешенные, МассивСотрудников, КадровыеДанные, ДатаСрезаИнформации);

	Для Каждого Строка Из КадровыеДанныеСотрудников Цикл
		ФИОСотрудника = Строка.ФИОПолные;
		ПодразделениеСотрудника = Строка.Подразделение;
		Пол = Строка.Пол;
		ТрудовойДоговорДата = Строка.ТрудовойДоговорДата;
		ПолноеНаименованиеОрганизации = Строка.Организация.НаименованиеПолное;
		Должность = Строка.Должность;
	КонецЦикла;
	
	//Невада (11.02.2019 Решетняк Г.В.
	ТЗ = Новый ТаблицаЗначений;
	ПараметрСтроки0 = Новый КвалификаторыСтроки(0);
	ПараметрСтроки7 = Новый КвалификаторыСтроки(7);
	ПараметрСтроки50 = Новый КвалификаторыСтроки(50);
	ПараметрСтроки150 = Новый КвалификаторыСтроки(150);
	ТЗ.Колонки.Добавить("Город", Новый ОписаниеТипов("Строка",,,,ПараметрСтроки50,,),,);
	ТЗ.Колонки.Добавить("ФИОСотрудника", Новый ОписаниеТипов("Строка",,,,ПараметрСтроки150,,),,);
	ТЗ.Колонки.Добавить("Пол", Новый ОписаниеТипов("Строка",,,,ПараметрСтроки7,,),,);
	ТЗ.Колонки.Добавить("Должность", Новый ОписаниеТипов("СправочникСсылка.Должности",,,,,,),,);
	ТЗ.Колонки.Добавить("ПолноеНаименованиеОрганизации", Новый ОписаниеТипов("Строка",,,,ПараметрСтроки0,,),,);
	ТЗ.Колонки.Добавить("ПодразделениеСотрудникаСсылка", Новый ОписаниеТипов("СправочникСсылка.ПодразделенияОрганизаций",,,,,,),,);
	ТЗ.Колонки.Добавить("ТрудовойДоговорДата", Новый ОписаниеТипов("Дата",,,,,,),,);
	
	НоваяСтрока = ТЗ.Добавить();
	НоваяСтрока.ФИОСотрудника = ФИОСотрудника;
	НоваяСтрока.Пол = Пол;
	НоваяСтрока.ПолноеНаименованиеОрганизации = ПолноеНаименованиеОрганизации;
	НоваяСтрока.ТрудовойДоговорДата = ТрудовойДоговорДата;
	НоваяСтрока.ПодразделениеСотрудникаСсылка = ПодразделениеСотрудника;
	НоваяСтрока.Должность = Должность;
	
	//АдресаОрганизаций = УправлениеКонтактнойИнформациейЗарплатаКадры.АдресаОрганизаций(ОрганизацияСотрудника);
	//ОписаниеФактическогоАдреса = УправлениеКонтактнойИнформациейЗарплатаКадры.АдресОрганизации(
	//		АдресаОрганизаций,
	//		ОрганизацияСотрудника,
	//		Справочники.ВидыКонтактнойИнформации.ФактАдресОрганизации);
	//НоваяСтрока.ОрганизациянвГород = ОписаниеФактическогоАдреса.Город;
	
	АдресСтруктура = ЗарплатаКадры.СтруктураАдресаИзXML(ПодразделениеСотрудника.нвуАдресВнутреннееПредставление, Неопределено);
	Если АдресСтруктура.Свойство("Город") И НЕ ПустаяСтрока(АдресСтруктура.Город) Тогда
		Город = АдресСтруктура.Город;
	ИначеЕсли АдресСтруктура.Свойство("НаселенныйПункт") И НЕ ПустаяСтрока(АдресСтруктура.НаселенныйПункт) Тогда
		Город = АдресСтруктура.НаселенныйПункт;
	ИначеЕсли АдресСтруктура.Свойство("Регион") И НЕ ПустаяСтрока(АдресСтруктура.Регион) Тогда
		Город = АдресСтруктура.Регион;
	КонецЕсли;
	НоваяСтрока.Город = Лев(Город,СтрДлина(Город)-2);
	
	Запрос.Текст = "ВЫБРАТЬ
	               |	ТЗ.ФИОСотрудника КАК ФИОСотрудника,
				   |	ТЗ.Город КАК Город,
				   |	ТЗ.Пол КАК Пол,
				   |	ТЗ.ТрудовойДоговорДата КАК ТрудовойДоговорДата,
				   |	ТЗ.ПолноеНаименованиеОрганизации КАК ПолноеНаименованиеОрганизации,
				   |	ТЗ.ПодразделениеСотрудникаСсылка КАК ПодразделениеСотрудникаСсылка,
				   |	ТЗ.Должность КАК Должность
	               |ПОМЕСТИТЬ ВТ_ИсходныеДанныеВПФ
	               |ИЗ
	               |	&ТЗ КАК ТЗ";
	Запрос.УстановитьПараметр("ТЗ", ТЗ);
	Запрос.Выполнить();
	
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ИсходныеДанныеВПФ.ФИОСотрудника КАК ФИОСотрудника,
	|	ИсходныеДанныеВПФ.Город КАК Город,
	|	ИсходныеДанныеВПФ.Пол КАК Пол,
	|	ИсходныеДанныеВПФ.ТрудовойДоговорДата КАК ТрудовойДоговорДата,
	|	ИсходныеДанныеВПФ.Должность КАК Должность,
	|	ИсходныеДанныеВПФ.ПолноеНаименованиеОрганизации КАК ПолноеНаименованиеОрганизации,
	|	ИсходныеДанныеВПФ.ПодразделениеСотрудникаСсылка КАК ПодразделениеСотрудникаСсылка
	|ИЗ
	|	ВТ_ИсходныеДанныеВПФ КАК ИсходныеДанныеВПФ";
	
	Возврат Запрос.Выполнить();
	
КонецФункции

Функция НайтиОтветственноеЛицоДляПодразделенияСотрудника(Подразделение) //заготовка, потом в общий модуль нвСтруктураКомпании, НВ_Общий

	НужныеДанные = СделатьЗапросОтветственногоЛица(Подразделение);
	Если НЕ НужныеДанные = Ложь Тогда
		Возврат НужныеДанные
	Иначе
		//обходим иерархию до самого верха
		ПодразделениеКандидат = Подразделение;
		Пока НЕ ПодразделениеКандидат = Справочники.ПодразделенияОрганизаций.ПустаяСсылка() Цикл
			ПодразделениеКандидат = ПодразделениеКандидат.Родитель;
			НужныеДанные = СделатьЗапросОтветственногоЛица(ПодразделениеКандидат);
			Если НужныеДанные = Ложь Тогда
				Продолжить
			Иначе
				Возврат НужныеДанные
			КонецЕсли
		КонецЦикла;
	КонецЕсли;
	//последняя попытка поиска ответственного лица для организации
	Возврат СделатьЗапросОтветственногоЛица(Подразделение.Владелец);
	
КонецФункции

Функция СделатьЗапросОтветственногоЛица(СтруктурнаяЕдиница)
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ ПЕРВЫЕ 1
	               |	нвСведенияОбОтветственныхЛицахСрезПоследних.Должность КАК Должность,
	               |	нвСведенияОбОтветственныхЛицахСрезПоследних.ДокументПраваПодписи КАК ДокументПраваПодписи,
	               |	нвСведенияОбОтветственныхЛицахСрезПоследних.НомерДокументаПраваПодписи КАК НомерДокументаПраваПодписи,
	               |	нвСведенияОбОтветственныхЛицахСрезПоследних.ДатаДокументаПраваПодписи КАК ДатаДокументаПраваПодписи,
	               |	нвСведенияОбОтветственныхЛицахСрезПоследних.ФизическоеЛицо КАК ФизическоеЛицо
	               |ИЗ
	               |	РегистрСведений.нвСведенияОбОтветственныхЛицах.СрезПоследних(, СтруктурнаяЕдиница = &СтруктурнаяЕдиница) КАК нвСведенияОбОтветственныхЛицахСрезПоследних
	               |ГДЕ
	               |	нвСведенияОбОтветственныхЛицахСрезПоследних.ПравоПодписиПоДоверенности
	               |
	               |УПОРЯДОЧИТЬ ПО
	               |	нвСведенияОбОтветственныхЛицахСрезПоследних.Период УБЫВ";
	Запрос.УстановитьПараметр("СтруктурнаяЕдиница", СтруктурнаяЕдиница);
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() И Выборка.Количество() = 1 Тогда
		Возврат Выборка
	Иначе
		Возврат Ложь
	КонецЕсли;
КонецФункции

Функция НайтиДокументУвольнение(Сотрудник)
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	Увольнение.ДатаУвольнения КАК ДатаУвольнения,
	               |	Увольнение.Ссылка КАК УвольнениеСсылка
	               |ИЗ
	               |	Документ.Увольнение КАК Увольнение
	               |ГДЕ
	               |	Увольнение.Сотрудник = &Сотрудник
	               |	И Увольнение.Проведен";
	Запрос.УстановитьПараметр("Сотрудник", Сотрудник);
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() И Выборка.Количество() = 1 Тогда
		Возврат Выборка
	Иначе
		Возврат Ложь
	КонецЕсли;
КонецФункции