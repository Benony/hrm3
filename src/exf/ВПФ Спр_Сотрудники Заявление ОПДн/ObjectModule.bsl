﻿Функция СведенияОВнешнейОбработке() Экспорт
	
	ПараметрыРегистрации = Новый Структура;
	МассивНазначений = Новый Массив;
	МассивНазначений.Добавить("Справочник.Сотрудники");
	ПараметрыРегистрации.Вставить("Вид", "ПечатнаяФорма");
	ПараметрыРегистрации.Вставить("Назначение", МассивНазначений);
	ПараметрыРегистрации.Вставить("Наименование", "Заявление согласия на ОПДн");
	ПараметрыРегистрации.Вставить("БезопасныйРежим", ЛОЖЬ);
	ПараметрыРегистрации.Вставить("Версия", "1.0"); 
	ПараметрыРегистрации.Вставить("Информация", "Внешняя печатная форма договора об индивидуальной материальной ответственности."); 
	ТаблицаКоманд = ПолучитьТаблицуКоманд();
	ДобавитьКоманду(ТаблицаКоманд, "Заявление согласия на ОПДн", "ЗаявлениеОПДн", "ВызовСерверногоМетода", Истина, "ПечатьMXL");
	ПараметрыРегистрации.Вставить("Команды", ТаблицаКоманд);

	Возврат ПараметрыРегистрации;

КонецФункции

Функция ПолучитьТаблицуКоманд()
	
	Команды = Новый ТаблицаЗначений;
	Команды.Колонки.Добавить("Представление", Новый ОписаниеТипов("Строка"));
	Команды.Колонки.Добавить("Идентификатор", Новый ОписаниеТипов("Строка"));
	Команды.Колонки.Добавить("Использование", Новый ОписаниеТипов("Строка"));
	Команды.Колонки.Добавить("ПоказыватьОповещение", Новый ОписаниеТипов("Булево"));
	Команды.Колонки.Добавить("Модификатор", Новый ОписаниеТипов("Строка"));
	Возврат Команды;

КонецФункции

Процедура ДобавитьКоманду(ТаблицаКоманд, Представление, Идентификатор, Использование, ПоказыватьОповещение = Ложь, Модификатор = "")

	НоваяКоманда = ТаблицаКоманд.Добавить();
	НоваяКоманда.Представление = Представление; 
	НоваяКоманда.Идентификатор = Идентификатор;
	НоваяКоманда.Использование = Использование;
	НоваяКоманда.ПоказыватьОповещение = ПоказыватьОповещение;
	НоваяКоманда.Модификатор = Модификатор;

КонецПроцедуры

Процедура Печать(МассивОбъектов, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	
	ЭтотОбъект.СсылкаНаОбъект = МассивОбъектов[0].Ссылка;
	УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(КоллекцияПечатныхФорм, "ЗаявлениеОПДн", "Заявление согласия на ОПДн", СформироватьПечатнуюФорму(МассивОбъектов[0].Ссылка, ОбъектыПечати));

КонецПроцедуры

Функция СформироватьПечатнуюФорму(СсылкаНаОбъект, ОбъектыПечати)

	ТабДокумент = Новый ТабличныйДокумент;
	ТабДокумент.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_ЗаявлениеОПДн";
	
	// получаем данные для печати
	ТолькоРазрешенные = Истина;
	ДатаСрезаИнформации = ТекущаяДата();
	КадровыеДанные = "Организация, ФИОПолные, АдресПоПропискеПредставление, ДокументВид, ДокументСерия, ДокументНомер, ДокументКемВыдан, ДокументДатаВыдачи";//Невада (14.03.2019 Решетняк Г.В. задача тестирования 0000028378

	МассивСотрудников = Новый Массив;
	МассивСотрудников.Добавить(СсылкаНаОбъект);

	КадровыеДанныеСотрудников = КадровыйУчет.КадровыеДанныеСотрудников(ТолькоРазрешенные, МассивСотрудников, КадровыеДанные, ДатаСрезаИнформации);

	Для Каждого Строка Из КадровыеДанныеСотрудников Цикл
		ОрганизацияСотрудника = Строка.Организация;
		//Невада (14.03.2019 Решетняк Г.В. задача тестирования 0000028378
		ФИОПолные = Строка.ФИОПолные;
		АдресСотрудника = Строка.АдресПоПропискеПредставление;
		ДокументВид = Строка.ДокументВид;
		ДокументСерия = Строка.ДокументСерия;
		ДокументНомер = Строка.ДокументНомер;
		ДокументКемВыдан = Строка.ДокументКемВыдан;
		ДокументДатаВыдачи = Строка.ДокументДатаВыдачи;
		//Невада (14.03.2019 Решетняк Г.В. задача тестирования 0000028378
	КонецЦикла;
	
	НаименованиеПолное = ОрганизацияСотрудника.НаименованиеПолное;
	ОрганизацияНаименование = ОрганизацияСотрудника.Наименование;
		
	АдресаОрганизаций = УправлениеКонтактнойИнформациейЗарплатаКадры.АдресаОрганизаций(ОрганизацияСотрудника);
	ОписаниеФактическогоАдреса = УправлениеКонтактнойИнформациейЗарплатаКадры.АдресОрганизации(
			АдресаОрганизаций,
			ОрганизацияСотрудника,
			Справочники.ВидыКонтактнойИнформации.ФактАдресОрганизации);
	нвГород = ОписаниеФактическогоАдреса.Город;
	
	// запоминаем области макета
	Макет = ПолучитьМакет("ЗаявлениеОПДн");
	ОбластьМакетаДоговор = Макет.ПолучитьОбласть("Договор"); // Договор
	
	ОбособленноеПодразделение = ПолучитьОбособленноеПодразделение();
	
	//параметры ОПДн
	ОбластьМакетаДоговор.Параметры.ПолноеНазваниеОрганизации = НаименованиеПолное;
	ОбластьМакетаДоговор.Параметры.КраткоеНазваниеОрганизации = ОрганизацияСотрудника.Наименование;
	//параметры ОПДн
	
	Если ОбособленноеПодразделение <> Неопределено Тогда
		
		ЗначениеАдреса = УправлениеСвойствами.ЗначениеСвойства(ОбособленноеПодразделение, "Адрес");
		Если СтрДлина(ЗначениеАдреса) > 0 Тогда
			ОбластьМакетаДоговор.Параметры.АдресОрганизации = ЗначениеАдреса;
		КонецЕсли;
				
		//НВД 2020.05.26 РешетнякГВ, наряд 0000037102(
		//ОбособленноеПодразделениеРП = СклонениеПредставленийОбъектов.ПросклонятьПредставлениеПоВсемПадежам(ОбособленноеПодразделение, Ложь, Неопределено, Ложь).РодительныйПадеж;
		АдресСтруктурыСклонения = Новый УникальныйИдентификатор;
		ПараметрыСклонения = Новый Структура("ЭтоФИО, Пол", Ложь, Неопределено);
		АдресСтруктурыСклонения = СклонениеПредставленийОбъектовВызовСервера.ДлительнаяОперацияСклоненияПоПадежам(АдресСтруктурыСклонения, ОбособленноеПодразделение, ПараметрыСклонения);
		ОбособленноеПодразделениеРП = ПолучитьИзВременногоХранилища(АдресСтруктурыСклонения.АдресРезультата).Родительный;
		//)НВД 2020.05.26 РешетнякГВ
		ОбластьМакетаДоговор.Параметры.НаименованиеОПРП = ОбособленноеПодразделениеРП;
				
		//Заполнение города
		ЗначениеГорода = УправлениеСвойствами.ЗначениеСвойства(ОбособленноеПодразделение, "Город");
		Если ЗначениеГорода = "" или ЗначениеГорода = Неопределено Тогда
			ОбластьМакетаДоговор.Параметры.нвГород = "Хабаровск"
		Иначе
			ОбластьМакетаДоговор.Параметры.нвГород = ЗначениеГорода;
		КонецЕсли;
	Иначе
		
		ОбластьМакетаДоговор.Параметры.нвГород = "Хабаровск"
		
	КонецЕсли;
	
	//Невада (14.03.2019 Решетняк Г.В. задача тестирования 0000028378
	ОбластьМакетаДоговор.Параметры.ФИОСотрудника = ФИОПолные;
	ОбластьмакетаДоговор.Параметры.АдресСотрудника = АдресСотрудника;
	ОбластьМакетаДоговор.Параметры.ДокументВид = ДокументВид;
	ОбластьМакетаДоговор.Параметры.ДокументСерия = ДокументСерия;
	ОбластьМакетаДоговор.Параметры.ДокументНомер = ДокументНомер;
	ОбластьМакетаДоговор.Параметры.ДокументКемВыдан = ДокументКемВыдан;
	ОбластьМакетаДоговор.Параметры.ДокДатаВыдачи = Формат(ДокументДатаВыдачи, "ДЛФ=DD");//Невада (20.05.2019 Решетняк Г.В. задача тестирования 000029612 добавлена форматная строка для даты выдачи
	//Невада )14.03.2019 Решетняк Г.В. задача тестирования 0000028378
	
	// выводим данные
	ТабДокумент.Вывести(ОбластьМакетаДоговор);
	
	//НВД 2020.02.19 РешетнякГВ, 0000035749 (
	ТабДокумент.НижнийКолонтитул.Выводить = Истина;
	ТабДокумент.НижнийКолонтитул.НачальнаяСтраница = 1;
	ШрифтКолонтитула = Новый Шрифт("Times New Roman", 12,,,,,);
	ТабДокумент.НижнийКолонтитул.Шрифт = ШрифтКолонтитула;
	ТабДокумент.НижнийКолонтитул.ТекстСправа = "Подпись работника ______________________";
	ТабДокумент.НижнийКолонтитул.ВертикальноеПоложение = ВертикальноеПоложение.Центр; 
	//)НВД 2020.02.19 РешетнякГВ
	
	Возврат ТабДокумент;

КонецФункции

Функция ПолучитьОбособленноеПодразделение()	
	
	ТолькоРазрешенные = Истина;
	ДатаСрезаИнформации = ТекущаяДата();
	КадровыеДанные = "Подразделение";

	МассивСотрудников = Новый Массив;
	МассивСотрудников.Добавить(СсылкаНаОбъект.Ссылка);

	КадровыеДанныеСотрудников = КадровыйУчет.КадровыеДанныеСотрудников(ТолькоРазрешенные, МассивСотрудников, КадровыеДанные, ДатаСрезаИнформации);

	Для Каждого Строка Из КадровыеДанныеСотрудников Цикл
		ТекущееПодразделение = Строка.Подразделение;
	КонецЦикла;
	
	МассивПодразделений = Новый Массив;
	МассивПодразделений.Добавить(ТекущееПодразделение);
	
    Пока ТекущееПодразделение.Родитель <> Справочники.ПодразделенияОрганизаций.ПустаяСсылка() Цикл
		ТекущееПодразделение = ТекущееПодразделение.Родитель;
		МассивПодразделений.Добавить(ТекущееПодразделение);
	КонецЦикла;

	Для Каждого Подразделение из МассивПодразделений Цикл
		Если НЕ нвНастройкиПовтИсп.ЭтоБазаУпр() И Подразделение.ОбособленноеПодразделение Тогда
			ОбособленноеПодразделение = Подразделение.Ссылка;
		КонецЕсли;
	КонецЦикла;
	
	Возврат ОбособленноеПодразделение;
	
КонецФункции