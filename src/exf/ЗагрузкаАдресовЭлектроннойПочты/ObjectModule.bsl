﻿# Область Подключение
Функция СведенияОВнешнейОбработке() Экспорт
	
	//Инициализируем структуру с параметрами регистрации
	
	//Определяем список объектов, вызывающих обработку	
	ПараметрыРегистрации = ПолучитьПараметрыРегистрации();
	ПараметрыРегистрации.Версия = "1.0";

	//Определяем команды для печати формы
	
	ТаблицаКоманд = ПолучитьТаблицуКоманд();

	ДобавитьКоманду(ТаблицаКоманд,
		"Загрузка адресов электронной почты", // Представление команды в пользовательском интерфейсе
		"ЗагрузкаЕмейлов",
		"ВызовСерверногоМетода", // Уникальный идентификатор команды
	);

	ПараметрыРегистрации.Вставить("Команды", ТаблицаКоманд);

	Возврат ПараметрыРегистрации;

КонецФункции

Процедура ДобавитьКоманду(ТаблицаКоманд, Представление, Идентификатор, Использование = "ВызовСерверногоМетода", ПоказыватьОповещение = Истина, Модификатор = "")

	НоваяКоманда = ТаблицаКоманд.Добавить();
	НоваяКоманда.Представление = Представление;
	НоваяКоманда.Идентификатор = Идентификатор;
	НоваяКоманда.Использование = Использование;
	НоваяКоманда.ПоказыватьОповещение = ПоказыватьОповещение;
	НоваяКоманда.Модификатор = Модификатор;

КонецПроцедуры

Функция ПолучитьПараметрыРегистрации(ОбъектыНазначенияФормы = Неопределено, НаименованиеОбработки = "", Информация = "", Версия = "1.0")
	
	ПараметрыРегистрации = Новый Структура;
	ПараметрыРегистрации.Вставить("Вид", "ДополнительнаяОбработка");
	ПараметрыРегистрации.Вставить("БезопасныйРежим", Ложь);
	ПараметрыРегистрации.Вставить("Назначение");
	
	Если Не ЗначениеЗаполнено(НаименованиеОбработки) Тогда
		НаименованиеОбработки = ЭтотОбъект.Метаданные().Представление();
	КонецЕсли; 
	
	ПараметрыРегистрации.Вставить("Наименование", НаименованиеОбработки);
	
	Если Не ЗначениеЗаполнено(Информация) Тогда
		
		Информация = ЭтотОбъект.Метаданные().Комментарий;
		
	КонецЕсли; 
	
	ПараметрыРегистрации.Вставить("Информация", Информация);
	
	ПараметрыРегистрации.Вставить("Версия", Версия);

	Возврат ПараметрыРегистрации;

КонецФункции

// Формирует таблицу значений с командами печати
//	
// Возвращаемое значение:
//		ТаблицаЗначений
//
Функция ПолучитьТаблицуКоманд()

	Команды = Новый ТаблицаЗначений;
	
	//Представление команды в пользовательском интерфейсе
	Команды.Колонки.Добавить("Представление", Новый ОписаниеТипов("Строка"));
	
	//Уникальный идентификатор команды или имя макета печати
	Команды.Колонки.Добавить("Идентификатор", Новый ОписаниеТипов("Строка"));
	
	//Способ вызова команды: "ОткрытиеФормы", "ВызовКлиентскогоМетода", "ВызовСерверногоМетода"
	// "ОткрытиеФормы" - применяется только для отчетов и дополнительных отчетов
	// "ВызовКлиентскогоМетода" - вызов процедуры Печать(), определённой в модуле формы обработки
	// "ВызовСерверногоМетода" - вызов процедуры Печать(), определённой в модуле объекта обработки
	Команды.Колонки.Добавить("Использование", Новый ОписаниеТипов("Строка"));
	
	//Показывать оповещение.
	//Если Истина, требуется показать оповещение при начале и при завершении работы обработки. 
	//Имеет смысл только при запуске обработки без открытия формы
	Команды.Колонки.Добавить("ПоказыватьОповещение", Новый ОписаниеТипов("Булево"));
	
	//Дополнительный модификатор команды. 
	//Используется для дополнительных обработок печатных форм на основе табличных макетов.
	//Для таких команд должен содержать строку ПечатьMXL
	Команды.Колонки.Добавить("Модификатор", Новый ОписаниеТипов("Строка"));

	Возврат Команды;

КонецФункции
# КонецОбласти

&НаСервере
Процедура ВыполнитьКоманду(ИдентификаторКоманды, ПараметрыВыполненияКоманды = Неопределено) Экспорт
	
	ТекстовыйДокумент = Новый ТекстовыйДокумент;
	Попытка
		ТекстовыйДокумент.Прочитать("\\zup-corp\RepADout.txt");
	Исключение
		Сообщить("Ошибка открытия файла!");
	КонецПопытки;
	Для ТекущаяСтрока = 1 По ТекстовыйДокумент.КоличествоСтрок() Цикл
		Попытка
			МассивСтроки = мРазложитьСтрокуВМассивПодстрок(ТекстовыйДокумент.ПолучитьСтроку(ТекущаяСтрока),Символы.Таб);
			СтрокаГУИД = МассивСтроки[0];
			СтрокаАдрес = МассивСтроки[1];
			ЗаполнитьАдресЭлектроннойПочты(СтрокаГУИД,СтрокаАдрес);
		Исключение
			Сообщить("Не удалось получить адрес электронной почты в Строке "+СокрЛП(ТекущаяСтрока));
		КонецПопытки;
	КонецЦикла;
	
КонецПроцедуры

Функция мРазложитьСтрокуВМассивПодстрок(Знач Стр, Разделитель = ",")
	
	МассивСтрок = Новый Массив();
	Если Разделитель = " " Тогда
		Стр = СокрЛП(Стр);
		Пока 1=1 Цикл
			Поз = Найти(Стр,Разделитель);
			Если Поз=0 Тогда
				МассивСтрок.Добавить(Стр);
				Возврат МассивСтрок;
			КонецЕсли;
			МассивСтрок.Добавить(Лев(Стр,Поз-1));
			Стр = СокрЛ(Сред(Стр,Поз));
		КонецЦикла;
	Иначе
		ДлинаРазделителя = СтрДлина(Разделитель);
		Пока 1=1 Цикл
			Поз = Найти(Стр,Разделитель);
			Если Поз=0 Тогда
				МассивСтрок.Добавить(Стр);
				Возврат МассивСтрок;
			КонецЕсли;
			МассивСтрок.Добавить(Лев(Стр,Поз-1));
			Стр = Сред(Стр,Поз+ДлинаРазделителя);
		КонецЦикла;                       
	КонецЕсли;
	
КонецФункции

&НаСервере
Процедура ЗаполнитьАдресЭлектроннойПочты(СтрокаГУИД,СтрокаАдрес)
	
	Физлицо = Справочники.ФизическиеЛица.ПолучитьСсылку(Новый УникальныйИдентификатор(Нрег(СтрокаГУИД))).ПолучитьОбъект();
	Если Физлицо <> Неопределено Тогда
		
		СтрокаАдреса = Физлицо.КонтактнаяИнформация.Найти(Перечисления.ТипыКонтактнойИнформации.АдресЭлектроннойПочты,"Тип");
		СтрокаАдреса.АдресЭП = СтрокаАдрес;
		СтрокаАдреса.Представление = СтрокаАдрес; 
		Физлицо.Записать();
		
	КонецЕсли;	
	
КонецПроцедуры;