﻿
Функция CreateVacation(GUIDDocuments, RecordingMode, NewDocument, DeleteMark)
	
	Если NewDocument тогда
		
		GUIDNewDocuments = нвд_wСоздатьДокументы.СоздатьДокументОтпуск(GUIDDocuments,RecordingMode);
		GUIDNewDocuments = Строка(GUIDNewDocuments);
		
		Возврат GUIDNewDocuments;
	Иначе
		
		GUIDNewDocuments = нвд_wСоздатьДокументы.ПровестиДокументОтпуск(GUIDDocuments,RecordingMode,DeleteMark);
		Возврат GUIDNewDocuments;

	КонецЕсли;
КонецФункции
