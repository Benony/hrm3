﻿
Функция NalogiByPeriod(Kod) 

	XDTOДанныеФин = нвwДанныеФинПоЗапросамУПР.НалогиФИН(Kod);	
	
	Возврат XDTOДанныеФин;
	
КонецФункции

Функция StrahByPeriod(Kod) 

	XDTOДанныеФин = нвwДанныеФинПоЗапросамУПР.СтраховыеФИН(Kod);	
	
	Возврат XDTOДанныеФин;
	
КонецФункции

Функция DeleteRegistration(Kod, Registrators, RegistratorsStrah)
	
	МассивДок = Новый Массив;
		
	Для каждого Registrator Из Registrators.Registrator Цикл
		МассивДок.Добавить(нвwДанныеФинПоЗапросамУПР.ПолучитьСсылкуНаДокументПоГуид(Registrator.Name, Registrator.UUID));
	КонецЦикла;
	
	//Нети (Батрасова А. 25.05.2020, наряд 0000037087
	//Если НЕ ЗначениеЗаполнено(МассивДок) Тогда
	//	Возврат Неопределено;	
	//КонецЕсли;
	//Нети )Батрасова А. 25.05.2020
	
	МассивДокСтрах = Новый Массив;
	
	Для каждого Registrator Из RegistratorsStrah.Registrator Цикл
		МассивДокСтрах.Добавить(нвwДанныеФинПоЗапросамУПР.ПолучитьСсылкуНаДокументПоГуид(Registrator.Name, Registrator.UUID));
	КонецЦикла;
	
	//Нети (Батрасова А. 25.05.2020, наряд 0000037087
	//Если НЕ ЗначениеЗаполнено(МассивДокСтрах) Тогда
	//	Возврат Неопределено;
	//КонецЕсли;
	//Нети )Батрасова А. 25.05.2020
	
	Успешно = нвwДанныеФинПоЗапросамУПР.ОчиститьРегистрациюВыгруженных(Kod, МассивДок, МассивДокСтрах);	
	
	Возврат Успешно;
	
КонецФункции
