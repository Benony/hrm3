﻿
//Невада (Михайлова 05.02.19 наряд 0000028575
&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	ДополнительныеПараметры = Новый Структура("ФизическоеЛицо", ПараметрКоманды);
	ОписаниеОповещения = новый ОписаниеОповещения("ПеревыпускБейджикаЗавершение", ЭтотОбъект, ДополнительныеПараметры);
	ТекстВопроса = НСтр("ru = 'Будет произведено аннулирование текущего бейджика сотрудника %1 и выпуск нового. Продолжить?'");
	ТекстВопроса = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстВопроса, ПараметрКоманды);
	ПоказатьВопрос(ОписаниеОповещения, ТекстВопроса, РежимДиалогаВопрос.ОКОтмена);
КонецПроцедуры

&НаКлиенте
Процедура ПеревыпускБейджикаЗавершение(Ответ, ДополнительныеПараметры)
	Если Ответ <> КодВозвратаДиалога.ОК Тогда
		Возврат;
	КонецЕсли;
	ПеревыпускБейджика(ДополнительныеПараметры.ФизическоеЛицо);
	Оповестить("ПеревыпускБейджика");
КонецПроцедуры // 

&НаСервере
Процедура ПеревыпускБейджика(ФизическоеЛицо)
	ПериодАннулирования = ТекущаяДата();
	Запрос = новый Запрос;
	Запрос.УстановитьПараметр("ФизическоеЛицо", ФизическоеЛицо);
	Запрос.Текст =
	"ВЫБРАТЬ
	|	Бейджики.Ссылка КАК Ссылка,
	|	Бейджики.Аннулирован КАК Аннулирован,
	|	Бейджики.Владелец КАК Владелец
	|ИЗ
	|	Справочник.нвБейджики КАК Бейджики
	|ГДЕ
	|	Бейджики.Владелец = &ФизическоеЛицо
	|	И Бейджики.Аннулирован = ДАТАВРЕМЯ(1, 1, 1)";
	результат = Запрос.Выполнить();
	списокБейджиков = результат.Выгрузить().ВыгрузитьКолонку("Ссылка"); 
	Если списокБейджиков.Количество() = 0 Тогда
		возврат;
	КонецЕсли;
	Для каждого элементБейджик Из списокБейджиков Цикл
		Бейджик = элементБейджик.ПолучитьОбъект();
		Бейджик.Аннулирован = ПериодАннулирования;
		Бейджик.Записать();
	КонецЦикла;
	//возврат;
	Запрос.УстановитьПараметр("списокБейджиков", списокБейджиков);
	Запрос.УстановитьПараметр("ПериодАннулирования", ПериодАннулирования);
	Запрос.Текст =
	"ВЫБРАТЬ
	|	&ПериодАннулирования КАК Период,
	|	&ФизическоеЛицо КАК ФизическоеЛицо,
	|	нвБейджикиСрезПоследних.Бейджик КАК Бейджик,
	|	нвБейджикиСрезПоследних.Организация КАК Организация,
	|	нвБейджикиСрезПоследних.Подразделение КАК Подразделение,
	|	нвБейджикиСрезПоследних.Должность КАК Должность,
	|	нвБейджикиСрезПоследних.Приказ КАК Приказ,
	|	нвТипыДисконтныхКарт.Префикс КАК Префикс,
	|	нвТипыДисконтныхКарт.ТипШК КАК ТипШК
	|ИЗ
	|	РегистрСведений.нвБейджики.СрезПоследних(, Бейджик В (&списокБейджиков)) КАК нвБейджикиСрезПоследних
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.нвНастройкиПрограммы КАК нвНастройкиПрограммы
	|		ПО (ИСТИНА)
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.нвНастройкиУчетаБейджиков.НастройкиПоОрганизациям КАК нвНастройкиУчетаБейджиковНастройкиПоОрганизациям
	|			ЛЕВОЕ СОЕДИНЕНИЕ Справочник.нвТипыДисконтныхКарт КАК нвТипыДисконтныхКарт
	|			ПО нвНастройкиУчетаБейджиковНастройкиПоОрганизациям.ТипДисконтнойКарты = нвТипыДисконтныхКарт.Ссылка
	|		ПО (нвНастройкиПрограммы.нвНастройкиУчетаБейджиков = нвНастройкиУчетаБейджиковНастройкиПоОрганизациям.Ссылка)
	|			И нвБейджикиСрезПоследних.Организация = нвНастройкиУчетаБейджиковНастройкиПоОрганизациям.Организация";
		
	результат = Запрос.Выполнить();
	ТаблицаСреза = результат.Выгрузить();
	Если ТаблицаСреза.Количество() = 0 Тогда
		//был создан вручную, выберем основного сотрудника, организацию
		Запрос.Текст =
		"ВЫБРАТЬ
		|	&ПериодАннулирования КАК Период,
		|	РегистрСведений.ФизическоеЛицо КАК ФизическоеЛицо,
		|	РегистрСведений.Сотрудник КАК Сотрудник,
		|	РегистрСведений.ТекущаяОрганизация КАК Организация,
		|	РегистрСведений.ТекущаяДолжность КАК Должность,
		|	РегистрСведений.ТекущееПодразделение КАК Подразделение
		|ПОМЕСТИТЬ ВТКадровыеДанныеСотрудниковПолный
		|ИЗ
		|	РегистрСведений.ТекущиеКадровыеДанныеСотрудников КАК РегистрСведений
		|ГДЕ
		|	РегистрСведений.ФизическоеЛицо = &ФизическоеЛицо
		|	И РегистрСведений.ТекущийВидЗанятости <> ЗНАЧЕНИЕ(Перечисление.ВидыЗанятости.ВнутреннееСовместительство)
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ВТКадровыеДанныеСотрудниковПолный.Сотрудник КАК Сотрудник,
		|	ВТКадровыеДанныеСотрудниковПолный.ФизическоеЛицо КАК ФизическоеЛицо,
		|	ВТКадровыеДанныеСотрудниковПолный.Организация КАК Организация,
		|	ВТКадровыеДанныеСотрудниковПолный.Должность КАК Должность,
		|	ВТКадровыеДанныеСотрудниковПолный.Подразделение КАК Подразделение,
		|	нвТипыДисконтныхКарт.Префикс КАК Префикс,
		|	нвТипыДисконтныхКарт.ТипШК КАК ТипШК
		|ИЗ
		|	ВТКадровыеДанныеСотрудниковПолный КАК ВТКадровыеДанныеСотрудниковПолный
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.нвНастройкиПрограммы КАК нвНастройкиПрограммы
		|		ПО (ИСТИНА)
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.нвНастройкиУчетаБейджиков.НастройкиПоОрганизациям КАК нвНастройкиУчетаБейджиковНастройкиПоОрганизациям
		|			ЛЕВОЕ СОЕДИНЕНИЕ Справочник.нвТипыДисконтныхКарт КАК нвТипыДисконтныхКарт
		|			ПО нвНастройкиУчетаБейджиковНастройкиПоОрганизациям.ТипДисконтнойКарты = нвТипыДисконтныхКарт.Ссылка
		|		ПО (нвНастройкиПрограммы.нвНастройкиУчетаБейджиков = нвНастройкиУчетаБейджиковНастройкиПоОрганизациям.Ссылка)
		|			И ВТКадровыеДанныеСотрудниковПолный.Организация = нвНастройкиУчетаБейджиковНастройкиПоОрганизациям.Организация";
	    результат = Запрос.Выполнить();
		ТаблицаСрезаСотрудников = результат.Выгрузить();
		Если ТаблицаСрезаСотрудников.Количество() = 0 Тогда
			Текст = Нстр("ru = 'Не удается определить организацию.  Возможно сотрудник не принят на работу'");
		    ОбщегоНазначенияКлиентСервер.СообщитьПользователю(Текст, , , ,);
			возврат;
		КонецЕсли;
		ДанныеДляЗаполнения = ТаблицаСрезаСотрудников[0];
	иначе
		ДанныеДляЗаполнения = ТаблицаСреза[0];
	КонецЕсли;
	нвуРегистрацияБейджиков.СоздатьНовыйБейджик(ДанныеДляЗаполнения);
	////удаление истории
	//НаборЗаписей = регистрыСведений.нвБейджики.СоздатьНаборЗаписей();
	//Для каждого элементБейджик Из списокБейджиков Цикл
	//	НаборЗаписей.Отбор.Бейджик.Установить(элементБейджик);
	//	НаборЗаписей.Записать();
	//КонецЦикла;

КонецПроцедуры //
//Невада )Михайлова 05.02.19 наряд 0000028575


