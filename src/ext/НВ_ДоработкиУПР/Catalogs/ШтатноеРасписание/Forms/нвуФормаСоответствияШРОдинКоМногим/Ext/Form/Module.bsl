﻿
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	СтрЗаголовок = "";
	
	Если ЗначениеЗаполнено(Параметры.ПодразделениеУПР) Тогда
		ПодразделениеУПР = Параметры.ПодразделениеУПР;
		ОрганизацияУПР = ?(ЗначениеЗаполнено(Параметры.ОрганизацияУПР), Параметры.ОрганизацияУПР, Параметры.ПодразделениеУПР.Владелец);
		Должность = Параметры.Должность;
		СтрЗаголовок = "" + ОрганизацияУПР + ", " + ПодразделениеУПР + Символы.ПС;
		
		Для Каждого Стр Из Параметры.СоответствияПозицийШР Цикл 
			НоваяСтр = СоответствияПозицийШР.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяСтр, Стр);
		КонецЦикла;
	КонецЕсли;
	
	Элементы.НадписьПодразделение.Заголовок = СтрЗаголовок + Символы.ПС;
	ЗаполнитьСписокВыбораОргФИНСервер();
	
КонецПроцедуры

&НаКлиенте
Процедура СоответствияПозицийШРПослеУдаления(Элемент)
	
	Отказ = Истина;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьСтавки(Команда)
	
	ЗаполнитьОбщая("КоличествоСтавок");
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьГрафик(Команда)
	
	ЗаполнитьОбщая("ГрафикРаботы");
	
КонецПроцедуры

&НаКлиенте
Процедура ОК(Команда)
	
	//каждая организация ФИН + поджразделение ФИН могут быть использованы только в одной строке таблицы
	Если ЕстьДублиОрганизацийПодразделенийФИН() Тогда
		Сообщение = Новый СообщениеПользователю;
		Сообщение.Текст = "В строках таблицы есть дублирующиеся значения сочетания организация ФИН + подразделение ФИН! Данные не могут быть сохранены.";
		Сообщение.Сообщить();
		
		Возврат;
	КонецЕсли;
	
	Закрыть(СоответствияПозицийШР);
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьОбщая(ВариантЗаполнения)
	
	ОткрытьФорму("Справочник.ШтатноеРасписание.Форма.нвуФормаЗаполненияПозиции", Новый Структура("ВариантЗаполнения", ВариантЗаполнения),,,,, 
		Новый ОписаниеОповещения("нвЗаполнитьНажатиеЗавершение", ЭтотОбъект));
	
КонецПроцедуры
	
&НаКлиенте
Процедура нвЗаполнитьНажатиеЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	Если Результат = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ВыбЗначение = Неопределено;
	Если ТипЗнч(Результат) = Тип("Структура") И Результат.Свойство("ВыбЗначение", ВыбЗначение) = Истина Тогда
		Если ЗначениеЗаполнено(ВыбЗначение) Тогда
			ЗаполнитьСтрокиТЗ(Результат.ВариантЗаполнения, ВыбЗначение, Результат.ВариантВсеВыделенные);	
		КонецЕсли;	
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьСтрокиТЗ(ВариантЗаполнения, ВыбЗначение, ВариантВсеВыделенные)
	
	ТЗ = РеквизитФормыВЗначение("СоответствияПозицийШР");
	
	Если ВариантВсеВыделенные = 0 Тогда
		ТЗ.ЗаполнитьЗначения(ВыбЗначение, ВариантЗаполнения);
	Иначе
		Для Каждого ИдСтр Из Элементы.СоответствияПозицийШР.ВыделенныеСтроки Цикл
			ВыдСтрока = СоответствияПозицийШР.НайтиПоИдентификатору(ИдСтр); 
			Если ВыдСтрока <> Неопределено Тогда
				СтрокиТЗ = ТЗ.НайтиСтроки(Новый Структура("ОрганизацияФИН, ПодразделениеФИН", ВыдСтрока.ОрганизацияФИН, ВыдСтрока.ПодразделениеФИН)); 
				Если СтрокиТЗ.Количество() > 0 Тогда
					СтрокиТЗ[0][ВариантЗаполнения] = ВыбЗначение;		
				КонецЕсли
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	
	ЗначениеВРеквизитФормы(ТЗ, "СоответствияПозицийШР");
	
КонецПроцедуры
	
&НаСервереБезКонтекста
Функция ПолучитьОрганизацииФИН(Организация)
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ РАЗРЕШЕННЫЕ РАЗЛИЧНЫЕ
	|	СоответствиеОрганизаций.ОрганизацияФин КАК ОрганизацияФИН,
	|	СоответствиеОрганизаций.ОрганизацияФин.Наименование КАК ОрганизацияФинНаименование,
	|	СоответствиеОрганизаций.ОрганизацияУпр КАК ОрганизацияУпр
	|ПОМЕСТИТЬ ВТ_ОрганизацииФИН
	|ИЗ
	|	РегистрСведений.нвуСоответствиеОрганизаций КАК СоответствиеОрганизаций
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ВТ_ОрганизацииФИН.ОрганизацияФИН КАК ОрганизацияФИН,
	|	ВТ_ОрганизацииФИН.ОрганизацияФинНаименование КАК ОрганизацияФинНаименование
	|ИЗ
	|	ВТ_ОрганизацииФИН КАК ВТ_ОрганизацииФИН
	|ГДЕ
	|	НЕ ВТ_ОрганизацииФИН.ОрганизацияФИН В
	|				(ВЫБРАТЬ
	|					ВТ_ОрганизацииФИН.ОрганизацияФИН КАК ОрганизацияФИН
	|				ИЗ
	|					ВТ_ОрганизацииФИН КАК ВТ_ОрганизацииФИН
	|				ГДЕ
	|					ВТ_ОрганизацииФИН.ОрганизацияУпр = &Организация)
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ВТ_ОрганизацииФИН.ОрганизацияФИН,
	|	ВТ_ОрганизацииФИН.ОрганизацияФинНаименование
	|ИЗ
	|	ВТ_ОрганизацииФИН КАК ВТ_ОрганизацииФИН
	|ГДЕ
	|	ВТ_ОрганизацииФИН.ОрганизацияУпр = &Организация
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	Организации.Ссылка,
	|	Организации.Наименование
	|ИЗ
	|	Справочник.Организации КАК Организации
	|ГДЕ
	|	НЕ Организации.Ссылка В
	|				(ВЫБРАТЬ
	|					ВТ_ОрганизацииФИН.ОрганизацияФИН КАК ОрганизацияФИН
	|				ИЗ
	|					ВТ_ОрганизацииФИН КАК ВТ_ОрганизацииФИН)
	|	И НЕ Организации.Ссылка В
	|				(ВЫБРАТЬ
	|					ВТ_ОрганизацииФИН.ОрганизацияУпр КАК ОрганизацияУПР
	|				ИЗ
	|					ВТ_ОрганизацииФИН КАК ВТ_ОрганизацииФИН)
	|	И Организации.Ссылка <> &Организация
	|
	|УПОРЯДОЧИТЬ ПО
	|	ОрганизацияФинНаименование";
	Запрос.УстановитьПараметр("Организация", Организация);
	
	ОрганизацииФИН = Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("ОрганизацияФИН");
	
	Возврат ОрганизацииФИН;
	
КонецФункции

&НаСервере
Процедура ЗаполнитьСписокВыбораОргФИНСервер()
	
	СписокВыбораФИН = ПолучитьОрганизацииФИН(ОрганизацияУПР);
   	Элементы.СоответствияПозицийШРОрганизацияФИН.СписокВыбора.Очистить();
	
    Для каждого Орг Из СписокВыбораФИН Цикл
        Элементы.СоответствияПозицийШРОрганизацияФИН.СписокВыбора.Добавить(Орг);  
    КонецЦикла; 
    
КонецПроцедуры

&НаСервереБезКонтекста
Функция СоответствияПозицийШРОрганизацияФИНПриИзмененииНаСервере(ПодразделениеУПР, ОрганизацияФИН)
	
	ПодразделениеФин = Справочники.ПодразделенияОрганизаций.ПустаяСсылка();
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ РАЗРЕШЕННЫЕ РАЗЛИЧНЫЕ
	|	ЕСТЬNULL(нвуСоответствиеПодразделений.ПодразделениеФин, ЗНАЧЕНИЕ(Справочник.ПодразделенияОрганизаций.ПустаяСсылка)) КАК ПодразделениеФин
	|ИЗ
	|	РегистрСведений.нвуСоответствиеПодразделений КАК нвуСоответствиеПодразделений
	|ГДЕ
	|	нвуСоответствиеПодразделений.ПодразделениеФин.Владелец = &ОрганизацияФин
	|	И нвуСоответствиеПодразделений.ПодразделениеУпр = &ПодразделениеУПР";
	
	Запрос.УстановитьПараметр("ОрганизацияФИН", ОрганизацияФИН);
	Запрос.УстановитьПараметр("ПодразделениеУПР", ПодразделениеУПР);
	
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() Тогда
		ПодразделениеФин = Выборка.ПодразделениеФин;	
	КонецЕсли;
	
	Возврат ПодразделениеФин;
	
КонецФункции

&НаСервере
Функция ПроверитьОргФИНПодрФИНУжеЕсть(ОрганизацияФИН, ПодразделениеФИН, ТекИндекс)
	
	ИДСтроки = Неопределено;
	
	СтрокиПоОрг = СоответствияПозицийШР.НайтиСтроки(Новый Структура("ОрганизацияФИН, ПодразделениеФИН", ОрганизацияФИН, ПодразделениеФИН));
	Если СтрокиПоОрг.Количество() > 0 Тогда
		Для Каждого Стр Из СтрокиПоОрг Цикл
			Если ТекИндекс <> СоответствияПозицийШР.Индекс(Стр) Тогда
				ИДСтроки = СоответствияПозицийШР.Индекс(СтрокиПоОрг[0]);
				Прервать;
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	
	Возврат ИДСтроки;
	
КонецФункции

&НаКлиенте
Процедура СоответствияПозицийШРОрганизацияФИНОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	СоответствияПозицийШРОрганизацияФИНОбщая("ОрганизацияФИН", ВыбранноеЗначение, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура СоответствияПозицийШРОрганизацияФИНОбщая(ИмяПоля, ЗначениеПоля, СтандартнаяОбработка)
	
	//каждая организация ФИН+подразделение ФИН могут быть использованы только в одной строке таблицы
	ИДСтроки = ПроверитьОргФИНПодрФИНУжеЕсть(?(ИмяПоля = "ПодразделениеФИН", Элементы.СоответствияПозицийШР.ТекущиеДанные.ОрганизацияФИН, ЗначениеПоля), 
		?(ИмяПоля = "ОрганизацияФИН", Элементы.СоответствияПозицийШР.ТекущиеДанные.ПодразделениеФИН, ЗначениеПоля), 
		СоответствияПозицийШР.Индекс(Элементы.СоответствияПозицийШР.ТекущиеДанные));
	Если ИДСтроки <> Неопределено Тогда
		СтандартнаяОбработка = Ложь;
		
		Сообщение = Новый СообщениеПользователю;
		Сообщение.Текст = "Для сочетания организация ФИН " + СоответствияПозицийШР[ИДСтроки].ОрганизацияФИН + " подразделение ФИН " 
			+ СоответствияПозицийШР[ИДСтроки].ПодразделениеФИН +  " уже заполнены настройки!";
		Сообщение.Поле = "СоответствияПозицийШР[" + ИДСтроки + "]." + ИмяПоля;
		Сообщение.УстановитьДанные(Элементы["СоответствияПозицийШР" + ИмяПоля]);
		Сообщение.Сообщить();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура СоответствияПозицийШРПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа, Параметр)
	
	Если Копирование Тогда
		Отказ = Истина;	
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Функция ЕстьДублиОрганизацийПодразделенийФИН()
	
	ТаблДляПроверки = СоответствияПозицийШР.Выгрузить().Скопировать();
	ТаблДляПроверки.Свернуть("ОрганизацияФИН, ПодразделениеФИН");
	
	Если СоответствияПозицийШР.Количество() > ТаблДляПроверки.Количество() Тогда
		Возврат Истина;	
	КонецЕсли;
	
	Возврат Ложь;
	
КонецФункции


&НаКлиенте
Процедура СоответствияПозицийШРПодразделениеФИНОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	СоответствияПозицийШРОрганизацияФИНОбщая("ПодразделениеФИН", ВыбранноеЗначение, СтандартнаяОбработка);
	
КонецПроцедуры

