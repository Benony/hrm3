﻿//Невада (Фомичева И  25.05.2018 24960
&После("УстановитьПредставлениеФОТ")
Процедура нвуУстановитьПредставлениеФОТ(Форма)
	
	ФорматнаяСтрока = "ЧДЦ=2";
	Форма.Элементы.ГруппаСуммаНаРуки.Видимость = истина;
	
	ФОТМин				  = Форма.Объект.ФОТМин;
	ФОТМакс 			  = Форма.Объект.ФОТМакс;
	ФОТУправленческийМин  = Форма.Объект.ФОТУправленческийМин;
	ФОТУправленческийМакс = Форма.Объект.ФОТУправленческийМакс;
	
	Если Форма.ИспользоватьВилкуСтавок Тогда
				
		Если ФОТМин < ФОТМакс и ФОТУправленческийМин < ФОТУправленческийМакс Тогда
			НДФЛ_Мин  = ФОТМин * 0.13;
			НДФЛ_Макс = ФОТМакс * 0.13;
			
			ФОТУпрЗаВычетомНДФЛ_Мин  = Формат(ФОТУправленческийМин - НДФЛ_Мин,ФорматнаяСтрока);
			ФОТУпрЗаВычетомНДФЛ_Макс = Формат(ФОТУправленческийМакс - НДФЛ_Макс,ФорматнаяСтрока);
			
			//ФОТУправленческий    = Формат(ФОТУправленческийМин,ФорматнаяСтрока) + " - " + Формат(ФОТУправленческийМакс,ФорматнаяСтрока); 
			ФОТУпрЗаВычетомНДФЛ  = ФОТУпрЗаВычетомНДФЛ_Мин + " - " + ФОТУпрЗаВычетомНДФЛ_Макс;
			
		ИначеЕсли (ФОТМин > 0 И ФОТМин = ФОТМакс) и (ФОТУправленческийМин > 0 И ФОТУправленческийМин = ФОТУправленческийМакс) Тогда
			НДФЛ_Мин  = ФОТМин * 0.13;
			//ФОТУправленческий     = Формат(ФОТУправленческийМин, ФорматнаяСтрока); 
			ФОТУпрЗаВычетомНДФЛ   =  Формат(ФОТУправленческийМин - НДФЛ_Мин,ФорматнаяСтрока);

		ИначеЕсли ФОТМин = 0 тогда
			ФОТУправленческий    = Формат(ФОТУправленческийМин,ФорматнаяСтрока) + " - " + Формат(ФОТУправленческийМакс,ФорматнаяСтрока); 
		КонецЕсли;
		
	Иначе
		НДФЛ = Форма.Объект.ФОТ * 0.13;
		//ФОТУправленческий   = Формат(Форма.Объект.ФОТУправленческий, ФорматнаяСтрока);
		ФОТУпрЗаВычетомНДФЛ = Формат(Форма.Объект.ФОТУправленческий - НДФЛ,ФорматнаяСтрока);
	КонецЕсли;
	
	
	Если Не Форма.НастройкиРасчетаУправленческойЗарплатыПозицийШтатногоРасписания.ДоначислятьДоУправленческогоУчета Тогда
		
		//Если Не ПустаяСтрока(Форма.ФОТ) Тогда
			Форма.СуммаНаРуки = ФОТУпрЗаВычетомНДФЛ;
		//Иначе
		//	Форма.СуммаНаРуки = ФОТУправленческий;
		//КонецЕсли;
		
	Иначе 
		Форма.Элементы.ГруппаСуммаНаРуки.Видимость = Ложь;	
	КонецЕсли;

КонецПроцедуры
//Невада )Фомичева И 
