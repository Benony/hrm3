﻿Функция нвуВидыСтажаВЛисткеНетрудоспособностиУпр() Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	нвНастройкиПрограммы.ВидСтажаДляБЛУпр	
	|ИЗ
	|	РегистрСведений.нвНастройкиПрограммы КАК нвНастройкиПрограммы";
	Результат = Запрос.Выполнить();
	ВидыСтажа = Новый Массив;
	НеобязательныеВидыСтажа = Новый Соответствие;
	Если Не Результат.Пустой() Тогда
		Выборка = Результат.Выбрать();
		Пока Выборка.Следующий() Цикл
			ВидыСтажа.Добавить(Выборка.ВидСтажаДляБЛУпр);
		КонецЦикла;
	КонецЕсли;
	
	Возврат Новый Структура("ВидыСтажа, НеобязательныеВидыСтажа", ВидыСтажа, НеобязательныеВидыСтажа);
КонецФункции