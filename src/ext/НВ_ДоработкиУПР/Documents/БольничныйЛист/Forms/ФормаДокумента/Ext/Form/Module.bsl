﻿
&НаКлиенте
Процедура нвунвуРассчитыватьПоПравиламУПРПриИзмененииПосле(Элемент)
	
	нвуПроставитьФлажкиДляУпр();
	нвуУстановитьВидимостьУпр();
	ЗаполнитьСтаж();
	ОпределитьРазмерыПособияПоСтажу(ЭтотОбъект); //Нети (Батрасова А. 09.01.2019, задача внедрения 000027054
	нвуЗаполнитьПериодРасчетаСреднегоЗаработка(Истина);
	ВыполнитьПерезаполнениеИРасчетНачислений();
	
КонецПроцедуры

&НаКлиенте
Процедура нвуУстановитьВидимостьУпр()
	
	Элементы.нвуРазмерПособияФСС.Видимость = Объект.нвуРассчитыватьПоПравиламУПР;
	
КонецПроцедуры

&НаКлиенте
Процедура нвуПриОткрытииПосле(Отказ)
	
	нвуУстановитьВидимостьУпр();
	
КонецПроцедуры

&НаКлиенте
Процедура нвунвуРазмерПособияФССПриИзмененииПосле(Элемент)
	
	ВыполнитьПерезаполнениеИРасчетНачислений();
	
КонецПроцедуры

&НаКлиенте
Процедура нвуОткрытьСреднийЗаработокВместо(Команда)
	
	Оповещение = Новый ОписаниеОповещения("ОткрытьСреднийЗаработокЗавершение", ЭтотОбъект);
	
	Если Объект.нвуРассчитыватьПоПравиламУПР Тогда
		//УчетСреднегоЗаработкаКлиент.ОткрытьФормуВводаСреднегоЗаработкаОбщий(нвуПараметрыРедактированияСреднегоЗаработкаУпр(), ЭтотОбъект, Оповещение);	
		ПараметрыРедактирования = нвуПараметрыРедактированияСреднегоЗаработкаУпр();
		Если Не ПараметрыЗаполненыКорректно(ПараметрыРедактирования) Тогда
			Если Оповещение <> Неопределено Тогда 
				ВыполнитьОбработкуОповещения(Оповещение, Неопределено);
			КонецЕсли;	
		КонецЕсли;	
		
		ОткрытьФорму("ОбщаяФорма.нвуВводДанныхДляРасчетаСреднегоЗаработкаУпр", ПараметрыРедактирования, ЭтотОбъект, , , , Оповещение, РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
		
	Иначе
		УчетСреднегоЗаработкаКлиент.ОткрытьФормуВводаСреднегоЗаработкаФСС(ПараметрыРедактированияСреднегоЗаработка(), ЭтотОбъект, Оповещение);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Функция нвуПараметрыРедактированияСреднегоЗаработкаУпр()
	
	ПараметрыРедактирования = УчетСреднегоЗаработкаКлиентСервер.ПараметрыРедактированияОбщегоСреднегоЗаработкаПоДокументу();
	ПараметрыРедактирования.ДокументСсылка = Объект.Ссылка;
	ПараметрыРедактирования.Сотрудник = Объект.Сотрудник;
	ПараметрыРедактирования.Организация = Объект.Организация;
	ПараметрыРедактирования.ДатаНачалаСобытия = Объект.ДатаНачалаСобытия;
	ПараметрыРедактирования.НачалоПериодаРасчета = Объект.ПериодРасчетаСреднегоЗаработкаНачало;
	ПараметрыРедактирования.ОкончаниеПериодаРасчета = Объект.ПериодРасчетаСреднегоЗаработкаОкончание; 
	ПараметрыРедактирования.ЭтоСреднечасовойЗаработок = Ложь;
	ПараметрыРедактирования.ФиксПериодРасчета = Истина;
	ПараметрыРедактирования.Вставить("нвуРассчитыватьПоПравиламУПР", Объект.нвуРассчитыватьПоПравиламУПР);
	
	нвуУчетСреднегоЗаработкаУпр.нвуЗаполнитьТаблицыДанныхСреднегоЗаработкаПоДокументу(Объект, ПараметрыРедактирования);
	
	Возврат ПараметрыРедактирования;
	
КонецФункции

&НаСервере
&Вместо("ПерезаполнитьНачисления")
Процедура нвуПерезаполнитьНачисления(ВыводитьСообщения)
	
	Если Не ИспользуетсяРасчетЗарплаты Тогда
		Возврат;
	КонецЕсли;
	
	ОчиститьРассчитанныеДанные(Истина);
	
	ЗаполнитьВидыРасчетаВДокументе();
	
	ТекущийОбъект = ЭтотОбъект.РеквизитФормыВЗначение("Объект");
	Если Не ТекущийОбъект.ДокументГотовКРасчету(ВыводитьСообщения) Тогда
		ОбработатьСообщенияПользователю();
		УстановитьОтметкуНезаполненногоПланируемойДатыВыплаты(ЭтотОбъект);
		ПерезаполнятьСреднийЗаработок = Истина;
		Возврат;
	КонецЕсли;
	
	ПериодРасчетаЗарплаты = ПериодРасчетаЗарплатыДоНачалаОтсутствия();
	
	МенеджерРасчета = РасчетЗарплатыРасширенный.СоздатьМенеджерРасчета(Объект.ПериодРегистрации, Объект.Организация);
	ЗаполнитьНастройкиМенеджераРасчета(МенеджерРасчета, ПериодРасчетаЗарплаты);
	
	ЗаполнитьНачисления(МенеджерРасчета, ПериодРасчетаЗарплаты);
	ЗаполнитьПерерасчеты(МенеджерРасчета);
	ЗаполнитьУдержания(МенеджерРасчета);
	ЗаполнитьПараметрыРасчетаПособияПоНетрудоспособности(МенеджерРасчета);
	
	Если Объект.нвуРассчитыватьПоПравиламУПР Тогда
		// Заполняем соответствие известных показателей.
		ТаблицаНачислений = МенеджерРасчета.ТаблицаИсходныеДанныеНачисленияЗарплатыПоНачислениям();
		НоваяСтрока = ТаблицаНачислений.Добавить();
		НоваяСтрока.Сотрудник = Объект.Сотрудник;
		НоваяСтрока.Начисление = Объект.ВидОплатыЗаСчетРаботодателя;
		НоваяСтрока.ДатаНачала = Объект.ДатаНачала;
		НоваяСтрока.ДатаОкончания = Объект.ДатаОкончанияОплаты;
			
		ЗначениеПоказателя = Объект.СреднийДневнойЗаработок;
		Показатель = ОбщегоНазначенияКлиентСервер.ПредопределенныйЭлемент("Справочник.ПоказателиРасчетаЗарплаты.СреднийЗаработокОбщий");

		МенеджерРасчета.ДобавитьИзвестноеЗначениеПоказателя(НоваяСтрока, Показатель, ЗначениеПоказателя);
	КонецЕсли;
	
	МенеджерРасчета.РассчитатьЗарплату();
	
	РасчетЗарплатыВДанныеФормы(МенеджерРасчета.Зарплата);
	
КонецПроцедуры

&НаСервере
&Вместо("ЗаполнитьПериодРасчетаСреднегоЗаработка")
Процедура нвуЗаполнитьПериодРасчетаСреднегоЗаработка(ЗаполнитьБезусловно)
	
	Если Не ИспользуетсяРасчетЗарплаты Тогда
		Возврат;
	КонецЕсли;
	
	ПериодИзменился = Ложь;
	
	Если Не Объект.ФиксПериодРасчетаСреднегоЗаработка Тогда
		
		ПериодРасчетаСреднего = УчетПособийСоциальногоСтрахованияКлиентСервер.ПериодРасчетаСреднегоЗаработкаФСС(Объект.ДатаНачалаСобытия, ПорядокРасчетаСреднегоЗаработкаФСС());
		
		Если НачалоМесяца(Объект.ПериодРасчетаСреднегоЗаработкаНачало) <> НачалоМесяца(ПериодРасчетаСреднего.ДатаНачала)
			Или НачалоМесяца(Объект.ПериодРасчетаСреднегоЗаработкаОкончание) <> НачалоМесяца(ПериодРасчетаСреднего.ДатаОкончания) Тогда
			
			Объект.ПериодРасчетаСреднегоЗаработкаНачало	= ПериодРасчетаСреднего.ДатаНачала;
			Объект.ПериодРасчетаСреднегоЗаработкаОкончание = ПериодРасчетаСреднего.ДатаОкончания;
			
			// Период годами
			Объект.ПериодРасчетаСреднегоЗаработкаПервыйГод = Год(ПериодРасчетаСреднего.ДатаНачала);
			Объект.ПериодРасчетаСреднегоЗаработкаВторойГод = Год(ПериодРасчетаСреднего.ДатаОкончания);
			
			ПериодИзменился = Истина;
			
		КонецЕсли;
	Иначе
		Если Объект.нвуРассчитыватьПоПравиламУПР Тогда
			Объект.ПериодРасчетаСреднегоЗаработкаНачало = ДобавитьМесяц(НачалоМесяца(Объект.ДатаНачалаСобытия), - 6);
			Объект.ПериодРасчетаСреднегоЗаработкаОкончание = НачалоМесяца(Объект.ДатаНачалаСобытия) - 1;
			
			ПериодИзменился = Истина;
		КонецЕсли;
	КонецЕсли;
	
	Если ПериодИзменился
		Или ЗаполнитьБезусловно Тогда
		ОбновитьДанныеДляРасчетаСреднего();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
&Вместо("ПеренестиДанныеУчетаСреднегоЗаработкаВДокумент")
Процедура нвуПеренестиДанныеУчетаСреднегоЗаработкаВДокумент(РезультатРедактирования)
	
	// Переносит данные учета среднего заработка (результат работы формы "калькулятора")
	// в таблицы документа.
	
	Объект.СреднийЗаработокФСС.Очистить();
	Объект.ОтработанноеВремяДляСреднегоФСС.Очистить();
	Объект.СреднийЗаработокДанныеСтрахователей.Очистить();
	Объект.ПериодыБолезниУходаЗаДетьми.Очистить();
	Объект.СреднийЗаработокУпр.Очистить();
	Объект.ОтработанноеВремяДляСреднегоУпр.Очистить();
	
	Если Объект.нвуРассчитыватьПоПравиламУПР Тогда
		нвуУчетСреднегоЗаработкаУпр.нвуЗаполнитьДанныеУчетаОбщегоСреднегоЗаработка(
			Объект.СреднийЗаработокУпр, 
			Объект.ОтработанноеВремяДляСреднегоУпр, 
			РезультатРедактирования, 
			Модифицированность);
	Иначе
		УчетПособийСоциальногоСтрахованияРасширенный.ЗаполнитьДанныеУчетаСреднегоЗаработкаФСС(Объект, РезультатРедактирования, ЭтотОбъект);
	КонецЕсли;
	
	Объект.ФиксПериодРасчетаСреднегоЗаработка = РезультатРедактирования.ФиксПериодРасчета;
	Объект.ПериодРасчетаСреднегоЗаработкаНачало = РезультатРедактирования.НачалоПериодаРасчета;
	Объект.ПериодРасчетаСреднегоЗаработкаОкончание = РезультатРедактирования.ОкончаниеПериодаРасчета;
	Если НЕ Объект.нвуРассчитыватьПоПравиламУПР Тогда 
		Объект.ПериодРасчетаСреднегоЗаработкаПервыйГод = РезультатРедактирования.ГодыПериодаРасчета[0];
		Объект.ПериодРасчетаСреднегоЗаработкаВторойГод = РезультатРедактирования.ГодыПериодаРасчета[1];
	КонецЕсли;
	
	Объект.СреднийДневнойЗаработок = РезультатРедактирования.СреднийЗаработок;
	
КонецПроцедуры

&НаСервере
&Вместо("РассчитатьСреднийЗаработок")
Процедура нвуРассчитатьСреднийЗаработок()
		
	УстановитьПривилегированныйРежим(Истина);
	
	// Для расчета среднего дневного заработка заполняем структуру параметров.
	ПараметрыРасчета = ПараметрыРасчетаСреднегоДневногоЗаработкаФСС();
	
	Если Объект.нвуРассчитыватьПоПравиламУПР Тогда
		ДополнительныеПараметры = УчетСреднегоЗаработкаКлиентСервер.ДополнительныеПараметрыРасчетаСреднегоЗаработка();
		ДополнительныеПараметры.ДатаНачалаСобытия = Объект.ДатаНачалаСобытия;
		ДополнительныеПараметры.НачалоПериода = Объект.ПериодРасчетаСреднегоЗаработкаНачало;
		ДополнительныеПараметры.ОкончаниеПериода = Объект.ПериодРасчетаСреднегоЗаработкаОкончание;
		ДополнительныеПараметры.ПоЧасам = Ложь;
		ДополнительныеПараметры.Вставить("нвуРассчитыватьПоПравиламУПР", Истина);
	
		Объект.СреднийДневнойЗаработок = нвуУчетСреднегоУпрКлиентСервер.нвуСреднийЗаработокУпрБЛ(Объект.СреднийЗаработокУпр, Объект.ОтработанноеВремяДляСреднегоУпр, ДополнительныеПараметры);	
	Иначе		
		Объект.СреднийДневнойЗаработок = УчетПособийСоциальногоСтрахования.СреднийДневнойЗаработокФСС(ПараметрыРасчета);
	КонецЕсли;

	Объект.МинимальныйСреднедневнойЗаработок = УчетПособийСоциальногоСтрахования.МинимальныйСреднедневнойЗаработокФСС(ПараметрыРасчета);
	
	Если Объект.ДоплачиватьДоДенежногоСодержания Тогда
		
		Если ОбщегоНазначения.ПодсистемаСуществует("ЗарплатаКадрыПриложения.ГосударственнаяСлужба.РасчетДенежногоСодержания") Тогда
			Модуль = ОбщегоНазначения.ОбщийМодуль("РасчетДенежногоСодержания");
			Модуль.РассчитатьСохраняемоеДенежноеСодержаниеДокумента(ЭтотОбъект, ОписаниеДокумента(ЭтотОбъект));
		КонецЕсли;

	КонецЕсли;
	
	ЗаполнениеВыполнено = Истина;
	
КонецПроцедуры

&НаСервере
&Вместо("ДатыИзменений")
Функция нвуДатыИзменений()
	
	// Создаем таблицу точек на оси времени и начислений которые с ними связаны
	// затем строим из этих точек интервалы.
	ДатыИзменений = Новый ТаблицаЗначений;
	ДатыИзменений.Колонки.Добавить("Дата", Новый ОписаниеТипов("Дата"));
	ДатыИзменений.Колонки.Добавить("Начисление", Новый ОписаниеТипов("ПланВидовРасчетаСсылка.Начисления"));
	
	Если Не Объект.НазначитьПособие Тогда
		НоваяДата = ДатыИзменений.Добавить();
		НоваяДата.Дата = Объект.ДатаНачала;
		НоваяДата.Начисление = Объект.ВидНеоплачиваемогоВремени;
		Возврат ДатыИзменений;
	КонецЕсли;
	
	Если Объект.ДатаНачала < Объект.ДатаНачалаОплаты Тогда
		НоваяДата = ДатыИзменений.Добавить();
		НоваяДата.Дата = Объект.ДатаНачала;
		НоваяДата.Начисление = Объект.ВидНеоплачиваемогоВремени;
	КонецЕсли;
	
	ДатаНачалаОплатыЗаСчетФСС = Объект.ДатаНачалаСобытия + УчетПособийСоциальногоСтрахованияКлиентСервер.КоличествоДнейЗаСчетРаботодателя(Объект.ДатаНачалаСобытия) * УчетПособийСоциальногоСтрахованияКлиентСервер.ДлинаСуток();
	
	Если Объект.ПричинаНетрудоспособности = Перечисления.ПричиныНетрудоспособности.ОбщееЗаболевание Тогда
		
		Если ДатаНачалаОплатыЗаСчетФСС > Объект.ДатаНачалаОплаты Тогда
			НоваяДата = ДатыИзменений.Добавить();
			НоваяДата.Дата = Объект.ДатаНачалаОплаты;
			НоваяДата.Начисление = Объект.ВидОплатыЗаСчетРаботодателя;
			// Оплата за счет ФСС
			Если НЕ Объект.нвуРассчитыватьПоПравиламУПР Тогда
				Если ДатаНачалаОплатыЗаСчетФСС <= Объект.ДатаОкончанияОплаты Тогда
					НоваяДата = ДатыИзменений.Добавить();
					НоваяДата.Дата = ДатаНачалаОплатыЗаСчетФСС;
					НоваяДата.Начисление = Объект.ВидОплатыПособия;
				КонецЕсли;
			КонецЕсли;
		Иначе
			НоваяДата = ДатыИзменений.Добавить();
			НоваяДата.Дата = Объект.ДатаНачалаОплаты;
			НоваяДата.Начисление = Объект.ВидОплатыПособия;
		КонецЕсли;
	Иначе
		НоваяДата = ДатыИзменений.Добавить();
		НоваяДата.Дата = Объект.ДатаНачалаОплаты;
		НоваяДата.Начисление = Объект.ВидОплатыПособия;
		
		Если ЗначениеЗаполнено(Объект.ДатаОкончанияОплаты)
			И Объект.ДатаНачалаПоловиннойОплаты > Объект.ДатаНачалаОплаты
			И Объект.ДатаНачалаПоловиннойОплаты <= Объект.ДатаОкончанияОплаты Тогда
			НоваяДата = ДатыИзменений.Добавить();
			НоваяДата.Дата = Объект.ДатаНачалаПоловиннойОплаты;
			НоваяДата.Начисление = Объект.ВидОплатыПособия;
		КонецЕсли;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(Объект.ДатаНарушенияРежима)
		И Объект.ДатаНарушенияРежима > Объект.ДатаНачалаОплаты
		И Объект.ДатаНарушенияРежима <= Объект.ДатаОкончанияОплаты
		И ДатыИзменений.Найти(Объект.ДатаНарушенияРежима, "Дата") = Неопределено Тогда
		НоваяДата = ДатыИзменений.Добавить();
		НоваяДата.Дата = Объект.ДатаНарушенияРежима;
		Если Объект.ДатаНарушенияРежима >= ДатаНачалаОплатыЗаСчетФСС
			Или Не Объект.ПричинаНетрудоспособности = Перечисления.ПричиныНетрудоспособности.ОбщееЗаболевание Тогда
			НоваяДата.Начисление = Объект.ВидОплатыПособия;
		Иначе
			НоваяДата.Начисление = Объект.ВидОплатыЗаСчетРаботодателя;
		КонецЕсли;
	КонецЕсли;
	
	Если Объект.ДатаОкончанияОплаты < Объект.ДатаОкончания Тогда
		НоваяДата = ДатыИзменений.Добавить();
		НоваяДата.Дата = Объект.ДатаОкончанияОплаты + УчетПособийСоциальногоСтрахованияКлиентСервер.ДлинаСуток();
		НоваяДата.Начисление = Объект.ВидНеоплачиваемогоВремени;
	КонецЕсли;
	
	Возврат ДатыИзменений;
	
КонецФункции

&НаСервере
&Перед("ОбновитьДанныеДляРасчетаСреднего")
Процедура нвуОбновитьДанныеДляРасчетаСреднего()
	
	Если Не ИспользуетсяРасчетЗарплаты Тогда
		Возврат;
	КонецЕсли;

	Если Не ЗначениеЗаполнено(Объект.Сотрудник) Или Не ЗначениеЗаполнено(Объект.ДатаНачалаСобытия) Тогда
		Возврат;
	КонецЕсли;
	
	Если Объект.нвуРассчитыватьПоПравиламУПР Тогда
		УстановитьПривилегированныйРежим(Истина);
		
		нвуУчетСреднегоЗаработкаУпр.нвуОбновитьДанныеОбщегоСреднегоЗаработка(
				Новый Структура("ДанныеОНачислениях, ДанныеОВремени", 
				Объект.СреднийЗаработокУпр, Объект.ОтработанноеВремяДляСреднегоУпр), 
				Объект.ДатаНачалаСобытия, 
				Объект.ПериодРасчетаСреднегоЗаработкаНачало, 
				Объект.ПериодРасчетаСреднегоЗаработкаОкончание,
				ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(Объект.Сотрудник), , 
				Объект.Ссылка);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Функция ПараметрыЗаполненыКорректно(ПараметрыРедактирования)
	
	Если Не ЗначениеЗаполнено(ПараметрыРедактирования.Сотрудник) Тогда
		ПоказатьПредупреждение(, НСтр("ru = 'Сотрудник не указан.'"));
		Возврат Ложь;
	КонецЕсли;	
	
	Возврат Истина;
	
КонецФункции

&НаКлиенте
&Вместо("НадписьСтажНажатиеПродолжение")
Процедура нвуНадписьСтажНажатиеПродолжение(Ответ, ДополнительныеПараметры)
	
	Если НЕ Объект.нвуРассчитыватьПоПравиламУПР Тогда
		ПродолжитьВызов(Ответ, ДополнительныеПараметры);
		Возврат;
	КонецЕсли;
	
	Если Ответ <> КодВозвратаДиалога.Да Тогда
		Возврат;
	КонецЕсли;

	Отказ = Ложь;
	Если Не ЗначениеЗаполнено(Объект.Сотрудник) Тогда
		Текст = Нстр("ru = 'Не выбран сотрудник'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(Текст, , "Объект.Сотрудник", ,Отказ);
	КонецЕсли;
	Если Не ЗначениеЗаполнено(Объект.Сотрудник) Тогда
		Текст = Нстр("ru = 'Не заполнена дата начала освобождения от работы'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(Текст, , "Объект.ДатаНачала", ,Отказ);
	КонецЕсли;
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	ЗапрашиваемыеВидыСтажа = нвуВидыСтажаВЛисткеНетрудоспособностиУпр();	
	
	ВидыСтажа = Новый ФиксированныйМассив(ЗапрашиваемыеВидыСтажа.ВидыСтажа);
	НеобязательныеВидыСтажа = Новый ФиксированноеСоответствие(ЗапрашиваемыеВидыСтажа.НеобязательныеВидыСтажа);
	
	КадровыйУчетРасширенныйКлиент.ОткрытьФормуРедактированияСтажейСотрудника(ЭтотОбъект, Объект.Сотрудник, Объект.ДатаНачалаСобытия, ВидыСтажа, , , , НеобязательныеВидыСтажа);	
	
	//ПродолжитьВызов(Ответ, ДополнительныеПараметры);
КонецПроцедуры

&НаСервереБезКонтекста
Функция нвуВидыСтажаВЛисткеНетрудоспособностиУпр() Экспорт
	
	Возврат Документы.БольничныйЛист.нвуВидыСтажаВЛисткеНетрудоспособностиУпр();
	
КонецФункции


&НаСервере
&Вместо("ДанныеОСтажеСотрудника")
Функция нвуДанныеОСтажеСотрудника()
	
	Если НЕ Объект.нвуРассчитыватьПоПравиламУПР Тогда
		Результат = ПродолжитьВызов();
		Возврат Результат;
	КонецЕсли;
	
	УстановитьПривилегированныйРежим(Истина);
	
	// Поля стажа СтажЛет, СтажМесяцев автозаполняются по данным о стаже в Компании
	
	МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	
	// Заполняем исходные данные для получения стажа.
	Запрос.Текст =
	"ВЫБРАТЬ
	|	Сотрудники.Ссылка КАК Сотрудник,
	|	нвНастройкиПрограммы.ВидСтажаДляБЛУпр КАК ВидСтажа,
	|	&ДатаНачалаСобытия КАК Дата
	|ПОМЕСТИТЬ ВТИсходныеДанные
	|ИЗ
	|	Справочник.Сотрудники КАК Сотрудники
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.нвНастройкиПрограммы КАК нвНастройкиПрограммы
	|		ПО (Сотрудники.Ссылка = &Сотрудник)";
	
	Запрос.УстановитьПараметр("Сотрудник", Объект.Сотрудник);
	Запрос.УстановитьПараметр("ДатаНачалаСобытия", ?(ЗначениеЗаполнено(Объект.ДатаНачалаСобытия), Объект.ДатаНачалаСобытия, ТекущаяДатаСеанса()));
	Запрос.Выполнить();
	
	// Создаем таблицу стажей
	УстановитьПривилегированныйРежим(Истина);
	КадровыйУчетРасширенный.СоздатьВТСтажиСотрудников(МенеджерВременныхТаблиц, Ложь);
	УстановитьПривилегированныйРежим(Ложь);
	
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ЕСТЬNULL(СтажДляБЛ.РазмерМесяцев, 0) КАК СтажВМесяцахРасширенный,
	|	ЕСТЬNULL(СтажДляБЛ.РазмерМесяцев, 0) КАК СтажВМесяцах,
	|	ЛОЖЬ КАК ФинансированиеФедеральнымБюджетом,
	|	ВЫБОР
	|		КОГДА СтажДляБЛ.Сотрудник ЕСТЬ НЕ NULL
	|			ТОГДА ИСТИНА
	|		ИНАЧЕ ЛОЖЬ
	|	КОНЕЦ КАК ДляСотрудникаВведенСтраховойСтаж
	|ИЗ
	|	Справочник.Сотрудники КАК Сотрудники
	|		ЛЕВОЕ СОЕДИНЕНИЕ ВТСтажиСотрудников КАК СтажДляБЛ
	|		ПО Сотрудники.Ссылка = СтажДляБЛ.Сотрудник
	|ГДЕ
	|	Сотрудники.Ссылка = &Сотрудник";
	
	Запрос.УстановитьПараметр("Сотрудник", Объект.Сотрудник);
	ДанныеОСтажеСотрудника = Запрос.Выполнить().Выбрать();
	ДанныеОСтажеСотрудника.Следующий();
	Возврат ДанныеОСтажеСотрудника;
	
КонецФункции

&НаСервере
Процедура нвуПриСозданииНаСервереПосле(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Ключ.Пустая() Тогда
		//Объект.нвуРассчитыватьПоПравиламУПР = Истина;  //Невада (Михайлова 23.01.19 наряд 0000028781) 
		Объект.нвуРассчитыватьПоПравиламУПР = нвНастройкиПовтИсп.ЗначениеНастройкиПрограммы("РассчитыватьДокументыСреднегоПравиламиУпр", Ложь); //Невада (Михайлова 23.01.19 наряд 0000028781) 

		нвуПроставитьФлажкиДляУпр();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура нвуСотрудникПриИзмененииПосле(Элемент)
	
	нвуПроставитьФлажкиДляУпр();
	
КонецПроцедуры

&НаСервере
Процедура нвуПроставитьФлажкиДляУпр()
	
	Объект.УчитыватьЗаработокПредыдущихСтрахователей = НЕ Объект.нвуРассчитыватьПоПравиламУПР;
	Объект.ФиксПериодРасчетаСреднегоЗаработка = Объект.нвуРассчитыватьПоПравиламУПР;
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
&После("ОбновитьНачисленоУдержаноИтог")
Процедура нвуОбновитьНачисленоУдержаноИтог(Форма)
	
	Объект = Форма.Объект;
	
	Если Объект.нвуРассчитыватьПоПравиламУПР Тогда
		Форма.НачисленоФСС = Форма.НачисленоФСС + Объект.нвуРазмерПособияФСС;
		Форма.НачисленоИтог = Форма.НачисленоИтог + Объект.нвуРазмерПособияФСС;
	КонецЕсли;
	
КонецПроцедуры


