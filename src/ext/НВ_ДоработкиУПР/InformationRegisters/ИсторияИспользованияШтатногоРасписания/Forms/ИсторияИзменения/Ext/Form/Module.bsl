﻿
&НаКлиенте
Процедура нвуПриОткрытииПосле(Отказ)
	
	нвуУстановитьВидимостьТарифнойСетки(); // Невада Ващенко Е.В. Наряд №0000026207 31.07.2018 тарифная надбавка
	
КонецПроцедуры

// Невада Ващенко Е.В. Наряд №0000026207 31.07.2018 тарифная надбавка {
&НаКлиенте
Процедура нвуУстановитьВидимостьТарифнойСетки()
	
	ИспользоватьКвалификационнуюНадбавку = нвуПолучитьФункциональнуюОпциюНаСервереБезКонтекста("ИспользоватьКвалификационнуюНадбавку");
	
	Элементы.ИсторияИзмененияТарифнаяСеткаГруппа.Видимость = НЕ ИспользоватьКвалификационнуюНадбавку;
	Элементы.ИсторияИзмененияТарифнаяСеткаГруппа.Доступность = НЕ ИспользоватьКвалификационнуюНадбавку;
			
КонецПроцедуры
 
&НаСервереБезКонтекста
Функция нвуПолучитьФункциональнуюОпциюНаСервереБезКонтекста(ИмяФункциональнойОпции)
	
	Возврат ПолучитьФункциональнуюОпцию(ИмяФункциональнойОпции);
	
КонецФункции
// } Невада Ващенко Е.В. Наряд №0000026207 31.07.2018
