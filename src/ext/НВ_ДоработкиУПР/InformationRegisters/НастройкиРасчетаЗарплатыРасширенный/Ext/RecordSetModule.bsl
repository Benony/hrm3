﻿
// Невада Ващенко Е.В. Наряд №0000026207 31.07.2018 тарифная надбавка {
&После("ПередЗаписью")
Процедура нвуПередЗаписью(Отказ, Замещение)
	
	Если ЗарплатаКадры.ОтключитьБизнесЛогикуПриЗаписи(ЭтотОбъект) Тогда
		Возврат;
	КонецЕсли;
	
	Если ЭтотОбъект.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	ИспользоватьТарифныеСетки = ЭтотОбъект[0].ИспользоватьТарифныеСеткиПриРасчетеЗарплаты;
	ИспользоватьКвалификационнуюНадбавку = ЭтотОбъект[0].ИспользоватьКвалификационнуюНадбавку;
	
	Если ИспользоватьТарифныеСетки И ИспользоватьКвалификационнуюНадбавку Тогда
		ЭтотОбъект[0].ИспользоватьТарифныеСеткиПриРасчетеЗарплатыБюджет = Ложь;
		ЭтотОбъект[0].ИспользоватьТарифныеСеткиПриРасчетеЗарплатыХозрасчет = Ложь;
	КонецЕсли; 
	
КонецПроцедуры
// } Невада Ващенко Е.В. Наряд №0000026207 31.07.2018