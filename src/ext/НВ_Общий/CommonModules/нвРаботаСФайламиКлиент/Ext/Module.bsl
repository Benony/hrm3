﻿//Открытие файла/каталога
//РеквизитФайл - реквизит, в котором хранится путь к файлу
//Заголовок - заголовок окна открытия диалога работы с файлом
//РежимВыбораФайла - РежимДиалогаВыбораФайла
//Расширение - строка с доступными расширениями файла по умолчанию
//Фильтр - строка с набором файловых фильтров
Процедура ФайлНачалоВыбора(РеквизитФайл, Заголовок = "", РежимВыбораФайла = Неопределено, Расширение = "", Фильтр = "", 
	ОписаниеОповещения = Неопределено, МножественныйВыбор = Ложь, ИндексФильтра = 0) Экспорт
	
	Если РежимВыбораФайла = Неопределено Тогда
		РежимВыбораФайла = РежимДиалогаВыбораФайла.ВыборКаталога;
	КонецЕсли;
	
	ДиалогВыбораФайла = Новый ДиалогВыбораФайла(РежимВыбораФайла);
	ДиалогВыбораФайла.Каталог = РеквизитФайл;
	ДиалогВыбораФайла.МножественныйВыбор = МножественныйВыбор;
	Если Расширение <> "" Тогда
		ДиалогВыбораФайла.Расширение = Расширение;
	КонецЕсли;
	Если Фильтр <> "" Тогда
		ДиалогВыбораФайла.Фильтр = Фильтр;
	КонецЕсли;
	Если Заголовок <> "" Тогда
		ДиалогВыбораФайла.Заголовок = Заголовок;
	Иначе
		ДиалогВыбораФайла.Заголовок = "Выберите " + ?(РежимВыбораФайла = РежимДиалогаВыбораФайла.ВыборКаталога, "каталог", "файл");
	КонецЕсли;
	ДиалогВыбораФайла.ИндексФильтра = ИндексФильтра;
	
	Если ОписаниеОповещения <> Неопределено Тогда 
		ДиалогВыбораФайла.Показать(ОписаниеОповещения);
	ИначеЕсли ДиалогВыбораФайла.Выбрать() Тогда
		//Нети (Батрасова А. 06.03.2019, наряд 0000029629
		//РеквизитФайл = ?(РежимВыбораФайла = РежимДиалогаВыбораФайла.Сохранение, ДиалогВыбораФайла.ПолноеИмяФайла, ДиалогВыбораФайла.Каталог);
		РеквизитФайл = ?(РежимВыбораФайла = РежимДиалогаВыбораФайла.Сохранение ИЛИ РежимВыбораФайла = РежимДиалогаВыбораФайла.Открытие, ДиалогВыбораФайла.ПолноеИмяФайла, ДиалогВыбораФайла.Каталог);
		//Нети )Батрасова А. 06.03.2019		
	КонецЕсли;
	
КонецПроцедуры
