﻿
&НаСервере
Процедура ПосчитатьТекущийСтажНаСервере()
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	НакопленныеСтажиФизическихЛиц.ФизическоеЛицо КАК ФизическоеЛицо,
	               |	НакопленныеСтажиФизическихЛиц.ДатаНачала КАК ДатаНачала,
				   |	НакопленныеСтажиФизическихЛиц.ДатаОкончания КАК ДатаОкончания,
	               |	НакопленныеСтажиФизическихЛиц.РазмерМесяцев КАК РазмерМесяцев,
	               |	НакопленныеСтажиФизическихЛиц.РазмерДней КАК РазмерДней,
	               |	НакопленныеСтажиФизическихЛиц.Прерван КАК Прерван,
	               |	0 КАК ЛетСтажа,
	               |	0 КАК МесяцевСтажа,
	               |	0 КАК ДнейСтажа
	               |ИЗ
	               |	РегистрСведений.НакопленныеСтажиФизическихЛиц КАК НакопленныеСтажиФизическихЛиц
	               |ГДЕ
	               |	НакопленныеСтажиФизическихЛиц.ФизическоеЛицо В
	               |			(ВЫБРАТЬ
	               |				НакопленныеСтажиФизическихЛиц.ФизическоеЛицо КАК ФизическоеЛицо
	               |			ИЗ
	               |				РегистрСведений.НакопленныеСтажиФизическихЛиц КАК НакопленныеСтажиФизическихЛиц
	               |			ГДЕ
	               |				НакопленныеСтажиФизическихЛиц.ВидСтажа = &ВидСтажа
	               |			СГРУППИРОВАТЬ ПО
	               |				НакопленныеСтажиФизическихЛиц.ФизическоеЛицо
	               |			ИМЕЮЩИЕ
	               |				КОЛИЧЕСТВО(РАЗЛИЧНЫЕ НакопленныеСтажиФизическихЛиц.ДатаНачала) > 1)
	               |	И НакопленныеСтажиФизическихЛиц.ВидСтажа = &ВидСтажа";
	Запрос.УстановитьПараметр("ВидСтажа", Объект.ВидСтажа);
	ТЗ = Запрос.Выполнить().Выгрузить();

	//дополняем данными по размеру стажа на начало текущего дня
	Для Каждого Строка Из ТЗ Цикл
		
		ДанныеСтажа = ЗарплатаКадрыРасширенныйКлиентСервер.СведенияОСтаже();
		ДанныеСтажа.ДатаОтсчета = Строка.ДатаНачала;
		ДанныеСтажа.ИсчисляетсяСДатыПриема = Ложь;
		ДанныеСтажа.Лет = 0;
		ДанныеСтажа.Месяцев = Строка.РазмерМесяцев;
		ДанныеСтажа.Дней = Строка.РазмерДней;
		ДанныеСтажа.Прерван = Строка.Прерван;
		
		ПродолжительностьСтажа = КадровыйУчетРасширенныйВызовСервера.ПродолжительностьСтажа(ДанныеСтажа, НачалоДня(ТекущаяДата()));
		
		Строка.ЛетСтажа = ПродолжительностьСтажа.Лет;
		Строка.МесяцевСтажа = ПродолжительностьСтажа.Месяцев;
		Строка.ДнейСтажа = ПродолжительностьСтажа.Дней;
		
	КонецЦикла;
	
	//теперь нужно почистить ненужные дубли
	Для Каждого Строка Из ТЗ Цикл
		ПараметрыОтбора = Новый Структура;
		ПараметрыОтбора.Вставить("ФизическоеЛицо", Строка.ФизическоеЛицо);
		НайденныеСтроки = ТЗ.НайтиСтроки(ПараметрыОтбора);
		МаксСтажФизЛица = 0;
		Для Каждого СтрокаМассива Из НайденныеСтроки Цикл
			Стаж = СтрокаМассива.ЛетСтажа*1000 + СтрокаМассива.МесяцевСтажа*10 + СтрокаМассива.ДнейСтажа;
			Если Стаж > МаксСтажФизЛица Тогда
				МаксСтажФизЛица = Стаж
			КонецЕсли;
		КонецЦикла;
		СтажСтроки = Строка.ЛетСтажа*1000 + Строка.МесяцевСтажа*10 + Строка.ДнейСтажа;
		Если СтажСтроки < МаксСтажФизЛица Тогда
			//удалить строку из РС
			МенеджерЗаписи = РегистрыСведений.НакопленныеСтажиФизическихЛиц.СоздатьМенеджерЗаписи();
			МенеджерЗаписи.ФизическоеЛицо = Строка.ФизическоеЛицо;
			МенеджерЗаписи.ДатаНачала = Строка.ДатаНачала;
			МенеджерЗаписи.ДатаОкончания = Строка.ДатаОкончания;
			МенеджерЗаписи.ВидСтажа = Объект.ВидСтажа;
			МенеджерЗаписи.Прочитать();
			Если МенеджерЗаписи.Выбран() Тогда
				Сообщить("В РС ""Накопленные стажи физических лиц"" удалена строка: физическое лицо = " + МенеджерЗаписи.ФизическоеЛицо + ", вид стажа = " + МенеджерЗаписи.ВидСтажа + ", дата начала = " + МенеджерЗаписи.ДатаНачала + ", дата окончания = " + МенеджерЗаписи.ДатаОкончания + ", размер месяцев = " + МенеджерЗаписи.РазмерМесяцев + ", размер дней = " + МенеджерЗаписи.РазмерДней + ", прерван = " + МенеджерЗаписи.Прерван);
				МенеджерЗаписи.Удалить();
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
	
	Сообщить("Выполнение обработки завершено.");
	
КонецПроцедуры

&НаКлиенте
Процедура ПосчитатьТекущийСтаж(Команда)
	ПосчитатьТекущийСтажНаСервере();
КонецПроцедуры
