﻿
&НаСервере
Процедура ПолучитьОсновныхСотрудниковНаСервере()
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	Если Объект.ОтборПоОрганизации = Истина Тогда
		ПоискФизическихЛицБезОсновногоСотрудникаПоОрганизации(Объект.Организация, Запрос)
	Иначе
		СписокОрганизаций = СписокОрганизаций();
		Для Каждого ОрганизацияСсылка Из СписокОрганизаций Цикл
			ПоискФизическихЛицБезОсновногоСотрудникаПоОрганизации(ОрганизацияСсылка, Запрос)
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПолучитьОсновныхСотрудников(Команда)
	Объект.ОсновныеСотрудники.Очистить();
	ПолучитьОсновныхСотрудниковНаСервере();
	Сообщить("Завершено выполнение обработки.");
КонецПроцедуры

&НаКлиенте
Процедура ОтборПоОрганизацииПриИзменении(Элемент)
	
	Если Объект.ОтборПоОрганизации = Истина Тогда
		Элементы.Организация.Доступность = Истина
	Иначе
		Элементы.Организация.Доступность = Ложь
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	Объект.НачалоПериодаРаботы = Дата('18991231');
	Объект.КонецПериодаРаботы = КонецМесяца(ТекущаяДата());
	Объект.Период = ТекущаяДата();
	
КонецПроцедуры

&НаСервере
Функция СписокОрганизаций()
	
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ
		|	Организации.Ссылка КАК Организация
		|ИЗ
		|	Справочник.Организации КАК Организации";
	Возврат Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("Организация");
	
КонецФункции

&НаСервере
Процедура ПоискФизическихЛицБезОсновногоСотрудникаПоОрганизации(Организация, Запрос)
	
	ТЗФизическиеЛица = КадровыйУчет.ФизическиеЛицаРаботавшиеВОрганизации(Ложь, Организация, Объект.НачалоПериодаРаботы, Объект.КонецПериодаРаботы, Неопределено);
	СписокФизическихЛиц = ТЗФизическиеЛица.ВыгрузитьКолонку("ФизическоеЛицо");
	// Сформируем временную таблицу ВТФизическиеЛицаРаботавшиеВОрганизации.
	КадровыйУчет.СоздатьВТФизическиеЛицаРаботавшиеВОрганизации(Запрос.МенеджерВременныхТаблиц, Ложь, Организация, Объект.НачалоПериодаРаботы, Объект.КонецПериодаРаботы, Неопределено);
	// Сформируем временную таблицу ВТОсновныеСотрудникиФизическихЛиц.
	КадровыйУчет.СоздатьВТОсновныеСотрудникиФизическихЛиц(Запрос.МенеджерВременныхТаблиц, Ложь, СписокФизическихЛиц, Организация, Объект.Период);
	// Теперь найдем физических лиц, у которых нет основного сотрудника.
	Запрос.Текст = "ВЫБРАТЬ
	               |	РаботавшиеФизЛица.ФизическоеЛицо КАК ФизическоеЛицо,
	               |	ОсновныеСотрудники.ГоловнаяОрганизация КАК Организация
	               |ИЗ
	               |	ВТФизическиеЛицаРаботавшиеВОрганизации КАК РаботавшиеФизЛица
	               |		ЛЕВОЕ СОЕДИНЕНИЕ ВТОсновныеСотрудникиФизическихЛиц КАК ОсновныеСотрудники
	               |		ПО РаботавшиеФизЛица.ФизическоеЛицо = ОсновныеСотрудники.ФизическоеЛицо
	               |ГДЕ
	               |	ОсновныеСотрудники.Сотрудник ЕСТЬ NULL
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |УНИЧТОЖИТЬ ВТФизическиеЛицаРаботавшиеВОрганизации
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |УНИЧТОЖИТЬ ВТОсновныеСотрудникиФизическихЛиц";
	ТЗФизЛицаБезОсновногоСотрудника = Запрос.Выполнить().Выгрузить();
	Для Каждого Строка Из ТЗФизЛицаБезОсновногоСотрудника Цикл
		НоваяСтрокаТЧ = Объект.ОсновныеСотрудники.Добавить();
		НоваяСтрокаТЧ.ФизическоеЛицо = Строка.ФизическоеЛицо;
		НоваяСтрокаТЧ.Организация = Организация;
	КонецЦикла;
	
КонецПроцедуры