﻿
&НаСервере
Процедура УдалитьПустыеПозицииШРНаСервере()
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	ШтатноеРасписание.Ссылка КАК Ссылка
	               |ИЗ
	               |	Справочник.ШтатноеРасписание КАК ШтатноеРасписание
	               |ГДЕ
	               |	ШтатноеРасписание.Наименование = &Наименование";
	Запрос.УстановитьПараметр("Наименование", "");
	
	СсылкиНаПустыеПозицииШР = Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("Ссылка");
	
	Если СсылкиНаПустыеПозицииШР.Количество() > 0 Тогда
		УдалитьОбъекты(СсылкиНаПустыеПозицииШР,Ложь,,,,);
		Сообщить("Позиции штатного расписания с пустым наименованием удалены.");
	Иначе
		Сообщить("Позиции штатного расписания с пустым наименованием не обнаружены.")
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура УдалитьПустыеПозицииШР(Команда)
	УдалитьПустыеПозицииШРНаСервере();
КонецПроцедуры
